import * as actionTypes from "./actionTypes";
import { createAction } from "redux-actions";

export const searchUserRequest = createAction(actionTypes.SEARCH_USERS_REQUEST);
export const searchUsersSuccess = createAction(actionTypes.SEARCH_USERS_SUCCESS, data => data);
export const searchUsersFailure = createAction(actionTypes.SEARCH_USERS_FAILURE);
export const searchUsers = createAction(actionTypes.SEARCH_USERS);
