import { expect } from "chai";
import { renderIntoDocument, scryRenderedDOMComponentsWithClass } from "react-addons-test-utils";
import {
    UserListPage,
    manyUsersFoundMessageClassName,
    nothingFoundMessageClassName,
    smthWrongMessageClassName,
    loaderClassName,
    __RewireAPI__ as UserListPageRewireAPI
} from "./../components/UserListPage.jsx";

import { initialState } from "./../reducer";

describe("List component", () => {
    const listClassName = "list";
    const menuClassName = "menu";
    let state = { ...initialState };

    UserListPageRewireAPI.__Rewire__("List", React.createClass({
        render: () => <div className={listClassName}></div>
    }));

    UserListPageRewireAPI.__Rewire__("Menu", React.createClass({
        render: () => <div className={menuClassName}></div>
    }));

    it("отображает список, когда пользователи найдены", () => {
        state.users = [1, 2];
        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const listGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, listClassName);
        expect(listGroup.length).to.equal(1);
    });

    it("показывает сообщение, если ничего не найдено, и что-то введено в строку поиска", () => {
        state.users = [];
        state.searchString = "some search text";
        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const nothingFoundMessageGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, nothingFoundMessageClassName);
        expect(nothingFoundMessageGroup.length).to.equal(1);
    });

    it("не показывает сообщение, что ничего не найдено, если ничего не введено в строку поиска", () => {
        state.users = [];
        state.searchString = "";
        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const nothingFoundMessageGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, nothingFoundMessageClassName);
        expect(nothingFoundMessageGroup.length).to.equal(0);
    });

    it("показывает сколько всего найдено пользователей, если найдено больше, чем отображается", () => {
        state.users = [1, 2, 3, 4, 5];
        state.totalUsersFound = 20;

        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const manyUsersFoundMessageGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, manyUsersFoundMessageClassName);
        expect(manyUsersFoundMessageGroup.length).to.equal(1);
    });

    it("показывает сколько всего найдено пользователей, если найдено столько, сколько отображается", () => {
        state.users = [1, 2, 3, 4, 5];
        state.totalUsersFound = 5;

        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const manyUsersFoundMessageGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, manyUsersFoundMessageClassName);
        expect(manyUsersFoundMessageGroup.length).to.equal(0);
    });

    it("показывает сообщение об ошибке, если isFailed = true", () => {
        state.isFailed = true;

        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const smthWrongMessageGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, smthWrongMessageClassName);
        expect(smthWrongMessageGroup.length).to.equal(1);
    });

    it("не показывает список пользователей, если isFailed = true", () => {
        state.users = [1, 2];
        state.isFailed = true;

        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const listGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, listClassName);
        expect(listGroup.length).to.equal(0);
    });

    it("не показывает сообщение об ошибке, если isFailed = false", () => {
        state.isFailed = false;

        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const smthWrongMessageGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, smthWrongMessageClassName);
        expect(smthWrongMessageGroup.length).to.equal(0);
    });

    it("показывает loader, если isSearching = true", () => {
        state.isSearching = true;

        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const loaderGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, loaderClassName);
        expect(loaderGroup.length).to.equal(1);
    });

    it("не показывает loader, если isSearching = false", () => {
        state.isSearching = false;

        const UserListPageRendered = renderIntoDocument(<UserListPage {...state} />);
        const loaderGroup = scryRenderedDOMComponentsWithClass(UserListPageRendered, loaderClassName);
        expect(loaderGroup.length).to.equal(0);
    });
});
