import { expect } from "chai";
import freeze from "deep-freeze";
import userListReducer, { initialState } from "./../reducer";
import { searchUsers, searchUserRequest, searchUsersSuccess, searchUsersFailure } from "./../actions";

describe("UserList reducer", () => {
    it(" should not replace same users", () => {
        const state = freeze({
            ...initialState,
            users: [
                freeze({ Thumbprint: "thumbprint1" }),
                freeze({ Thumbprint: "thumbprint2" })
            ]
        });

        const searchResult = {
            Users: [
                freeze({ Thumbprint: "thumbprint3" }),
                freeze({ Thumbprint: "thumbprint1" })
            ],
            TotalCount: 2
        };

        const searchUsersActionResult = searchUsersSuccess(searchResult);
        const newState = userListReducer(state, searchUsersActionResult);

        expect(newState.users[0]).to.equal(searchResult.Users[0]);
        expect(newState.users[1]).to.equal(state.users[0]);
    });

    it(" should return old users when result the same", () => {
        const state = freeze({
            ...initialState,
            users: [
                freeze({ Thumbprint: "thumbprint1" }),
                freeze({ Thumbprint: "thumbprint2" })
            ]
        });

        const searchResult = {
            Users: [
                freeze({ Thumbprint: "thumbprint1" }),
                freeze({ Thumbprint: "thumbprint2" })
            ],
            TotalCount: 2
        };

        const searchUsersActionResult = searchUsersSuccess(searchResult);
        const newState = userListReducer(state, searchUsersActionResult);

        expect(newState.users).to.equal(state.users);
    });

    it(" shouldn't isFailed & isSearching be true at the same time", () => {
        let state = {
            ...initialState,
            isFailed: false,
            isSearching: false
        };

        const searchResult = {
            Users: [],
            TotalCount: 0
        };

        state = userListReducer(state, searchUsers("some search text"));
        expect(state.isFailed).to.deep.equal(false);
        expect(state.isSearching).to.deep.equal(false);

        state = userListReducer(state, searchUserRequest());
        expect(state.isFailed).to.deep.equal(false);
        expect(state.isSearching).to.deep.equal(true);

        state = userListReducer(state, searchUsersSuccess(searchResult));
        expect(state.isFailed).to.deep.equal(false);
        expect(state.isSearching).to.deep.equal(false);

        state = userListReducer(state, searchUsersFailure());
        expect(state.isFailed).to.deep.equal(true);
        expect(state.isSearching).to.deep.equal(false);
    });
});

