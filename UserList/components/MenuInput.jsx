import { Component, PropTypes } from "react";
import { connect } from "react-redux";

import Input from "react-input-autosize";

import stylesMap from "../styles/Menu.scss";
import { searchUsers } from "../actions";

class MenuInput extends Component {
    constructor(props) {
        super(props);

        this._handleSearchInputChange = this._handleSearchInputChange.bind(this);
    }

    focus() {
        this._input.focus();
    }

    _handleSearchInputChange({ target: { value } }) {
        const { searchUsers, searchString } = this.props;
        if (value !== searchString) {
            searchUsers(value);
        }
    }

    render() {
        const { searchString } = this.props;
        return (
            <Input ref={component => { this._input = component; }}
                   className={stylesMap.inputWrapper}
                   inputClassName={stylesMap.input}
                   onChange={this._handleSearchInputChange}
                   value={searchString}
                   placeholder={"Поиск по ФИО, отпечатку"}
            />
        );
    }
}

MenuInput.propTypes = {
    searchString: PropTypes.string,
    searchUsers: PropTypes.func,
    changeSearchString: PropTypes.func
};

const mapStateToProps = state => ({ searchString: state.searchString });
export default connect(mapStateToProps, { searchUsers }, null, { withRef: true })(MenuInput);
