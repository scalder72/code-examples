import { Component, PropTypes } from "react";
import cx from "classnames";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import shouldPureComponentUpdate from "react-pure-render/function";
import ListItem from "./ListItem.jsx";
import styles from "../styles/List.scss";
import animationStyles from "../styles/Animations.scss";

const UserListHeader = () => (
    <li className={ styles.row }>
        <div className={cx(styles.header, styles.userInfo)}>
            Пользователь и сертификат
        </div>
        <div className={cx(styles.header, styles.support)}>
            Техподдержка
        </div>
        <div className={cx(styles.header, styles.oorv)}>
            ООРВ
        </div>
        <div className={cx(styles.header, styles.managerArm)}>
            АРМ Менеджера
        </div>
        <div className={cx(styles.header, styles.hm)}>
            Health Monitor
        </div>
    </li>
);

class List extends Component {
    shouldComponentUpdate = shouldPureComponentUpdate;

    render() {
        const { users, userProfileUrl } = this.props;
        const items = users.map((user) => (
            <ListItem key={user.Thumbprint} href={`${userProfileUrl}/${user.Id}`} {...user} />
        ));

        return (
            <ReactCSSTransitionGroup
                component="ul"
                className={styles.list}
                transitionName={{
                    enter: animationStyles.slideEnter,
                    enterActive: animationStyles.slideEnterActive,
                    leave: animationStyles.slideLeave,
                    leaveActive: animationStyles.slideLeaveActive,
                    appear: animationStyles.slideEnter,
                    appearActive: animationStyles.slideEnterActive
                }}
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
                transitionAppearTimeout={300}
                transitionAppear={true}
            >
                <UserListHeader />
                {items}
            </ReactCSSTransitionGroup>
        );
    }
}

List.propTypes = {
    users: PropTypes.array.isRequired,
    userProfileUrl: PropTypes.string.isRequired
};

export default List;
