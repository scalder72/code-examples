import { PropTypes, Component } from "react";
import {formatDate} from "billing-ui/libs/moment";
import shouldPureComponentUpdate from "react-pure-render/function";
import Icon, {IconTypes} from "billing-ui/components/Icon";

import styles from "../styles/List.scss";
import Platform from "./Platform";

class ListItem extends Component {
    shouldComponentUpdate = shouldPureComponentUpdate;

    render() {
        const { Fio = "____", Thumbprint, Upto, AccessToPlatforms, href } = this.props;
        return (
            <li className={styles.row}>
                <a className={styles.user} href={href}>
                    <div className={styles.userInfo}>
                        <h3 className={styles.userName}>{Fio}</h3>
                        <div className={styles.userInfoItem}>
                            <Icon className={styles.certIcon} type={IconTypes.Certificate}/>{Thumbprint}
                        </div>
                        <div className={styles.userInfoItem}>Действует до: {formatDate(Upto)}</div>
                    </div>
                    <Platform className={styles.support} access={AccessToPlatforms.Support}/>
                    <Platform className={styles.oorv} access={AccessToPlatforms.Oorv}/>
                    <Platform className={styles.managerArm} access={AccessToPlatforms.ManagerArm}/>
                    <Platform className={styles.hm} access={AccessToPlatforms.HealthMonitor}/>
                </a>
            </li>
        );
    }
}

ListItem.propTypes = {
    Fio: PropTypes.string,
    Thumbprint: PropTypes.string.isRequired,
    Upto: PropTypes.string.isRequired,
    AccessToPlatforms: PropTypes.shape({
        Support: PropTypes.bool.isRequired,
        Oorv: PropTypes.bool.isRequired,
        ManagerArm: PropTypes.bool.isRequired,
        HealthMonitor: PropTypes.bool.isRequired
    }),
    href: PropTypes.string.isRequired
};

export default ListItem;
