import { Component, PropTypes } from "react";
import { connect } from "react-redux";

import Icon, { IconTypes } from "billing-ui/components/Icon";
import MenuButton from "./MenuButton";
import SearchInput from "./MenuInput";

import styles from "../styles/Menu.scss";

class Menu extends Component {
    constructor(props) {
        super(props);
        this._handleSearchButtonClick = this._handleSearchButtonClick.bind(this);
    }

    _handleSearchButtonClick() {
        this._searchInput.focus();
    }

    render() {
        const { userProfileUrl, usersApiUrl } = this.props;

        return (
            <menu className={styles.menu}>
                <MenuButton href={userProfileUrl}>
                    <Icon className={styles.icon} type={IconTypes.Upload} />
                    <span className={styles.text}>Загрузить сертификат</span>
                </MenuButton>
                <MenuButton onClick={this._handleSearchButtonClick}>
                    <Icon className={styles.icon} type={IconTypes.Search} />
                    <SearchInput ref={component => { this._searchInput = component.getWrappedInstance(); }} />
                </MenuButton>
                <MenuButton href={usersApiUrl}>
                    <Icon className={styles.icon} type={IconTypes.User} />
                    <span className={styles.text}>Добавить пользователя api</span>
                </MenuButton>
            </menu>
        );
    }
}

Menu.propTypes = {
    userProfileUrl: PropTypes.string.isRequired,
    usersApiUrl: PropTypes.string.isRequired
};

export default connect((state) => ({
    userProfileUrl: state.userProfileUrl,
    usersApiUrl: state.usersApiUrl
}))(Menu);
