import { PropTypes } from "react";
import cx from "classnames";

import Button from "billing-ui/components/Button";
import stylesMap from "../styles/Menu.scss";

const MenuButton = ({ styles, className, onClick, href, children }) => (
    <Button styles={styles}
            className={cx(stylesMap.button, className)}
            href={href}
            onClick={onClick}
    >
        {children}
    </Button>
);

MenuButton.propTypes = {
    styles: PropTypes.object,
    className: PropTypes.string,
    onClick: PropTypes.func,
    href: PropTypes.string,
    children: PropTypes.node
};

MenuButton.defaultProps = {
    styles: {},
    onClick: () => {}
};

export default MenuButton;
