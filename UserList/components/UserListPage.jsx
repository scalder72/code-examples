import { Component, PropTypes } from "react";
import { connect } from "react-redux";
import cx from "classnames";
import {resolveString} from "billing-ui/helpers/PluralStringResolver";
import Loader from "react-ui/Loader";
import Spinner from "react-ui/Spinner";

import Menu from "./Menu.jsx";
import List from "./List.jsx";
import styles from "../styles/UserListPage.scss";
import animationStyles from "../styles/Animations.scss";

export const nothingFoundMessageClassName = "user-list-page__nothing-found-message";
export const manyUsersFoundMessageClassName = "user-list-page__many-users-found-message";
export const smthWrongMessageClassName = "user-list-page__smth-wrong-message";
export const loaderClassName = "user-list-page__loader";

export class UserListPage extends Component {
    render() {
        const { users, isSearching, isFailed, userProfileUrl, totalUsersFound, searchString } = this.props;

        const hasUsers = users.length > 0;
        const wasUsersFound = !isFailed && hasUsers;
        const hasMoreUsersThanFound = wasUsersFound && totalUsersFound > users.length;
        const wasNoUsersFound = !isFailed && !isSearching && !hasUsers && searchString.length > 0;
        const wasSearchFailed = !isSearching && isFailed;

        return (
            <section>
                <div className={styles.menuWrapper}>
                    <Menu />
                </div>
                <div className={styles.listWrapper}>
                    {wasUsersFound && (
                        <List users={users} userProfileUrl={userProfileUrl}/>
                    )}

                    {hasMoreUsersThanFound && (
                        <p className={cx(styles.message, animationStyles.fadeIn, manyUsersFoundMessageClassName)}>
                            Найдено {totalUsersFound} {resolveString(totalUsersFound, ["пользователь", "пользователя", "пользователей"])}.
                            Уточните параметры поиска.
                        </p>
                    )}

                    {wasNoUsersFound && (
                        <p className={cx(styles.message, animationStyles.fadeIn, nothingFoundMessageClassName)}>
                            Такого пользователя у нас нет. <br /> Можете смело
                            загружать его сертификат :)
                        </p>
                    )}

                    {isSearching && (
                        <div className={cx(animationStyles.fadeIn, loaderClassName)} >
                            <Loader type={Spinner.Types.mini} active={true} caption={"Ищем..."} />
                        </div>
                    )}

                    {wasSearchFailed && (
                        <p className={cx(styles.message, animationStyles.fadeIn, smthWrongMessageClassName)}>
                            Что-то пошло не так :(
                        </p>
                    )}
                </div>
            </section>
        );
    }
}

UserListPage.propTypes = {
    isFailed: PropTypes.bool.isRequired,
    isSearching: PropTypes.bool.isRequired,
    users: PropTypes.array.isRequired,
    userProfileUrl: PropTypes.string.isRequired,
    totalUsersFound: PropTypes.number.isRequired,
    searchString: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
    isFailed: state.isFailed,
    isSearching: state.isSearching,
    users: state.users,
    userProfileUrl: state.userProfileUrl,
    totalUsersFound: state.totalUsersFound,
    searchString: state.searchString
});

export default connect(mapStateToProps)(UserListPage);
