import {Component} from "react";
import {Provider} from "react-redux";
import configureStore from "billing-ui/helpers/reduxHelpers/configureStore";

import UserListPage from "./UserListPage";
import rootReducer, {initialState} from "../reducer";
import rootSaga from "../saga";

class UserListContainer extends Component {
    render() {
        const store = configureStore({...initialState, ...this.props}, rootReducer, rootSaga);
        return (
            <Provider store={store}>
                <UserListPage/>
            </Provider>
        );
    }
}

UserListContainer.propTypes = {};
export default UserListContainer
