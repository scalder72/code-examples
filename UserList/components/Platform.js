import styles from "../styles/List.scss";
import Icon, {IconTypes} from "billing-ui/components/Icon";
import cx from "classnames";
import { PropTypes } from "react";

const Platform = ({ className, access }) => {
    const iconClass = cx(styles.platformIcon, {
        [styles.iconBaseLine]: !access
    });

    return (
        <div className={className}>
            <Icon className={iconClass}
                  type={access ? IconTypes.Ok : IconTypes.BaseLine}
            />
        </div>
    )
};

Platform.propTypes = {
    className: PropTypes.string.isRequired,
    access: PropTypes.bool.isRequired
};

export default Platform;
