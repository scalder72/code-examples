export const SEARCH_USERS_REQUEST = "@@usersList/SEARCH_USERS_REQUEST";
export const SEARCH_USERS_SUCCESS = "@@usersList/SEARCH_USERS_SUCCESS";
export const SEARCH_USERS_FAILURE = "@@usersList/SEARCH_USERS_FAILURE";
export const SEARCH_USERS = "@@usersList/SEARCH_USERS";
