import {combineReducers} from "redux";
import {handleActions} from "redux-actions";
import * as actionTypes from "./actionTypes";

export const initialState = {
    isSearching: false,
    isFailed: false,
    searchString: "",
    users: [],
    totalUsersFound: 0
};

export const reducer = combineReducers({
    userProfileUrl: (state = "") => state,
    usersApiUrl: (state = "") => state,
    usersListSearchUrl: (state = "") => state,

    isSearching: handleActions({
        [actionTypes.SEARCH_USERS]: () => false,
        [actionTypes.SEARCH_USERS_REQUEST]: () => true,
        [actionTypes.SEARCH_USERS_SUCCESS]: () => false,
        [actionTypes.SEARCH_USERS_FAILURE]: () => false
    }, initialState.isSearching),

    isFailed: handleActions({
        [actionTypes.SEARCH_USERS]: () => false,
        [actionTypes.SEARCH_USERS_REQUEST]: () => false,
        [actionTypes.SEARCH_USERS_FAILURE]: () => true,
        [actionTypes.SEARCH_USERS_SUCCESS]: () => false
    }, initialState.isFailed),

    searchString: handleActions({
        [actionTypes.SEARCH_USERS]: (state, { payload: searchString }) => searchString
    }, initialState.searchString),

    users: handleActions({
        [actionTypes.SEARCH_USERS_SUCCESS]: (state, { payload: {Users} }) => {
            const usersMap = state.reduce((result, user) => {
                result[user.Thumbprint] = user;
                return result;
            }, {});

            if (state.length === Users.length && Users.every(({Thumbprint}) => usersMap[Thumbprint])) {
                return state;
            }

            return Users.map((user) => {
                const sameOldUser = usersMap[user.Thumbprint];
                return sameOldUser ? sameOldUser : user;
            });
        }
    }, initialState.users),

    totalUsersFound: handleActions({
        [actionTypes.SEARCH_USERS_SUCCESS]: (state, { payload: {TotalCount} }) => TotalCount
    }, initialState.totalUsersFound)
});

export default reducer;
