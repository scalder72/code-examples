import {takeLatest, delay} from "redux-saga";
import {put, call, select, fork, cancel, cancelled} from "redux-saga/effects";
import axios from "axios";

import * as actionTypes from "./actionTypes";
import {searchUsersFailure, searchUsersSuccess, searchUserRequest } from "./actions";

const emptySearchResult = {Users: [], TotalCount: 0};
const MIN_SEARCH_TEXT_LENGTH = 2;

function* searchUsers({payload: searchText}) {
    const usersListSearchUrl = yield select((state) => state.usersListSearchUrl);
    try {
        if (searchText.trim().length > MIN_SEARCH_TEXT_LENGTH) {
            yield put(searchUserRequest());
            const {data} = yield axios.get(usersListSearchUrl, {params: {text: searchText}});
            yield put(searchUsersSuccess(data));
        } else {
            yield put(searchUsersSuccess(emptySearchResult));
        }
    } catch (e) {
        const isCancelled = yield cancelled();
        if (!isCancelled) {
            yield put(searchUsersFailure());
        }
    }
}

function* userListSearchWatcher() {
    let searchUserTask;

    yield takeLatest([actionTypes.SEARCH_USERS], function* searchUserTaskHandler(action) {
        yield call(delay, 200);
        if (searchUserTask) {
            yield cancel(searchUserTask);
        }

        searchUserTask = yield fork(searchUsers, action);
    });
}

export default function* rootSaga() {
    yield [
        userListSearchWatcher()
    ];
}
