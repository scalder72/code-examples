﻿import axios from "billing-ui/libs/axios";

export const DELETE_BILL_BEGIN = "DELETE_BILL_BEGIN";
export const DELETE_BILL_SUCCESS = "DELETE_BILL_SUCCESS";
export const DELETE_BILL_COMPLETE = "DELETE_BILL_COMPLETE";
export const DELETE_BILL_ERROR = "DELETE_BILL_ERROR";

export const RESTORE_BILL_BEGIN = "RESTORE_BILL_BEGIN";
export const RESTORE_BILL_SUCCESS = "RESTORE_BILL_SUCCESS";
export const RESTORE_BILL_COMPLETE = "RESTORE_BILL_COMPLETE";
export const RESTORE_BILL_ERROR = "RESTORE_BILL_ERROR";

export const deleteBillBegin = () => ({
    type: DELETE_BILL_BEGIN
});

export const deleteBillSuccess = () => ({
    type: DELETE_BILL_SUCCESS,
    meta: {
        ga: {
            category: "bill-general",
            label: "delete",
            action: "delete"
        }
    }
});

export const deleteBillComplete = () => ({
    type: DELETE_BILL_COMPLETE
});

export const restoreBillBegin = () => ({
    type: RESTORE_BILL_BEGIN
});

export const restoreBillSuccess = () => ({
    type: RESTORE_BILL_SUCCESS,
    meta: {
        ga: {
            category: "bill-general",
            label: "recover",
            action: "recover"
        }
    }
});

export const restoreBillComplete = () => ({
    type: RESTORE_BILL_COMPLETE
});

export const deleteBill = (router, billId) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { SendToDumpUrl }, appState: { dumped } } = billInfo;

    if (dumped) {
        return;
    }

    dispatch(deleteBillBegin());

    axios
        .post(SendToDumpUrl)
        .then(() => {
            dispatch(deleteBillSuccess());
            dispatch(deleteBillComplete());
            router.replace(`/billinfo/${billId}/dumped`);
        })
        .catch(() => {
            dispatch(deleteBillComplete());
        });
};

export const restoreBill = (router, billId) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { RestoreFromDumpUrl }, appState: { dumped } } = billInfo;

    if (!dumped) {
        router.replace(`/billinfo/${billId}`);
        return;
    }

    dispatch(restoreBillBegin());

    axios
        .post(RestoreFromDumpUrl)
        .then(() => {
            dispatch(restoreBillSuccess());
            dispatch(restoreBillComplete());
            router.replace(`/billinfo/${billId}`);
        })
        .catch(() => {
            dispatch(restoreBillComplete());
        });
};

export const actionTypes = {
    DELETE_BILL_BEGIN,
    DELETE_BILL_SUCCESS,
    DELETE_BILL_COMPLETE,
    DELETE_BILL_ERROR,

    RESTORE_BILL_BEGIN,
    RESTORE_BILL_SUCCESS,
    RESTORE_BILL_COMPLETE,
    RESTORE_BILL_ERROR
};
