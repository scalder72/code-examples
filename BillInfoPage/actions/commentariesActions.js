﻿import axios from "billing-ui/libs/axios";

export const FETCH_COMMENTS_BEGIN = "FETCH_COMMENTS_BEGIN";
export const FETCH_COMMENTS_SUCCESS = "FETCH_COMMENTS_SUCCESS";
export const FETCH_COMMENTS_COMPLETE = "FETCH_COMMENTS_COMPLETE";
export const CREATE_COMMENT_BEGIN = "CREATE_COMMENT_BEGIN";
export const CREATE_COMMENT_ERROR = "CREATE_COMMENT_ERROR";
export const UPDATE_COMMENT_BEGIN = "UPDATE_COMMENT_BEGIN";
export const UPDATE_COMMENT_SUCCESS = "UPDATE_COMMENT_SUCCESS";
export const UPDATE_COMMENT_ERROR = "UPDATE_COMMENT_ERROR";
export const DELETE_COMMENT_ERROR = "DELETE_COMMENT_ERROR";

export const fetchCommentsBegin = () => ({
    type: FETCH_COMMENTS_BEGIN
});

export const fetchCommentsComplete = () => ({
    type: FETCH_COMMENTS_COMPLETE
});

export const fetchCommentsSuccess = (commentaries) => ({
    type: FETCH_COMMENTS_SUCCESS,
    commentaries
});

export const createCommentBegin = (text) => ({
    type: CREATE_COMMENT_BEGIN,
    Text: text,
    meta: {
        ga: {
            category: "bill-general",
            action: "save",
            label: "comment"
        }
    }
});

export const createCommentError = (text) => ({
    type: CREATE_COMMENT_ERROR,
    userInput: text
});

export const updateCommentBegin = (CommentaryId, text) => ({
    type: UPDATE_COMMENT_BEGIN,
    CommentaryId: CommentaryId,
    Text: text
});

export const updateCommentSuccess = (CommentaryId, data) => ({
    type: UPDATE_COMMENT_SUCCESS,
    CommentaryId: CommentaryId,
    Author: data.Author,
    Date: data.Date,
    meta: {
        ga: {
            category: "bill-general",
            action: "save",
            label: "comment"
        }
    }
});

export const updateCommentError = (CommentaryId, text, oldMessage) => ({
    type: UPDATE_COMMENT_ERROR,
    CommentaryId: CommentaryId,
    Text: oldMessage,
    userInput: text
});

export const deleteCommentError = (CommentaryId) => ({
    type: DELETE_COMMENT_ERROR,
    CommentaryId: CommentaryId
});

export const fetchCommentaries = (url, billId) => (dispatch) => {
    dispatch(fetchCommentsBegin());

    axios.get(`${url}/${billId}`)
        .then((response) => dispatch(fetchCommentsSuccess(response.data)))
        .finally(() => dispatch(fetchCommentsComplete()));
};

export const deleteCommentary = (url, commentaryId, billId) => (dispatch) => {
    axios
        .delete(`${url}/${commentaryId}`)
        .then(() => dispatch(fetchCommentaries(url, billId)))
        .catch(() => dispatch(deleteCommentError(commentaryId)));
};

export const createCommentary = (url, billId, text) => (dispatch) => {
    dispatch(createCommentBegin(text));

    axios
        .post(`${url}/${billId}`, { text: text })
        .then(() => dispatch(fetchCommentaries(url, billId)))
        .catch(() => dispatch(createCommentError(text)));
};

export const updateCommentary = (url, billId, text, CommentaryId, oldMessage) => (dispatch) => {
    dispatch(updateCommentBegin(CommentaryId, text));

    axios
        .put(`${url}/${CommentaryId}`, { text: text })
        .then((response) => dispatch(updateCommentSuccess(CommentaryId, response.data)))
        .catch(() => dispatch(updateCommentError(CommentaryId, text, oldMessage)));
};

export const actionTypes = {
    FETCH_COMMENTS_BEGIN,
    FETCH_COMMENTS_COMPLETE,
    FETCH_COMMENTS_SUCCESS,
    CREATE_COMMENT_BEGIN,
    CREATE_COMMENT_ERROR,
    UPDATE_COMMENT_BEGIN,
    UPDATE_COMMENT_SUCCESS,
    UPDATE_COMMENT_ERROR,
    DELETE_COMMENT_ERROR
};
