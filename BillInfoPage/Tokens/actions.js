﻿import axios from "billing-ui/libs/axios";
import { createAction } from "redux-actions";
import * as types from "./actionTypes";

const fetchTokensBegin = createAction(types.FETCH_TOKENS_BEGIN);
const fetchTokensComplete = createAction(types.FETCH_TOKENS_COMPLETE);
const fetchTokensSuccess = createAction(types.FETCH_TOKENS_SUCCESS);
const fetchTokensError = createAction(types.FETCH_TOKENS_ERROR);

export const fetchTokens = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { TokensUrl } } = billInfo;

    dispatch(fetchTokensBegin());
    axios
        .get(TokensUrl)
        .then(response => dispatch(fetchTokensSuccess(response.data)))
        .catch((e) => dispatch(fetchTokensError(e)))
        .finally(() => dispatch(fetchTokensComplete()));
};


export const deliverTokenBegin = createAction(types.DELIVER_TOKEN_BEGIN);
export const deliverTokenSuccess = createAction(types.DELIVER_TOKEN_SUCCESS);
export const deliverTokenComplete = createAction(types.DELIVER_TOKEN_COMPLETE);
export const deliverTokenError = createAction(types.DELIVER_TOKEN_ERROR);

export const deliverToken = (tokenId, tokenGroupName) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { TokenDeliveryUrl } } = billInfo;

    dispatch(deliverTokenBegin({ tokenGroupName }));
    axios
        .put(`${TokenDeliveryUrl}/${tokenId}`)
        .then(response => dispatch(deliverTokenSuccess({ token: response.data, tokenId, tokenGroupName })))
        .catch((e) => dispatch(deliverTokenError(e)))
        .finally(() => dispatch(deliverTokenComplete({ tokenGroupName })));
};


export const undeliverTokenBegin = createAction(types.UNDELIVER_TOKEN_BEGIN);
export const undeliverTokenSuccess = createAction(types.UNDELIVER_TOKEN_SUCCESS);
export const undeliverTokenComplete = createAction(types.UNDELIVER_TOKEN_COMPLETE);
export const undeliverTokenError = createAction(types.UNDELIVER_TOKEN_ERROR);

export const undeliverToken = (tokenId, tokenGroupName) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { TokenUndeliveryUrl } } = billInfo;

    dispatch(undeliverTokenBegin({ tokenId, tokenGroupName }));
    axios
        .put(`${TokenUndeliveryUrl}/${tokenId}`)
        .then(response => dispatch(undeliverTokenSuccess({ token: response.data, tokenId, tokenGroupName })))
        .catch((e) => dispatch(undeliverTokenError(e)))
        .finally(() => dispatch(undeliverTokenComplete({ tokenId, tokenGroupName })));
};
