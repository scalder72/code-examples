﻿import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchTokens } from "./actions";
import TokensWrapper from "./components/TokensWrapper.jsx";

const mapStateToProps = ({ billInfo: { tokensInfo }} = {}) => ({ ...tokensInfo });
const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchTokens }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TokensWrapper);
