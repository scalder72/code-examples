﻿import { Component, PropTypes } from "react";
import debounce from "debounce";

import Loader from "react-ui/Loader";

import TokensGroup from "./TokensGroup.jsx";
import TokensGroupWithDelivery from "./TokensGroupWithDelivery.jsx";

import styles from "../styles/TokensGroup.scss";

const renderTokensGroup = (TokenGroupInfoModels, CanIssue) => {
    const TokensGroupComponent = CanIssue ? TokensGroupWithDelivery : TokensGroup;
    return (TokenGroupInfoModels || []).map((group) => <TokensGroupComponent key={group.TokenGroupName} { ...group } />);
};

class TokensWrapper extends Component {
    constructor(props) {
        super(props);
        this._fetchTokens = debounce(props.fetchTokens, 200);
    }

    componentDidMount() {
        this._resolveDataFetching(this.props);
    }

    _resolveDataFetching = ({ fetchRequired }) => {
        if (fetchRequired) {
            this._fetchTokens();
        }
    };

    render() {
        const { TokenGroupInfoModels, CanIssue, isLoading } = this.props;

        return (
            <div className={styles.wrapper}>
                <Loader active={isLoading} type="big">
                    {renderTokensGroup(TokenGroupInfoModels, CanIssue)}
                </Loader>
            </div>
        );
    }
}

TokensWrapper.propTypes = {
    fetchRequired: PropTypes.bool,
    fetchTokens: PropTypes.func.isRequired,
    CanIssue: PropTypes.bool,
    TokenGroupInfoModels: PropTypes.array,
    isLoading: PropTypes.bool.isRequired
};

TokensWrapper.defaultProps = {
    isLoading: false
};

export default TokensWrapper;
