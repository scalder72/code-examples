﻿import { PropTypes } from "react";
import moment from "billing-ui/libs/moment";

import { resolveString } from "../../../Shared/Helpers/PluralStringResolver.js";
import styles from "../styles/TokensGroup.scss";

const TokensGroup = ({ TokenGroupName, DeliveredTokens, NotDeliveredTokens }) => {
    const deliveredTokensCount = DeliveredTokens.length;
    const undeliveredTokensCount = NotDeliveredTokens.length;
    const tokensDelivered = deliveredTokensCount > 0;

    return (
        <div className={styles["group-wrapper"]}>
            <div className={styles["group-header"]}>
                <div className={styles.title}>{TokenGroupName}</div>
            </div>
            <div className={styles["token-info"]}>
                {tokensDelivered && (
                    <span className={styles["token-info-date"]}>
                        {moment(DeliveredTokens[0].DeliveryDate).format("L")}
                    </span>
                )}
                {tokensDelivered
                    ? `Автоматически ${resolveString(deliveredTokensCount, ["выдан", "выдано", "выдано"])}
                                         ${deliveredTokensCount}
                                         ${resolveString(deliveredTokensCount, ["токен", "токена", "токенов"])}`
                    : `Клиенту выдадут ${undeliveredTokensCount}
                                           ${resolveString(undeliveredTokensCount, ["токен", "токена", "токенов"])}`
                }
            </div>
        </div>
    );
};

TokensGroup.propTypes = {
    TokenGroupName: PropTypes.string,
    DeliveredTokens: PropTypes.array,
    NotDeliveredTokens: PropTypes.array
};

export default TokensGroup;
