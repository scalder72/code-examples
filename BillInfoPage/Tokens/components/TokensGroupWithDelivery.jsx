﻿import { Component, PropTypes } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import cx from "classnames";
import debounce from "debounce";

import { deliverToken, undeliverToken } from "../actions";

import DeliveredToken from "./DeliveredToken.jsx";
import styles from "../styles/TokensGroup.scss";

export const deliverLink = "tokens-group-deliver-link";

export class TokensGroupWithDelivery extends Component {
    constructor(props) {
        super(props);
        this._deliverToken = debounce(props.deliverToken, 200);
        this._undeliverToken = debounce(props.undeliverToken, 200);
    }

    _resolveDeliverToken() {
        const { isLinkActive, TokenGroupName, NotDeliveredTokens } = this.props;

        if (isLinkActive) {
            this._deliverToken(NotDeliveredTokens[0].Id, TokenGroupName);
        }
    }

    _resolveUnDeliverToken(tokenId) {
        const { TokenGroupName } = this.props;
        this._undeliverToken(tokenId, TokenGroupName);
    }

    render() {
        const { TokenGroupName, DeliveredTokens, NotDeliveredTokens, isLinkActive } = this.props;
        const notDeliveredTokensCount = NotDeliveredTokens.length;

        return (
            <div className={styles["group-wrapper"]}>
                <div className={styles["group-header"]}>
                    <div className={styles.title}>{TokenGroupName}</div>
                    { notDeliveredTokensCount > 0 && (<div className={styles.count}>{notDeliveredTokensCount}</div>) }
                    { notDeliveredTokensCount > 0 && NotDeliveredTokens[0].CanDeliver && (
                        <div className={styles["link-col"]}>
                            <span className={cx(styles.link, deliverLink, {[styles.disabled]: !isLinkActive})}
                                  onClick={() => this._resolveDeliverToken()}>
                                Выдать
                            </span>
                        </div>
                    ) }
                </div>

                {DeliveredTokens.map((token) => (
                    <DeliveredToken key={token.TokenId} { ...token }
                                    onUndeliver={tokenId => { this._resolveUnDeliverToken(tokenId) }} />
                ))}
            </div>
        );
    }
}

TokensGroupWithDelivery.propTypes = {
    deliverToken: PropTypes.func,
    undeliverToken: PropTypes.func,

    TokenGroupName: PropTypes.string,
    DeliveredTokens: PropTypes.array,
    NotDeliveredTokens: PropTypes.array,
    isLinkActive: PropTypes.bool
};

TokensGroupWithDelivery.defaultProps = {
    isLinkActive: true
};

const mapDispatchToProps = (dispatch) => bindActionCreators({ deliverToken, undeliverToken }, dispatch);

export default connect(null, mapDispatchToProps)(TokensGroupWithDelivery);
