﻿import { PropTypes } from "react";

import cx from "classnames";
import moment from "billing-ui/libs/moment";

import styles from "../styles/DeliveredToken.scss";

const DeliveredToken = ({ DeliveryDate, CanUnDeliver, TokenId, isLinkActive, onUndeliver }) => (
    <div className={styles.token}>
        <div className={styles.date}>
            {moment(DeliveryDate).format("L")}
            <span className={styles.status}>Выдан</span>
        </div>
        {CanUnDeliver && (
            <div className={styles.action}>
                <span className={cx(styles.link, {[styles.disabled]: !isLinkActive})} onClick={ onUndeliver.bind(this, TokenId) }>
                    Отменить
                </span>
            </div>
        )}
    </div>
);

DeliveredToken.propTypes = {
    TokenId: PropTypes.string,
    DeliveryDate: PropTypes.string,
    CanUnDeliver: PropTypes.bool,
    onUndeliver: PropTypes.func.isRequired,
    isLinkActive: PropTypes.bool
};

DeliveredToken.defaultProps = {
    isLinkActive: true
};

export default DeliveredToken;
