﻿import { handleActions } from "redux-actions";
import reduceReducers from "reduce-reducers";

import * as types from "../actionTypes";
import { arrayReduceHelper } from "../../../Shared/Helpers/ArrayHelper";
import { deliveredTokenReducer } from "./deliveredTokenReducer";

export const tokensGroupReducer = reduceReducers(
    handleActions({
        [types.DELIVER_TOKEN_SUCCESS]: (state, { payload }) => ({
            ...state,
            DeliveredTokens: [...state.DeliveredTokens, payload.token],
            NotDeliveredTokens: state.NotDeliveredTokens.filter(token => token.Id !== payload.tokenId)
        }),
        [types.DELIVER_TOKEN_BEGIN]: (state) => ({
            ...state,
            isLinkActive: false
        }),
        [types.DELIVER_TOKEN_COMPLETE]: (state) => ({
            ...state,
            isLinkActive: true
        }),
        [types.UNDELIVER_TOKEN_SUCCESS]: (state, { payload }) => ({
            ...state,
            DeliveredTokens: state.DeliveredTokens.filter(token => token.TokenId !== payload.tokenId),
            NotDeliveredTokens: [...state.NotDeliveredTokens, payload.token]
        })
    }),
    (state, action) => {
        const { type, payload } = action;

        switch (type) {
            case types.UNDELIVER_TOKEN_BEGIN:
            case types.UNDELIVER_TOKEN_COMPLETE:
                return {
                    ...state,
                    DeliveredTokens: arrayReduceHelper(
                        token => token.TokenId === payload.tokenId,
                        deliveredTokenReducer,
                        state.DeliveredTokens,
                        action
                    )
                };

            default:
                return state;
        }
    }
);

export default tokensGroupReducer;
