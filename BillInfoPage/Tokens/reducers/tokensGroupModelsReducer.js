﻿import * as types from "../actionTypes";
import { arrayReduceHelper } from "../../../Shared/Helpers/ArrayHelper";
import { tokensGroupReducer } from "./tokensGroupReducer";

export const tokensGroupModelsReducer = (state = {}, action) => {
    const tokenGroups = state.TokenGroupInfoModels;

    switch (action.type) {
        case types.DELIVER_TOKEN_BEGIN:
        case types.DELIVER_TOKEN_COMPLETE:
        case types.DELIVER_TOKEN_SUCCESS:

        case types.UNDELIVER_TOKEN_BEGIN:
        case types.UNDELIVER_TOKEN_COMPLETE:
        case types.UNDELIVER_TOKEN_SUCCESS:
            return {
                ...state,
                TokenGroupInfoModels: arrayReduceHelper(
                    tokenGroup => tokenGroup.TokenGroupName === action.payload.tokenGroupName,
                    tokensGroupReducer,
                    tokenGroups,
                    action
                )
            };

        default:
            return state;
    }
};

export default tokensGroupModelsReducer;
