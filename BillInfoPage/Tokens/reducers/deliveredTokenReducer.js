﻿import { handleActions } from "redux-actions";
import * as types from "../actionTypes";

export const deliveredTokenReducer = handleActions({
    [types.UNDELIVER_TOKEN_BEGIN]: (state) => ({
        ...state,
        isLinkActive: false
    }),
    [types.UNDELIVER_TOKEN_COMPLETE]: (state) => ({
        ...state,
        isLinkActive: true
    })
});

export default deliveredTokenReducer;
