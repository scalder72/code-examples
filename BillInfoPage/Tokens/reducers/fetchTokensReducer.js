﻿import { handleActions } from "redux-actions";
import * as types from "../actionTypes";

export const fetchTokensReducer = handleActions({
    [types.FETCH_TOKENS_BEGIN]: (state) => ({
        ...state,
        fetchRequired: false,
        isLoading: true
    }),
    [types.FETCH_TOKENS_COMPLETE]: (state) => ({
        ...state,
        fetchRequired: false,
        isLoading: false
    }),
    [types.FETCH_TOKENS_SUCCESS]: (state, action) => ({
        ...state,
        ...action.payload,
        fetchRequired: false
    })
}, { fetchRequired: true });

export default fetchTokensReducer;
