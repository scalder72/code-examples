﻿import reduceReducers from "reduce-reducers";
import { fetchTokensReducer } from "./reducers/fetchTokensReducer";
import { tokensGroupModelsReducer } from "./reducers/tokensGroupModelsReducer";

export default reduceReducers(fetchTokensReducer, tokensGroupModelsReducer);
