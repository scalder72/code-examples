import { expect } from "chai";
import freeze from "deep-freeze";
import reducer from "../reducer";
import { undeliverTokenBegin, undeliverTokenSuccess, undeliverTokenComplete } from "../actions";

describe("tokens undelivery reducer ", () => {
    it("should set link inactive on undelivery begin", () => {
        const tokenId = "tokenId";
        const tokenGroupName = "tokenGroupName";
        const initState = freeze({
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }, { TokenId: tokenId }]
            }]
        });

        const expectedState = {
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }, { TokenId: tokenId, isLinkActive: false }]
            }]
        };
        const actual = reducer(initState, undeliverTokenBegin({ tokenId, tokenGroupName }));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should insert undelivered token on undelivery success", () => {
        const tokenId = "tokenId";
        const tokenGroupName = "tokenGroupName";
        const token = { Id: tokenId };
        const initState = freeze({
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }, { TokenId: tokenId }],
                NotDeliveredTokens: [{ Id: "tokenId3" }]
            }]
        });

        const expectedState = {
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }],
                NotDeliveredTokens: [{ Id: "tokenId3" }, { Id: tokenId }]
            }]
        };
        const actual = reducer(initState, undeliverTokenSuccess({ token, tokenId, tokenGroupName }));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should set link active on undelivery complete", () => {
        const tokenId = "tokenId";
        const tokenGroupName = "tokenGroupName";
        const initState = freeze({
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }, { TokenId: tokenId }]
            }]
        });

        const midState = reducer(initState, undeliverTokenBegin({ tokenId, tokenGroupName }));

        const expectedState = {
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }, { TokenId: tokenId, isLinkActive: true }]
            }]
        };
        const actual = reducer(midState, undeliverTokenComplete({ tokenId, tokenGroupName }));

        expect(actual).to.deep.equal(expectedState);
    });
});
