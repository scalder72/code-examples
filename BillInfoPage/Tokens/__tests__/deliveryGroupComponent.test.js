import { expect } from "chai";
import { renderIntoDocument, scryRenderedDOMComponentsWithClass } from "react-addons-test-utils";

import { TokensGroupWithDelivery, deliverLink } from "../components/TokensGroupWithDelivery.jsx";

describe("tokens group with delivery component", () => {
    it("should render deliver link if has deliverable token", () => {
        const initGroup = {
            TokenGroupName: "tokenGroupName",
            DeliveredTokens: [{ TokenId: "tokenId2" }],
            NotDeliveredTokens: [{ Id: "tokenId", CanDeliver: true }]
        };

        const group = renderIntoDocument(<TokensGroupWithDelivery {...initGroup} />);
        const link = scryRenderedDOMComponentsWithClass(group, deliverLink);

        expect(link.length).to.equal(1);
    });

    it("should not render deliver link if has no deliverable token", () => {
        const initGroup = {
            TokenGroupName: "tokenGroupName",
            DeliveredTokens: [{ TokenId: "tokenId2" }],
            NotDeliveredTokens: [{ Id: "tokenId" }]
        };

        const group = renderIntoDocument(<TokensGroupWithDelivery {...initGroup} />);
        const link = scryRenderedDOMComponentsWithClass(group, deliverLink);

        expect(link.length).to.equal(0);
    });

    it("should not render deliver link if has no tokens", () => {
        const initGroup = {
            TokenGroupName: "tokenGroupName",
            DeliveredTokens: [{ TokenId: "tokenId2" }],
            NotDeliveredTokens: []
        };

        const group = renderIntoDocument(<TokensGroupWithDelivery {...initGroup} />);
        const link = scryRenderedDOMComponentsWithClass(group, deliverLink);

        expect(link.length).to.equal(0);
    });
});
