import { expect } from "chai";
import freeze from "deep-freeze";
import reducer from "../reducer";
import { deliverTokenBegin, deliverTokenSuccess, deliverTokenComplete } from "../actions";

describe("tokens delivery reducer ", () => {
    it("should set link inactive on delivery begin", () => {
        const tokenGroupName = "tokenGroupName";
        const initState = freeze({
            TokenGroupInfoModels: [
                { TokenGroupName: tokenGroupName },
                { TokenGroupName: "somethingElse" }
            ]
        });

        const expectedState = {
            TokenGroupInfoModels: [
                {
                    TokenGroupName: tokenGroupName,
                    isLinkActive: false
                },
                { TokenGroupName: "somethingElse" }
            ]
        };
        const actual = reducer(initState, deliverTokenBegin({ tokenGroupName }));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should insert delivered token on delivery success", () => {
        const tokenId = "tokenId";
        const tokenGroupName = "tokenGroupName";
        const token = { TokenId: tokenId };
        const initState = freeze({
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }],
                NotDeliveredTokens: [{ Id: tokenId }, { Id: "tokenId3" }]
            }]
        });

        const expectedState = {
            TokenGroupInfoModels: [{
                TokenGroupName: tokenGroupName,
                DeliveredTokens: [{ TokenId: "tokenId2" }, { TokenId: tokenId }],
                NotDeliveredTokens: [{ Id: "tokenId3" }]
            }]
        };
        const actual = reducer(initState, deliverTokenSuccess({ token, tokenId, tokenGroupName }));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should set link active on delivery complete", () => {
        const tokenGroupName = "tokenGroupName";
        const initState = freeze({
            TokenGroupInfoModels: [
                { TokenGroupName: tokenGroupName },
                { TokenGroupName: "somethingElse" }
            ]
        });

        const midState = reducer(initState, deliverTokenBegin({ tokenGroupName }));

        const expectedState = {
            TokenGroupInfoModels: [
                {
                    TokenGroupName: tokenGroupName,
                    isLinkActive: true
                },
                { TokenGroupName: "somethingElse" }
            ]
        };
        const actual = reducer(midState, deliverTokenComplete({ tokenGroupName }));

        expect(actual).to.deep.equal(expectedState);
    });
});
