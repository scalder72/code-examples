﻿import { Component, PropTypes } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import cx from "classnames";

import Icon, { IconTypes } from "billing-ui/components/Icon";
import Button, { AppearanceType } from "billing-ui/components/Button";

import { getDescription } from "./helpers/billInfoActionTypes";
import { deleteBill } from "../../actions/actionCreators";

import styles from "./../styles/SendToDumpBillAction.scss";
import buttonStyles from "../../Shared/styles/ActionButton.scss";

class SendToDumpBillAction extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    render() {
        const { ActionType, BillId, SendToDumpRefuseReason, deleteBill } = this.props;

        if (SendToDumpRefuseReason) {
            return (
                <div className={styles["no-delete"]}>
                    <div className={styles["no-delete_strikeout"]}>
                        <Icon type={IconTypes.Trash} />
                    </div>

                    Удалить счет нельзя:<br />
                    {SendToDumpRefuseReason}
                </div>
            );
        }

        return (
            <Button appearance={AppearanceType.delete}
                    className={cx(buttonStyles.button, buttonStyles.item)}
                    onClick={() => { deleteBill(this.context.router, BillId); }}>
                <Icon type={IconTypes.Trash} /> {getDescription(ActionType)}
            </Button>
        );
    }
}

SendToDumpBillAction.propTypes = {
    deleteBill: PropTypes.func.isRequired,
    ActionType: PropTypes.string.isRequired,
    BillId: PropTypes.string.isRequired,
    SendToDumpRefuseReason: PropTypes.string
};

const mapDispatchToProps = dispatch => bindActionCreators({deleteBill}, dispatch);
export default connect(null, mapDispatchToProps)(SendToDumpBillAction)
