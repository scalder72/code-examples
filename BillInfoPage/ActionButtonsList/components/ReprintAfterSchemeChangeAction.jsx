﻿import { Component, PropTypes } from "react";
import reactDOM from "react-dom";
import cx from "classnames";

import App from "./../../../globalApp";
import Button from "billing-ui/components/Button";

import { getDescription } from "./helpers/billInfoActionTypes";
import styles from "../../Shared/styles/ActionButton.scss";

import ChangeSchemeSafelyBillActionControl from "ContentBase/scripts/BillActions/Actions/ChangeSchemeSafelyBillAction";

class ReprintAfterSchemeChangeAction extends Component {
    componentDidMount() {
        this._changeSchemeControl = new ChangeSchemeSafelyBillActionControl({
            changeSchemeLink: reactDOM.findDOMNode(this)
        });

        this._changeSchemeControl.onActionSelect((goToPrintUrl) => {
            App.history.push(goToPrintUrl);
        });
    }

    componentWillUnmount() {
        this._changeSchemeControl.remove();
    }

    render() {
        const { ActionType, ActionUrl, GoToPrintUrl, onClick } = this.props;

        return (
            <div data-url={ActionUrl} data-print-url={GoToPrintUrl}>
                <Button className={cx(styles.button, styles.item)} onClick={onClick}>
                    {getDescription(ActionType)}
                </Button>
            </div>
        );
    }
}

ReprintAfterSchemeChangeAction.propTypes = {
    ActionType: PropTypes.string.isRequired,
    ActionUrl: PropTypes.string.isRequired,
    GoToPrintUrl: PropTypes.string.isRequired,
    onClick: PropTypes.func
};

export default ReprintAfterSchemeChangeAction;
