﻿import { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Loader from "react-ui/Loader";
import ErrorBlock from "./../../Shared/components/WarningBlock.jsx";
import { fetchActionButtons } from "../actions";

import billActionCreator from "./helpers/billInfoActionsFactory";

const createBillActions = (items = {}) => Object
    .keys(items)
    .map(actionType => billActionCreator(actionType, items[actionType]));

class ActionButtonsList extends Component {
    _resolveFetch() {
        const { fetchRequired, fetchActionButtons } = this.props;

        if (fetchRequired) {
            fetchActionButtons();
        }
    }

    componentDidMount() {
        const { fetchActionButtons } = this.props;
        fetchActionButtons();
    }

    componentDidUpdate() {
        this._resolveFetch();
    }

    render() {
        const { isLoading, items, errorMessage } = this.props;

        return (
            <Loader active={isLoading} caption="">
                {errorMessage && (
                    <ErrorBlock>
                        Идут технические работы, поэтому некоторые данные не загрузились.<br />
                        Обновите страницу через минуту.
                    </ErrorBlock>
                )}

                {!errorMessage && createBillActions(items)}
            </Loader>
        );
    }
}

ActionButtonsList.propTypes = {
    fetchRequired: PropTypes.bool,
    fetchActionButtons: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    items: PropTypes.object.isRequired,
    errorMessage: PropTypes.string
};

const mapActionsToProps = dispatch => bindActionCreators({fetchActionButtons}, dispatch);
const mapStateToProps = ({ billInfo: { billActions }}) => billActions;
export default connect(mapStateToProps, mapActionsToProps)(ActionButtonsList);
