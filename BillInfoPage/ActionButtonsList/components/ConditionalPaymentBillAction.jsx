﻿import { Component, PropTypes } from "react";
import ReactDOM from "react-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import cx from "classnames";

import Button from "billing-ui/components/Button";

import { getDescription } from "./helpers/billInfoActionTypes";
import { resolveBillActionSucceed } from "../actions";

import styles from "../../Shared/styles/ActionButton.scss";

class ConditionalPaymentBillAction extends Component {
    componentDidMount() {
        this._initConditionalPayment();
    }

    componentWillUnmount() {
        this._removeConditionalPayment();
    }

    _initConditionalPayment() {
        const { resolveBillActionSucceed } = this.props;

        // eslint-disable-next-line no-undef
        this._conditionalPayment = new ConditionalPaymentPopupControl({
            link: this._link,
            popupClassName: "right",
            popupPosition: {
                top: this._link.offsetHeight + 12,
                left: this._link.offsetWidth - 382
            }
        });

        this._conditionalPayment.onFormActionSuccess(resolveBillActionSucceed);
    }

    _removeConditionalPayment() {
        if (this._conditionalPayment) {
            this._conditionalPayment.remove();
            this._conditionalPayment = null;
        }
    }

    render() {
        const { ActionType, ActionUrl, onClick } = this.props;

        return (
            <Button className={cx(styles.button, styles.item)}
                    ref={component => { this._link = ReactDOM.findDOMNode(component) }}
                    onClick={onClick}
                    href={ActionUrl}>
                {getDescription(ActionType)}
            </Button>
        );
    }
}

ConditionalPaymentBillAction.propTypes = {
    resolveBillActionSucceed: PropTypes.func.isRequired,
    ActionType: PropTypes.string.isRequired,
    ActionUrl: PropTypes.string.isRequired,
    onClick: PropTypes.func
};

const mapDispatchToProps = dispatch => bindActionCreators({ resolveBillActionSucceed }, dispatch);
export default connect(null, mapDispatchToProps)(ConditionalPaymentBillAction);
