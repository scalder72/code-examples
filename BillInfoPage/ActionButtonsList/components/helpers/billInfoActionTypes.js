﻿import { getDescriptionCreator } from "../../../../Shared/utils";

export const billInfoActionTypes = {
    PartnerPrint: "PartnerPrint",
    PartnerSend: "PartnerSend",
    Edo: "Edo",
    AddConditionalPayment: "AddConditionalPayment",
    CreateConditionalPayment: "CreateConditionalPayment",
    SendToDump: "SendToDump",
    ChangeSchemeSafely: "ChangeSchemeSafely"
};

export const getDescription = getDescriptionCreator({
    PartnerPrint: "Напечатать документы",
    PartnerSend: "Отправить по почте",
    Edo: "Отправить через ДД",
    AddConditionalPayment: "Оплатить условно",
    CreateConditionalPayment: "Оплатить условно",
    SendToDump: "Удалить",
    ChangeSchemeSafely: (<span>Перейти с оферты<br/>на договор</span>)
});

const billGeneralCategory = "bill-general";

export const getGAOptions = getDescriptionCreator({
    PartnerPrint: {category: billGeneralCategory, action: "print", label: "print"},
    PartnerSend: {category: billGeneralCategory, action: "send", label: "mail"},
    Edo: {category: billGeneralCategory, action: "send", label: "diadoc"},
    AddConditionalPayment: {category: billGeneralCategory, action: "create", label: "conditional-payment"},
    CreateConditionalPayment: {category: billGeneralCategory, action: "create", label: "conditional-payment"},
    ChangeSchemeSafely: {category: "bill-certs", action: "click", label: "change-offer"}
});

export default billInfoActionTypes;
