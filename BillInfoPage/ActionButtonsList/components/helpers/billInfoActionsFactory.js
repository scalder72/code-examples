﻿import { PropTypes } from "react";
import Link from "react-router/lib/Link";

import billInfoActionTypes, { getDescription, getGAOptions } from "./billInfoActionTypes";
import cx from "classnames";
import GoogleAnalytics from "billing-ui/helpers/GoogleAnalytics";

import Button from "billing-ui/components/Button";
import SendToDumpBillAction from "./../SendToDumpBillAction.jsx";
import ConditionalPaymentBillAction from "./../ConditionalPaymentBillAction.jsx";
import ReprintAfterSchemeChangeAction from "./../ReprintAfterSchemeChangeAction.jsx";

import styles from "../../../Shared/styles/ActionButton.scss";

const trackEvent = (actionType) => {
    const { category, action, label } = getGAOptions(actionType);
    GoogleAnalytics.triggerEventAsync(category, action, label);
};

const BillInfoActionsFactory = (actionType, props) => {
    const { ActionUrl } = props;

    if (billInfoActionTypes[actionType] === undefined) {
        return null;
    }

    switch (actionType) {
        case billInfoActionTypes.SendToDump:
            return (<SendToDumpBillAction key={actionType} {...props} />);

        case billInfoActionTypes.ChangeSchemeSafely:
            return (<ReprintAfterSchemeChangeAction key={actionType} {...props} onClick={() => { trackEvent(actionType) }} />);

        case billInfoActionTypes.AddConditionalPayment:
        case billInfoActionTypes.CreateConditionalPayment:
            return (<ConditionalPaymentBillAction key={actionType} {...props} onClick={() => { trackEvent(actionType) }} />);

        default:
            return (
                <Link to={ActionUrl} key={actionType}>
                    <Button className={cx(styles.button, styles.item)} onClick={() => { trackEvent(actionType) }}>
                        {getDescription(actionType)}
                    </Button>
                </Link>
            );
    }
};

BillInfoActionsFactory.propTypes = {
    ActionUrl: PropTypes.string
};

export default BillInfoActionsFactory;
