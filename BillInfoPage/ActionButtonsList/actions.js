﻿import axios from "billing-ui/libs/axios";
import { createAction } from "redux-actions";
import * as type from "./actionTypes";

export const updateActionButtonsBegin = () => ({
    type: type.UPDATE_ACTION_BUTTON_BEGIN
});

export const updateActionButtonsComplete = () => ({
    type: type.UPDATE_ACTION_BUTTON_COMPLETE
});

export const updateActionButtonsSuccess = (model) => ({
    type: type.UPDATE_ACTION_BUTTON_SUCCESS,
    model
});

export const updateActionButtonsError = (xhr) => {
    let errorMessage = "";

    try {
        errorMessage = JSON.parse(xhr.responseText).message;
    } catch (err) {
        errorMessage = "Что-то пошло не так =(";
    }

    return {
        type: type.UPDATE_ACTION_BUTTON_ERROR,
        errorMessage
    };
};

export const fetchActionButtons = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { GetBillActionTypesUrl } } = billInfo;

    dispatch(updateActionButtonsBegin());

    axios.get(GetBillActionTypesUrl)
        .then(response => dispatch(updateActionButtonsSuccess(response.data)))
        .catch(xhr => dispatch(updateActionButtonsError(xhr)))
        .finally(() => dispatch(updateActionButtonsComplete()));
};

export const billActionSucceed = createAction(type.CONDITIONAL_PAYMENT_CREATED);

export const resolveBillActionSucceed = () => (dispatch) => {
    dispatch(billActionSucceed());
    dispatch(fetchActionButtons());
};
