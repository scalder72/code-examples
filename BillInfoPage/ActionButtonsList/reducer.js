﻿import { handleActions } from "redux-actions";
import * as actionTypes from "./actionTypes";

export default handleActions({
    [actionTypes.UPDATE_ACTION_BUTTON_BEGIN]: state => ({
        ...state,
        isLoading: true
    }),

    [actionTypes.UPDATE_ACTION_BUTTON_COMPLETE]: state => ({
        ...state,
        isLoading: false
    }),

    [actionTypes.UPDATE_ACTION_BUTTON_SUCCESS]: (state, action) => ({
        ...state,
        items: action.model,
        fetchRequired: false
    }),

    [actionTypes.UPDATE_ACTION_BUTTON_ERROR]: (state, action) => ({
        ...state,
        errorMessage: action.errorMessage
    })
}, {
    isLoading: false,
    items: {}
});
