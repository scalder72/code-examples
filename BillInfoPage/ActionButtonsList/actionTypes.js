﻿export const UPDATE_ACTION_BUTTON_BEGIN = "UPDATE_ACTION_BUTTON_BEGIN";
export const UPDATE_ACTION_BUTTON_SUCCESS = "UPDATE_ACTION_BUTTON_SUCCESS";
export const UPDATE_ACTION_BUTTON_COMPLETE = "UPDATE_ACTION_BUTTON_COMPLETE";
export const UPDATE_ACTION_BUTTON_ERROR = "UPDATE_ACTION_BUTTON_ERROR";

export const CONDITIONAL_PAYMENT_CREATED = "CONDITIONAL_PAYMENT_CREATED";
