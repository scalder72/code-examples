import { handleActions } from "redux-actions";
import { INDIVIDUAL_INN_LENGTH, KPP_LENGTH } from "../utils/const";
import * as actionTypes from "./../actionTypes";
import PayerTypes from "../utils/PayerTypes"

export const initialState = {
    payerInn: "",
    payerKpp: "",
    payerName: "",
    payerTypesCaption: "",
    payerId: null,
    receiverErrors: null,
    linksError: null,
    isPayerExist: false,
    isRequisitesValid: true,
    inProcess: false
};

const parseNumbers = (value, maxLength) => (value.match(/\d+/g) || []).join("").substring(0, maxLength);

const resolvePayerType = (inn, currentPayerType) => {
    if (inn.length < INDIVIDUAL_INN_LENGTH) {
        return PayerTypes.LegalEntity;
    }

    if (currentPayerType === undefined || currentPayerType === PayerTypes.LegalEntity) {
        return PayerTypes.IndividualBusinessman;
    }

    return currentPayerType;
};

export const certificateSelectClientReducer = handleActions({
    [actionTypes.OPEN_SELECT_PAYER]: (state, { payload }) => {
        if (payload && payload.preloadedPayer) {
            const { Inn, Kpp, RecipientName } = payload.preloadedPayer;
            return {
                ...initialState,
                payerInn: Inn,
                payerKpp: Kpp,
                payerName: RecipientName,
                isPayerExist: true,
                isRequisitesValid: true,
                payer: payload.preloadedPayer
            };
        }

        return initialState;
    },

    [actionTypes.SEARCH_PAYER_BEGIN]: (state, action) => ({
        ...state,
        receiverErrors: null,
        linksError: null,
        inProcess: true
    }),
    [actionTypes.SEARCH_PAYER_SUCCESS]: (state, { payload }) => {
        const { RecipientErrors, LinksError, CertificateRecipient } = payload;

        return {
            ...state,
            isPayerExist: CertificateRecipient !== null,
            isRequisitesValid: RecipientErrors === null && LinksError === null,
            payer: CertificateRecipient,
            receiverErrors: RecipientErrors,
            linksError: LinksError,
            inProcess: false
        };
    },
    [actionTypes.SEARCH_PAYER_ERROR]: (state, { payload }) => ({
        ...state,
        receiverErrors: payload,
        linksError: payload,
        payer: null,
        inProcess: false
    }),
    [actionTypes.HANDLE_PAYER_INN_CHANGE]: (state, { payload }) => {
        const { payerInn, currentInn } = payload;
        const { payerType } = state;

        const isInn = payerInn !== undefined;
        const inn = isInn && parseNumbers(currentInn, INDIVIDUAL_INN_LENGTH);

        if (isInn && payerInn !== inn) {
            return {
                ...state,
                payerInn: inn,
                receiverErrors: null,
                linksError: null,
                payer: null,
                isPayerExist: false,
                inProcess: false,
                payerType: resolvePayerType(inn, payerType)
            }
        }

        return state;
    },
    [actionTypes.HANDLE_PAYER_KPP_CHANGE]: (state, { payload }) => {
        const { payerKpp, currentKpp } = payload;

        const isKpp = payerKpp !== undefined;
        const kpp = isKpp && parseNumbers(currentKpp, KPP_LENGTH);

        if (isKpp && payerKpp !== kpp) {
            return {
                ...state,
                payerKpp: kpp,
                receiverErrors: null,
                linksError: null,
                payer: null,
                isPayerExist: false,
                inProcess: false
            }
        }

        return state;
    },
    [actionTypes.HANDLE_PAYER_NAME_CHANGE]: (state, { payload }) => {
        const { payerName } = payload;

        const isName = payerName !== undefined;

        if (isName) {
            return {
                ...state,
                payerName
            }
        }

        return state;
    },
    [actionTypes.SELECT_PAYER_TYPE]: (state, { payload: { payerType } }) => ({
        ...state,
        payerType: payerType,
        receiverErrors: null,
        linksError: null,
        inProcess: false
    }),

    [actionTypes.OPEN_PROLONGATION]: (state) => ({
        ...state,
        inProcess: false
    })
}, initialState);

export default certificateSelectClientReducer;
