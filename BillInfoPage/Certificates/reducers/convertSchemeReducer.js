import { handleActions } from "redux-actions";
import * as types from "../actionTypes";

export default handleActions({
    [types.CONVERT_SCHEME_ACTION_BEGIN]: state => ({
        ...state,
        isLoading: true
    }),
    [types.CONVERT_SCHEME_ACTION_SUCCESS]: state => ({
        ...state,
        isLoading: false
    }),
    [types.CONVERT_SCHEME_ACTION_ERROR]: state => ({
        ...state,
        isLoading: false
    })
});
