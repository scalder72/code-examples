import { handleActions } from "redux-actions";
import * as types from "./../actionTypes";
import { mergeSlotToSlots, createEmptySlot, createInitialState, updateSlots, updateContacts } from "./../utils/certificateIssueReducerHelper";

const initialState = {
    inProcess: false,
    payerId: null,
    contactsByPayerIds: {},
    payersByPayerIds: {},
    addCertificateSlot: null
};

export const certificateIssueReducer = handleActions({
    [types.OPEN_CERTIFICATES_ISSUE]: (state, { payload: { certificateIds } }) => {
        const { payerId } = state;
        const certificatesCount = certificateIds.length;
        const payerInfo = state.payersByPayerIds[payerId];

        const activeSlot = createEmptySlot(certificatesCount, certificatesCount, payerId, payerInfo);
        return updateSlots(activeSlot, null, { ...createInitialState(initialState, state), certificateIds });
    },
    [types.CLOSE_CERTIFICATES_ISSUE]: (state) => createInitialState(initialState, state),

    [types.ADD_CERTIFICATE]: (state) => {
        const { payerId, payersByPayerIds } = state;
        const activeSlot = createEmptySlot(1, 1, payerId, payersByPayerIds[payerId]);
        return updateSlots(activeSlot, null, state);
    },

    [types.REMOVE_CERTIFICATE]: (state, { payload }) => {
        const { activeSlot } = payload;

        return updateSlots(null, activeSlot, state);
    },

    [types.SELECT_QUANTITY]: (state, { payload }) => {
        const { slots, addCertificateSlot } = state;
        const { activeSlot, count } = payload;

        if (slots.every(slot => slot !== activeSlot) && addCertificateSlot !== activeSlot) {
            return state;
        }

        const newSlot = { ...activeSlot, count: count };

        return updateSlots(newSlot, activeSlot, state);
    },

    [types.SELECT_OWNER]: (state, { payload }) => {
        const { contactId, activeSlot } = payload;
        const newSlot = { ...activeSlot, count: 1, contactId };

        return updateSlots(newSlot, activeSlot, state);
    },

    [types.SELECT_PAYER]: (state, { payload }) => {
        const { activeSlot, payerInfo } = payload;

        const newSlot = {
            ...activeSlot,
            ...payerInfo,
            count: 1,
            isSelectPayerLightboxOpen: false,
            contactId: null
        };

        return updateSlots(newSlot, activeSlot, state);
    },

    [types.ADD_PAYER]: (state, { payload }) => {
        const { activeSlot, payerInfo } = payload;

        const newSlot = {
            ...activeSlot,
            ...payerInfo,
            count: 1,
            isSelectPayerLightboxOpen: false,
            contactId: null
        };

        return updateSlots(newSlot, null, state);
    },

    [types.FETCH_CERTIFICATES_SUCCESS]: (state, { payload }) => {
        const { payerId, contacts, payer = {} } = payload;

        if (payerId === undefined) {
            return state;
        }

        return {
            ...state,
            payerId,
            contactsByPayerIds: {
                [payerId]: contacts
            },
            payersByPayerIds: {
                [payerId]: payer
            }
        };
    },

    [types.FETCH_CONTACTS_SUCCESS]: (state, { payload }) => {
        const { payerId, contacts } = payload;
        const contactsByPayerIds = {
            ...state.contactsByPayerIds,
            [payerId]: contacts
        };

        return {
            ...state,
            contactsByPayerIds,
            slots: state.slots ? updateContacts(contactsByPayerIds, state.slots) : null
        };
    },

    [types.SELECT_PAYER_SUCCESS]: (state, { payload }) => {
        const { payerId, payer } = payload;

        const payersByPayerIds = {
            ...state.payersByPayerIds,
            [payerId]: payer
        };

        return {
            ...state,
            payersByPayerIds
        };
    },

    [types.OPEN_SELECT_PAYER]: (state, { payload }) => {
        const { activeSlot, isEmptySlot } = payload;
        const newSlot = { ...activeSlot, isSelectPayerLightboxOpen: true };

        if (activeSlot.isSelectPayerLightboxOpen) {
            return state;
        }

        if (isEmptySlot) {
            return {
                ...state,
                addCertificateSlot: newSlot
            };
        }

        return {
            ...state,
            slots: mergeSlotToSlots(newSlot, activeSlot, state.slots, state.payerId)
        };
    },

    [types.CLOSE_SELECT_PAYER]: (state, { payload }) => {
        const { slots, addCertificateSlot } = state;
        const { activeSlot, isEmptySlot } = payload;
        const newSlot = { ...activeSlot, isSelectPayerLightboxOpen: false };

        if (slots.every(slot => slot !== activeSlot) && addCertificateSlot !== activeSlot) {
            return state;
        }

        if (isEmptySlot) {
            return {
                ...state,
                addCertificateSlot: newSlot
            };
        }

        return {
            ...state,
            slots: mergeSlotToSlots(newSlot, activeSlot, state.slots, state.payerId)
        };
    },

    [types.CERTIFICATES_ISSUE_BEGIN]: (state) => ({ ...state, inProcess: true }),
    [types.CERTIFICATES_ISSUE_ERROR]: (state) => createInitialState(initialState, state),
    [types.CERTIFICATES_ISSUE_SUCCESS]: (state) => createInitialState(initialState, state)
}, initialState);

export default certificateIssueReducer;
