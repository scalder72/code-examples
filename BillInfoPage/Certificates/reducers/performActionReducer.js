import { handleActions } from "redux-actions";
import * as types from "../actionTypes";

export default handleActions({
    [types.PERFORM_ACTION_BEGIN]: state => ({
        ...state,
        isLoading: true
    }),
    [types.PERFORM_ACTION_SUCCESS]: state => ({
        ...state,
        isLoading: false
    }),
    [types.PERFORM_ACTION_ERROR]: state => ({
        ...state,
        isLoading: false
    })
});
