import { handleActions } from "redux-actions";
import * as types from "../actionTypes";

export default handleActions({
    [types.SELECT_IMPORTED_CERTIFICATE]: (state, { payload }) => ({
        ...state,
        selectedCertificate: payload
    })
});
