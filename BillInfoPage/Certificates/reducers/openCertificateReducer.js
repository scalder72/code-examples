import { handleActions } from "redux-actions";
import * as types from "../actionTypes";

export default handleActions({
    [types.OPEN_CERTIFICATE_BEGIN]: state => ({
        ...state,
        isLoading: true
    }),
    [types.OPEN_CERTIFICATE_SUCCESS]: state => ({
        ...state,
        isLoading: false
    }),
    [types.OPEN_CERTIFICATE_ERROR]: state => ({
        ...state,
        isLoading: false
    })
});
