import { arrayReduceHelper } from "billing-ui/helpers/ArrayHelper";
import * as types from "../actionTypes";
import openCertificateReducer from "./openCertificateReducer";
import performActionReducer from "./performActionReducer";

const certificateModelReducer = (state = {}, action) => {
    if (action.payload) {
        const { certificateId, certificatesGroupName } = action.payload;
        const certificatesGroup = state[certificatesGroupName];

        switch (action.type) {
            case types.OPEN_CERTIFICATE_BEGIN:
            case types.OPEN_CERTIFICATE_SUCCESS:
            case types.OPEN_CERTIFICATE_ERROR:
                return {
                    ...state,
                    [certificatesGroupName]: arrayReduceHelper(
                        certificate => certificate.Id === certificateId,
                        openCertificateReducer,
                        certificatesGroup,
                        action
                    )
                };

            case types.PERFORM_ACTION_BEGIN:
            case types.PERFORM_ACTION_SUCCESS:
            case types.PERFORM_ACTION_ERROR:
                return {
                    ...state,
                    [certificatesGroupName]: arrayReduceHelper(
                        certificate => certificate.Id === certificateId,
                        performActionReducer,
                        certificatesGroup,
                        action
                    )
                };

            default:
                return state;
        }
    }

    return state;
};

export default certificateModelReducer;
