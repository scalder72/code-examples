import { handleActions } from "redux-actions";
import { findIndex } from "billing-ui/helpers/ArrayHelper";
import { generateContacts, resolveCheckboxesStatus, resolveSetSelectOwner,
        setActiveContact, setContactIdToSelectedCertificates } from "../utils/certificatesProlongationReducerHelper";

import * as types from "../actionTypes";

const initialState = {
    clientSearchCompleted: false,
    submitEnabled: false,
    prolongationLinkDisabled: false,
    CertificatesForProlongation: []
};

export default handleActions({
    [types.OPEN_PROLONGATION]: (state, { payload: { CertificateIds, CertificateType, CertificateReason } }) => ({
        ...state,
        certificateIds: CertificateIds,
        certificateType: CertificateType,
        certificateReason: CertificateReason,
        certificatesLimit: CertificateIds.length,
        selectedCertificates: []
    }),
    [types.CLOSE_PROLONGATION]: state => ({
        ...initialState
    }),

    [types.FETCH_PROLONGATION_BEGIN]: state => ({
        ...state,
        prolongationLinkDisabled: true,
        clientSearchCompleted: false
    }),
    [types.FETCH_PROLONGATION_SUCCESS]: (state, { payload }) => ({
        ...state,
        ...payload,
        prolongationLinkDisabled: false
    }),
    [types.FETCH_PROLONGATION_ERROR]: state => ({
        ...state,
        prolongationLinkDisabled: false
    }),

    [types.SEARCH_PAYER_SUCCESS]: state => ({
        ...state,
        clientSearchCompleted: true,
        selectedCertificates: [],
        submitEnabled: false
    }),

    [types.PROLONGATION_SEARCH]: (state, { payload: { searchString } }) => ({
        ...state,
        searchString: searchString,
        selectedCertificates: state.selectedCertificates || []
    }),

    [types.ACTUALIZE_CHECKBOXES_STATUSES]: (state, { payload: { certificatesLimit } }) => {
        const { CertificatesForProlongation, selectedCertificates, updatedContacts } = state;
        const certificatesForProlongation = resolveCheckboxesStatus(
            CertificatesForProlongation || [], selectedCertificates, certificatesLimit, updatedContacts
        );

        return {
            ...state,
            CertificatesForProlongation: certificatesForProlongation
        };
    },
    [types.PROLONGATION_SET_SELECT_OWNER]: (state, { payload }) => {
        const { certificateId, contacts, OwnerName } = payload;
        const { selectedCertificates } = state;

        return {
            ...state,
            CertificatesForProlongation: resolveSetSelectOwner(state.CertificatesForProlongation || [], certificateId, OwnerName),
            updatedContacts: generateContacts(contacts, OwnerName),
            selectedCertificates: setContactIdToSelectedCertificates(selectedCertificates, certificateId, null)
        };
    },
    [types.PROLONGATION_SELECT_OWNER]: (state, { payload }) => {
        const { certificateId, contactId } = payload;
        const { updatedContacts, CertificatesForProlongation, selectedCertificates } = state;
        const contactIndex = findIndex(contact => contact.ContactId === contactId, updatedContacts);
        const activeContact = updatedContacts[contactIndex];

        return {
            ...state,
            CertificatesForProlongation: setActiveContact(CertificatesForProlongation, certificateId, activeContact),
            selectedCertificates: setContactIdToSelectedCertificates(selectedCertificates, certificateId, contactId)
        };
    }
}, initialState);
