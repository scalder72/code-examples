import { handleActions } from "redux-actions";
import * as types from "../actionTypes";
import certificatesInfoErrorLevel from "../utils/CertificatesErrorLevel";
import certificatesInfoErrorAction from "../utils/CertificatesErrorAction";

const initialState = {
    fetchRequired: true,
    autoSync: true,

    Error: {
        Level: certificatesInfoErrorLevel.None,
        Action: certificatesInfoErrorAction.None,
        Message: ""
    }
};

export default handleActions({
    [types.FETCH_CERTIFICATES_BEGIN]: (state, { payload: { silent = false } = {}}) => ({
        ...state,
        fetchRequired: false,
        isLoading: !silent
    }),
    [types.FETCH_CERTIFICATES_SUCCESS]: (state, { payload }) => ({
        ...state,
        ...payload,
        wasLoaded: true,
        isLoading: false
    }),
    [types.FETCH_CERTIFICATES_ERROR]: (state) => ({
        ...state,
        wasLoaded: true,
        isLoading: false
    }),
    [types.CERTIFICATES_SYNC_TURNED_ON]: state => ({
        ...state,
        autoSync: true
    }),
    [types.CERTIFICATES_SYNC_TURNED_OFF]: state => ({
        ...state,
        autoSync: false
    })
}, initialState);
