import * as actionTypes from "./../actionTypes";
import { resolveCheckboxesStatus, resolveSelectedCertificates } from "../utils/certificatesProlongationReducerHelper";

const selectClientWithProlongationReducer = (state, action) => {
    switch (action.type) {
        case actionTypes.OPEN_PROLONGATION:
            const { Inn, Kpp, RecipientName } = state.payer;

            return {
                ...state,
                selectClient: {
                    ...state.selectClient,
                    payerInn: Inn,
                    payerKpp: Kpp,
                    payerName: RecipientName,
                    payer: state.payer,
                    isPayerExist: true
                }
            };

        case actionTypes.FETCH_CONTACTS_SUCCESS:
            const { payerId, contacts } = action.payload;

            return {
                ...state,
                contacts,
                certificatesProlongation: {
                    ...state.certificatesProlongation,
                    payerId: payerId
                }
            };

        case actionTypes.SEARCH_PAYER_SUCCESS:
            return {
                ...state,
                certificatesProlongation: {
                    ...state.certificatesProlongation,
                    searchString: ""
                }
            };

        case actionTypes.PROLONGATION_CERTIFICATE_SELECTION:
            const linksError = state.selectClient ? state.selectClient.linksError : null;
            const { certificateId, certificatesLimit, setChecked } = action.payload;
            let submitEnabled = false;
            const selectedCertificates = resolveSelectedCertificates(
                state.certificatesProlongation.selectedCertificates || [], certificateId, setChecked
            );
            const certificatesForProlongation = resolveCheckboxesStatus(
                state.certificatesProlongation.CertificatesForProlongation || [], selectedCertificates, certificatesLimit
            );

            if (selectedCertificates.length > 0) {
                submitEnabled = true;
            }

            return {
                ...state,
                certificatesProlongation: {
                    ...state.certificatesProlongation,
                    CertificatesForProlongation: certificatesForProlongation,
                    submitEnabled: submitEnabled && !linksError,
                    selectedCertificates: selectedCertificates
                }
            };
    }

    return state;
};

export default selectClientWithProlongationReducer;
