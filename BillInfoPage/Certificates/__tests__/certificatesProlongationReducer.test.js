import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducer";
import { createEmptyContact } from "../utils/helpers";

describe("certificates prolongation reducer", () => {
    describe("fetch prolongation", () => {
        const initialState = freeze({
            payer: {
                CertificateRecipient: {
                    Inn: "1",
                    Kpp: "2",
                    RecipientName: "1",
                    RecipientId: "3"
                }
            }
        });

        it("should handle open lightbox", () => {
            const certificateIds = ["id1", "id2"];
            const CertificateType = "CertificateType";
            const CertificateReason = "certificateReason";
            const actual = reducer(initialState, actions.openProlongation({
                CertificateIds: certificateIds,
                CertificateType,
                CertificateReason
            }));

            expect(actual.certificatesProlongation.certificateIds).to.deep.equal(certificateIds);
            expect(actual.certificatesProlongation.certificateType).to.equal(CertificateType);
            expect(actual.certificatesProlongation.certificateReason).to.equal(CertificateReason);
            expect(actual.certificatesProlongation.certificatesLimit).to.equal(certificateIds.length);
            expect(actual.certificatesProlongation.selectedCertificates.length).to.equal(0);
        });

        it("should set link inactive on fetch begin", () => {
            const actual = reducer(initialState, actions.fetchProlongationBegin());
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.prolongationLinkDisabled).to.be.true;
            expect(actualProlongation.clientSearchCompleted).to.be.false;
        });

        it("should set payload on fetch success", () => {
            const actual = reducer(initialState, actions.fetchProlongationSuccess({ whatever: "else" }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.whatever).to.equal("else");
            expect(actualProlongation.prolongationLinkDisabled).to.be.false;
        });

        it("should set link active on fetch error", () => {
            const actual = reducer(initialState, actions.fetchProlongationError());
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.prolongationLinkDisabled).to.be.false;
        });
    });

    describe("certificate for prolongation checking", () => {
        const selectedCertificateId = "id1";
        const initialState = freeze({
            certificatesProlongation: {
                CertificatesForProlongation: [
                    {
                        Id: selectedCertificateId,
                        OwnerName: "OwnerName",
                        ExpirationDate: "2015.12.12",
                        AttentionRequired: false
                    },
                    {
                        Id: "id2",
                        OwnerName: "OwnerName2",
                        ExpirationDate: "2015.12.11",
                        AttentionRequired: false
                    }
                ]
            }
        });

        it("should select checked certificate", () => {
            const actual = reducer(initialState, actions.prolongationCertificateSelection({
                certificateId: selectedCertificateId,
                certificatesLimit: 2
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.selectedCertificates[0]).to.deep.equal({ OldCertificateId: selectedCertificateId });
            expect(actualProlongation.CertificatesForProlongation[0].isChecked).to.be.true;
        });

        it("should enable submit on any checked certificate", () => {
            const actual = reducer(initialState, actions.prolongationCertificateSelection({
                certificateId: selectedCertificateId,
                certificatesLimit: 2
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.submitEnabled).to.be.true;
        });

        it("should disable the rest of certificates if limit exceeded", () => {
            const actual = reducer(initialState, actions.prolongationCertificateSelection({
                certificateId: selectedCertificateId,
                certificatesLimit: 1
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.CertificatesForProlongation[1].isDisabled).to.be.true;
        });
    });

    describe("certificate for prolongation unchecking", () => {
        const selectedCertificateId = "id1";
        const checkedInitialState = freeze({
            certificatesProlongation: {
                CertificatesForProlongation: [
                    {
                        Id: selectedCertificateId,
                        OwnerName: "OwnerName",
                        ExpirationDate: "2015.12.12",
                        AttentionRequired: false,
                        isChecked: true
                    },
                    {
                        Id: "id2",
                        OwnerName: "OwnerName2",
                        ExpirationDate: "2015.12.11",
                        AttentionRequired: false,
                        isDisabled: true
                    }
                ],
                selectedCertificates: [{ OldCertificateId: selectedCertificateId }]
            }
        });

        it("should remove unchecked certificates", () => {
            const actual = reducer(checkedInitialState, actions.prolongationCertificateSelection(
                { certificateId: selectedCertificateId, certificatesLimit: 1 }
            ));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.selectedCertificates.length).to.equal(0);
        });

        it("should disable submit if no checked certificates left", () => {
            const actual = reducer(checkedInitialState, actions.prolongationCertificateSelection(
                { certificateId: selectedCertificateId, certificatesLimit: 1 }
            ));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.submitEnabled).to.be.false;
        });

        it("should enable certificates after uncheck if they were disabled", () => {
            const actual = reducer(checkedInitialState, actions.prolongationCertificateSelection(
                { certificateId: selectedCertificateId, certificatesLimit: 1 }
            ));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.CertificatesForProlongation[1].isDisabled).to.be.false;
        });

        it("should stay checked", () => {
            const actual = reducer(checkedInitialState, actions.prolongationCertificateSelection(
                { certificateId: selectedCertificateId, certificatesLimit: 1, setChecked: true }
            ));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.CertificatesForProlongation[0].isChecked).to.be.true;
        });
    });

    describe("setting select owner", () => {
        const selectedCertificateId = "id1";
        const selectedOwnerName = "OwnerName";
        const contacts = [
            {
                ContactId: "contactId1",
                FullName: "FullName",
                Post: "Get"
            }
        ];
        const emptyContact = createEmptyContact(selectedOwnerName);

        const initialState = freeze({
            certificatesProlongation: {
                CertificatesForProlongation: [
                    {
                        Id: selectedCertificateId,
                        OwnerName: selectedOwnerName,
                        ExpirationDate: "2015.12.12",
                        AttentionRequired: false
                    },
                    {
                        Id: "id2",
                        OwnerName: "OwnerName2",
                        ExpirationDate: "2015.12.11",
                        AttentionRequired: false,
                        selectOwner: true,
                        isChecked: true
                    }
                ],
                selectedCertificates: [{ OldCertificateId: "id2" }]
            }
        });

        it("should set select owner", () => {
            const actual = reducer(initialState, actions.prolongationSetSelectOwner({
                certificateId: selectedCertificateId,
                contacts: contacts
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.CertificatesForProlongation[0].selectOwner).to.be.true;
        });

        it("should update contacts list on set select owner", () => {
            const actual = reducer(initialState, actions.prolongationSetSelectOwner({
                certificateId: selectedCertificateId,
                contacts: contacts,
                OwnerName: selectedOwnerName
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.updatedContacts.length).to.equal(2);
        });

        it("should set active contact on setting select owner", () => {
            const actual = reducer(initialState, actions.prolongationSetSelectOwner({
                certificateId: selectedCertificateId,
                contacts: contacts,
                OwnerName: selectedOwnerName
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.CertificatesForProlongation[0].activeContact).to.deep.equal(emptyContact);
        });

        it("should remove select owner on uncheck", () => {
            const actual = reducer(initialState, actions.prolongationCertificateSelection({
                certificateId: "id2",
                certificatesLimit: 1
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.CertificatesForProlongation[1].selectOwner).to.be.false;
        });

        it("should set NeedChangeContact", () => {
            const actual = reducer(initialState, actions.prolongationSetSelectOwner({
                certificateId: "id2",
                contacts: contacts
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.selectedCertificates[0].NeedChangeContact).to.be.true;
        });

        it("should contain default name", () => {
            const actual = reducer(initialState, actions.prolongationSetSelectOwner({
                certificateId: selectedCertificateId,
                contacts: contacts,
                OwnerName: selectedOwnerName
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.updatedContacts[0].FullName).to.equal(selectedOwnerName);
        });
    });

    describe("change owner", () => {
        const selectedContactId = "contactId1";
        const selectedCertificateId = "id1";

        const initialState = freeze({
            certificatesProlongation: {
                CertificatesForProlongation: [
                    {
                        Id: selectedCertificateId,
                        OwnerName: "OwnerName",
                        ExpirationDate: "2015.12.12",
                        AttentionRequired: false,
                        isChecked: true,
                        selectOwner: true
                    },
                    {
                        Id: "id2",
                        OwnerName: "OwnerName2",
                        ExpirationDate: "2015.12.11",
                        AttentionRequired: false
                    }
                ],
                updatedContacts: [
                    {
                        ContactId: null,
                        FullName: "empty",
                        Post: null
                    },
                    {
                        ContactId: selectedContactId,
                        FullName: "FullName",
                        Post: "Get"
                    }
                ],
                selectedCertificates: [{ OldCertificateId: selectedCertificateId }]
            }
        });

        it("should set selected owner as active contact", () => {
            const actual = reducer(initialState, actions.prolongationSelectOwner({
                certificateId: selectedCertificateId,
                contactId: selectedContactId
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.CertificatesForProlongation[0].activeContact.ContactId).to.equal(selectedContactId);
            expect(actualProlongation.selectedCertificates[0].ContactId).to.equal(selectedContactId);
        });

        it("should set NeedChangeContact on change", () => {
            const actual = reducer(initialState, actions.prolongationSelectOwner({
                certificateId: selectedCertificateId,
                contactId: null
            }));
            const actualProlongation = actual.certificatesProlongation;

            expect(actualProlongation.selectedCertificates[0].NeedChangeContact).to.be.true;
        });
    });

    it("should clear prolongation info", () => {
        const clearState = {
            submitEnabled: false,
            prolongationLinkDisabled: false,
            clientSearchCompleted: false,
            CertificatesForProlongation: []
        };

        const initialState = freeze({
            selectedCertificates: [1, 2],
            whatever: "else"
        });

        const actual = reducer(initialState, actions.closeProlongation());
        const actualProlongation = actual.certificatesProlongation;

        expect(actualProlongation).to.deep.equal(clearState);
    });

    it("should actualize checkboxes", () => {
        const initialState = freeze({
            certificatesProlongation: {
                CertificatesForProlongation: [
                    {
                        Id: "id1",
                        OwnerName: "OwnerName",
                        ExpirationDate: "2015.12.12",
                        AttentionRequired: false
                    },
                    {
                        Id: "id2",
                        OwnerName: "OwnerName2",
                        ExpirationDate: "2015.12.11",
                        AttentionRequired: false
                    },
                    {
                        Id: "id3",
                        OwnerName: "OwnerName3",
                        ExpirationDate: "2015.12.13",
                        AttentionRequired: false
                    }
                ],
                updatedContacts: [
                    {
                        ContactId: null,
                        FullName: "empty",
                        Post: null
                    },
                    {
                        ContactId: "contId1",
                        FullName: "FullName",
                        Post: "Get"
                    }
                ],
                selectedCertificates: [
                    {
                        OldCertificateId: "id1",
                        NeedChangeContact: true,
                        ContactId: null
                    },
                    {
                        OldCertificateId: "id2",
                        NeedChangeContact: true,
                        ContactId: "contId1"
                    }
                ]
            }
        });

        const actual = reducer(
            initialState,
            actions.actualizeCheckboxesStatuses({ certificatesLimit: 2 })
        );
        const actualProlongation = actual.certificatesProlongation;

        expect(actualProlongation.CertificatesForProlongation[0].isChecked).to.be.true;
        expect(actualProlongation.CertificatesForProlongation[0].selectOwner).to.be.true;

        expect(actualProlongation.CertificatesForProlongation[1].isChecked).to.be.true;
        expect(actualProlongation.CertificatesForProlongation[1].selectOwner).to.be.true;

        expect(actualProlongation.CertificatesForProlongation[2].isChecked).to.be.false;
        expect(actualProlongation.CertificatesForProlongation[2].isDisabled).to.be.true;
        expect(actualProlongation.CertificatesForProlongation[2].selectOwner).to.be.false;
    });

    it("should set search string", () => {
        const searchString = "okay google";
        const initialState = freeze({});

        const actual = reducer(initialState, actions.prolongationSearch({ searchString }));
        const actualProlongation = actual.certificatesProlongation;

        expect(actualProlongation.searchString).to.equal(searchString);
    });

    it("should handle client search", () => {
        const initialState = freeze({});

        const actual = reducer(
            initialState,
            actions.searchPayerSuccess({ RecipientErrors: {}, LinksError: {}, CertificateRecipient: {} })
        );
        const actualProlongation = actual.certificatesProlongation;

        expect(actualProlongation.clientSearchCompleted).to.be.true;
        expect(actualProlongation.selectedCertificates.length).to.equal(0);
    });
});
