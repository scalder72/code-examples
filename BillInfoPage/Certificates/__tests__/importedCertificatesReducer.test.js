import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducers/importedCertificatesReducer";

describe("imported certificates reducer", () => {
    it("should set selected certificate id", () => {
        const initialState = freeze({});
        const selectedId = "selectedId";

        const actual = reducer(initialState, actions.selectImportedCertificate(selectedId));

        expect(actual.selectedCertificate).to.equal(selectedId);
    });
});
