import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducer";

describe("certificates bill info reducer", () => {
    it("should handle begin request", () => {
        const initialState = freeze({
            fetchRequired: true,
            isLoading: false
        });

        const actual = reducer(initialState, actions.fetchCertificatesBegin());

        expect(actual.isLoading).to.be.true;
        expect(actual.fetchRequired).to.be.false;
    });

    it("should handle error request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.fetchCertificatesError());

        expect(actual.isLoading).to.be.false;
    });

    it("should handle success request", () => {
        const initialState = freeze({});

        const responseData = freeze({
            Error: 1,
            ApplicationGroups: 2,
            WhateverElse: 3,
            payerId: "payerId",
            contacts: [],
            payer: { CertificateRecipient: { RecipientId: "payerId", Inn: "payerInn", Kpp: "payerKpp", RecipientName: "payerName" } }
        });

        const actual = reducer(initialState, actions.fetchCertificatesSuccess(responseData));

        expect(actual.selectClient).to.not.be.undefined;
        expect(actual.certificateIssue).to.not.be.undefined;
        expect(actual.certificatesProlongation).to.not.be.undefined;
        expect(actual.Error).to.equal(1);
        expect(actual.payerId).to.equal("payerId");
        expect(actual.contacts).to.not.be.undefined;
        expect(actual.isLoading).to.be.false;
        expect(actual.wasLoaded).to.be.true;
        expect(actual.contacts).to.not.be.undefined;
    });
});
