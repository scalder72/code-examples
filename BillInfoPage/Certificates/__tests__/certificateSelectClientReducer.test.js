import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer, { initialState } from "../reducers/certificateSelectClientReducer";

describe("certificate select client reducer", () => {
    const initialSelectClient = freeze(initialState);

    it("should handle open payer", () => {
        const incomingState = freeze({
            something: "else"
        });

        const actual = reducer(incomingState, actions.openSelectPayer());

        expect(actual).to.deep.equal(initialSelectClient);
    });

    it("should handle open payer on adding request", () => {
        const incomingState = freeze({
            something: "else"
        });
        const existingPayer = freeze({
            Inn: "1",
            Kpp: "2",
            RecipientName: "name"
        });
        const expected = {
            ...initialState,
            payerInn: existingPayer.Inn,
            payerKpp: existingPayer.Kpp,
            payerName: existingPayer.RecipientName,
            isPayerExist: true,
            isRequisitesValid: true,
            payer: existingPayer
        };

        const actual = reducer(incomingState, actions.openSelectPayer({preloadedPayer: existingPayer}));

        expect(actual).to.deep.equal(expected);
    });

    it("should handle close payer", () => {
        const incomingState = freeze({
            something: "else"
        });

        const actual = reducer(incomingState, actions.closeSelectPayer());

        expect(actual).to.deep.equal(incomingState);
    });

    it("should handle begin search payer", () => {
        const actual = reducer(initialSelectClient, actions.searchPayerBegin());

        expect(actual.receiverErrors).to.be.null;
        expect(actual.inProcess).to.be.true;
    });

    it("should handle success search payer", () => {
        const response = {
            RecipientErrors: null,
            LinksError: null,
            CertificateRecipient: {b: 2}
        };
        const actual = reducer(initialSelectClient, actions.searchPayerSuccess(response));

        expect(actual.receiverErrors).to.deep.equal(response.RecipientErrors);
        expect(actual.linksError).to.deep.equal(response.LinksError);
        expect(actual.payer).to.deep.equal(response.CertificateRecipient);
        expect(actual.inProcess).to.be.false;
        expect(actual.isPayerExist).to.be.true;
        expect(actual.isRequisitesValid).to.be.true;
    });

    it("should handle error search payer", () => {
        const errors = {a: 1};
        const actual = reducer(initialSelectClient, actions.searchPayerError(errors));

        expect(actual.receiverErrors).to.equal(errors);
        expect(actual.linksError).to.equal(errors);
        expect(actual.payer).to.be.null;
        expect(actual.inProcess).to.be.false;
    });

    it("should handle payer inn change", () => {
        const request = {payerInn: "1", currentInn: "2"};
        const actual = reducer(initialSelectClient, actions.handlePayerInnChange(request));

        expect(actual.payerInn).to.equal(request.currentInn);
        expect(actual.inProcess).to.be.false;
        expect(actual.isPayerExist).to.be.false;
        expect(actual.receiverErrors).to.be.null;
        expect(actual.linksError).to.be.null;
        expect(actual.payer).to.be.null;
    });

    it("should do nothing if inn is not passed", () => {
        const request = {};
        const actual = reducer(initialSelectClient, actions.handlePayerInnChange(request));

        expect(actual).to.equal(initialSelectClient);
    });

    it("should handle payer kpp change", () => {
        const request = {payerKpp: "1", currentKpp: "2"};
        const actual = reducer(initialSelectClient, actions.handlePayerKppChange(request));

        expect(actual.payerKpp).to.equal(request.currentKpp);
        expect(actual.inProcess).to.be.false;
        expect(actual.isPayerExist).to.be.false;
        expect(actual.receiverErrors).to.be.null;
        expect(actual.linksError).to.be.null;
        expect(actual.payer).to.be.null;
    });

    it("should do nothing if kpp is not passed", () => {
        const request = {};
        const actual = reducer(initialSelectClient, actions.handlePayerKppChange(request));

        expect(actual).to.equal(initialSelectClient);
    });

    it("should handle payer name change", () => {
        const request = {payerName: "Name"};
        const actual = reducer(initialSelectClient, actions.handlePayerNameChange(request));

        expect(actual.payerName).to.equal(request.payerName);
    });

    it("should do nothing if name is not passed", () => {
        const request = {};
        const actual = reducer(initialSelectClient, actions.handlePayerNameChange(request));

        expect(actual).to.equal(initialSelectClient);
    });
});
