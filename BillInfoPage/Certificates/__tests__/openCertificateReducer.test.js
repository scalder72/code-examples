import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducers/openCertificateReducer";

describe("open certificate reducer", () => {
    it("should handle begin request", () => {
        const initialState = freeze({
            isLoading: false
        });

        const actual = reducer(initialState, actions.openCertificateBegin());

        expect(actual.isLoading).to.be.true;
    });

    it("should handle error request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.openCertificateError());

        expect(actual.isLoading).to.be.false;
    });

    it("should handle success request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.openCertificateSuccess());

        expect(actual.isLoading).to.be.false;
    });
});
