import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducer";

describe("certificates convert scheme reducer", () => {
    it("should handle begin request", () => {
        const initialState = freeze({
            isLoading: false
        });

        const actual = reducer(initialState, actions.convertSchemeActionBegin());

        expect(actual.isLoading).to.be.true;
    });

    it("should handle error request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.convertSchemeActionError());

        expect(actual.isLoading).to.be.false;
    });

    it("should handle success request", () => {
        const initialState = freeze({});
        const actual = reducer(initialState, actions.convertSchemeActionSuccess());

        expect(actual.isLoading).to.be.false;
    });
});
