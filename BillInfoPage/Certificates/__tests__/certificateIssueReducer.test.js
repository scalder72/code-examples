import { expect } from "chai";
import * as actions from "./../actions";
import freeze from "deep-freeze";
import { certificateIssueReducer } from "../reducers/certificateIssueReducer";
import { __RewireAPI__ as GuidFactoryRewireAPI } from "billing-ui/helpers/GuidFactory";

describe("certificate issue reducer", () => {
    const certificateIds = freeze(["certificateId1", "certificateId2", "certificateId3"]);

    const initialContacts = [
        { ContactId: "contactId1", FullName: "fio", Post: "post" },
        { ContactId: "contactId2", FullName: "fio", Post: "post" },
        { ContactId: "contactId3", FullName: "fio", Post: "post" }
    ];

    const initialPayer = {
        RecipientId: "payerId",
        Inn: "inn",
        Kpp: "kpp",
        RecipientName: "name"
    };

    const initialSlot = freeze({
        uid: "uid",
        contactId: null,
        payerId: "payerId",
        count: 3,
        payerInn: "inn",
        payerKpp: "kpp",
        payerName: "name",
        maxCountToSelect: 3,
        isSelectPayerLightboxOpen: false,
        activeContact: { ContactId: null, FullName: "Без указания владельца", Post: null },
        contacts: [
            { ContactId: null, FullName: "Без указания владельца", Post: null },
            ...initialContacts
        ]
    });

    const initialState = freeze({
        certificateIds: certificateIds,
        reservedSlotsCount: 3,
        inProcess: false,
        payerId: "payerId",
        contactsByPayerIds: { payerId: initialContacts },
        payersByPayerIds: { payerId: initialPayer },
        addCertificateSlot: null,
        slots: [initialSlot]
    });

    it("should handle issue certificate action", () => {
        GuidFactoryRewireAPI.__Rewire__("GuidFactory", { create: () => "uid" });

        const state = {
            payerId: "payerId",
            payersByPayerIds: { payerId: initialPayer },
            contactsByPayerIds: { payerId: initialContacts }
        };
        const actual = certificateIssueReducer(state, actions.openCertificatesIssue({ certificateIds }));
        expect(actual).to.deep.equal(initialState);
        expect(actual.slots[0].maxCountToSelect).to.equal(3);

        GuidFactoryRewireAPI.__ResetDependency__("GuidFactory");
    });

    describe("quantity selection", () => {
        it("should handle certificate quantity selection", () => {
            const actual = certificateIssueReducer(initialState, actions.selectQuantity({
                activeSlot: initialSlot,
                count: 2
            }));

            expect(actual.slots[0].count).to.equal(2);
        });
    });

    describe("owner selection", () => {
        it("should handle certificate owner selection", () => {
            const contactId = "contactId1";

            const actual = certificateIssueReducer(initialState, actions.selectOwner({
                activeSlot: initialSlot,
                contactId
            }));

            expect(actual.slots.length).to.equal(1);
            expect(actual.slots[0].contactId).to.equal(contactId);
            expect(actual.slots[0].count).to.equal(1);
            expect(actual.slots[0].activeContact).to.deep.equal({ ContactId: "contactId1", Post: "post", FullName: "fio" });
            expect(actual.reservedSlotsCount).to.equal(1);
            expect(actual.addCertificateSlot).not.to.be.null;
        });

        it("should exclude selected contacts from selection", () => {
            const actual1 = certificateIssueReducer(initialState, actions.selectOwner({
                activeSlot: initialSlot,
                contactId: "contactId1"
            }));

            expect(actual1.slots[0].contacts.length).to.equal(4);

            const actual2 = certificateIssueReducer(actual1, actions.addCertificate());

            expect(actual2.slots.length).to.equal(2);
            expect(actual2.slots[1].contacts.length).to.equal(3);

            const newSlotContacts = actual2.slots[1].contacts.map((c) => c.ContactId);

            expect(newSlotContacts).not.to.contain("contactId1");
        });

        it("should prepend empty contact into contacts", () => {
            const actual = certificateIssueReducer(initialState, actions.selectOwner({
                activeSlot: initialSlot,
                contactId: "contactId1"
            }));

            expect(actual.slots[0].contacts[0].ContactId).to.be.null;
        });

        it("should merge empty slots", () => {
            const actual1 = certificateIssueReducer(initialState, actions.selectOwner({
                activeSlot: initialSlot,
                contactId: "contactId1"
            }));

            const actual2 = certificateIssueReducer(actual1, actions.addCertificate());

            expect(actual2.slots.length).to.equal(2);
            expect(actual2.slots[1].maxCountToSelect).to.equal(2);

            const actual3 = certificateIssueReducer(actual2, actions.selectOwner({
                activeSlot: actual2.slots[0],
                contactId: null
            }));

            expect(actual3.slots.length).to.equal(1);
            expect(actual3.slots[0].maxCountToSelect).to.equal(3);
            expect(actual3.slots[0].count).to.equal(2);
        });
    });

    describe("applications removing", () => {
        it("should handle slot removing when slot is alone", () => {
            const actual = certificateIssueReducer(initialState, actions.removeCertificate({
                activeSlot: initialSlot
            }));

            expect(actual.slots.length).to.equal(0);
            expect(actual.reservedSlotsCount).to.equal(0);
            expect(actual.addCertificateSlot).not.to.be.null;
        });

        it("should handle removing slot with contact", () => {
            const initialSlot1 = {
                ...initialSlot,
                contactId: "contactId1",
                count: 1,
                maxCountToSelect: 1,
                activeContact: initialContacts[0],
                contacts: [
                    { ContactId: null, FullName: "Без указания владельца", Post: null },
                    ...initialContacts
                ]
            };
            const initialSlot2 = {
                count: 2,
                maxCountToSelect: 2,
                contacts: [
                    { ContactId: null, FullName: "Без указания владельца", Post: null },
                    initialContacts[1],
                    initialContacts[2]
                ]
            };
            const _initialState = {
                ...initialState,
                slots: [initialSlot1, initialSlot2]
            };

            const actual = certificateIssueReducer(_initialState, actions.removeCertificate({
                activeSlot: initialSlot1
            }));

            expect(actual.slots.length).to.equal(1);
            expect(actual.addCertificateSlot).not.to.be.null;
        });
    });
});
