import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducers/performActionReducer";

describe("perform action reducer", () => {
    it("should handle begin request", () => {
        const initialState = freeze({
            isLoading: false
        });

        const actual = reducer(initialState, actions.performActionBegin());

        expect(actual.isLoading).to.be.true;
    });

    it("should handle error request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.performActionError());

        expect(actual.isLoading).to.be.false;
    });

    it("should handle success request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.performActionSuccess());

        expect(actual.isLoading).to.be.false;
    });
});
