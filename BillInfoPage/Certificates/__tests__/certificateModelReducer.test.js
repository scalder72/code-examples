import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducers/certificateModelReducer";

describe("open certificate model reducer", () => {
    it("should show loader on certificate but not global on begin", () => {
        const formId = "formId";
        const groupName = "Forms";
        const initialState = freeze({
            isLoading: false,
            [groupName]: [{
                Id: formId,
                isLoading: false
            }]
        });

        const actual = reducer(
            initialState,
            actions.openCertificateBegin({ certificateId: formId, certificatesGroupName: groupName })
        );

        expect(actual.isLoading).to.be.false;
        expect(actual[groupName][0].isLoading).to.be.true;
    });
});
