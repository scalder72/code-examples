import { expect } from "chai";
import { renderIntoDocument, scryRenderedDOMComponentsWithClass } from "react-addons-test-utils";
import { wrap } from "react-stateless-wrapper"

import InfoMessage, { wrapperClass, convertSchemeClass, printClass } from "../components/Messages/InfoMessage.jsx";
import certificatesErrorAction from "../utils/CertificatesErrorAction";

const WrappedInfoMessage = wrap(InfoMessage);

describe("certificates info message component", () => {
    let baseProps = {
        convertSchemeAction: () => {},
        urls: {
            PrintBillUrl: "PrintBillUrl"
        }
    };

    it("should not render wrapper if message is empty", () => {
        baseProps.Message = "";
        const message = renderIntoDocument(<WrappedInfoMessage {...baseProps} />);

        const wrapper = scryRenderedDOMComponentsWithClass(message, wrapperClass);

        expect(wrapper.length).to.equal(0);
    });

    it("should render only convert button for convert action", () => {
        baseProps.Message = "Message";
        baseProps.Action = certificatesErrorAction.ConvertLegalSchemeAndPrint;
        const message = renderIntoDocument(<WrappedInfoMessage {...baseProps} />);

        const convertButton = scryRenderedDOMComponentsWithClass(message, convertSchemeClass);
        const printButton = scryRenderedDOMComponentsWithClass(message, printClass);


        expect(convertButton.length).to.equal(1);
        expect(printButton.length).to.equal(0);
    });

    it("should render only print button for print action", () => {
        baseProps.Message = "Message";
        baseProps.Action = certificatesErrorAction.Print;
        const message = renderIntoDocument(<WrappedInfoMessage {...baseProps} />);

        const convertButton = scryRenderedDOMComponentsWithClass(message, convertSchemeClass);
        const printButton = scryRenderedDOMComponentsWithClass(message, printClass);

        expect(convertButton.length).to.equal(0);
        expect(printButton.length).to.equal(1);
    });
});
