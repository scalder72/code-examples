const certificatesApplicationReason = {
    New: "New",
    UnplannedChange: "UnplannedChange"
};

export default certificatesApplicationReason;
