import { omitEntityByIndex, findIndex } from "billing-ui/helpers/ArrayHelper";
import { createEmptyContact } from "./helpers";

export const resolveSelectedCertificates = (selectedCertificatesList, certificateId, setChecked) => {
    const certificateIndex = findIndex(cert => cert.OldCertificateId === certificateId, selectedCertificatesList);
    let changedCertificatesList = [ ...selectedCertificatesList ];

    if (certificateIndex >= 0) {
        if (setChecked === true) {
            return changedCertificatesList;
        }

        changedCertificatesList = omitEntityByIndex(certificateIndex, changedCertificatesList);
    } else {
        if (setChecked === false) {
            return changedCertificatesList;
        }

        changedCertificatesList.push({ OldCertificateId: certificateId });
    }

    return changedCertificatesList;
};

export const resolveCheckboxesStatus = (certificatesForProlongation, selectedCertificates, certificatesLimit, updatedContacts) => {
    const limitExceeded = selectedCertificates.length >= certificatesLimit;

    return certificatesForProlongation.map(cert => {
        const selectedCertificateIndex = findIndex(oldCert => oldCert.OldCertificateId === cert.Id, selectedCertificates);
        let selectedCertificate = null;
        let activeContact = null;
        const isChecked = selectedCertificateIndex >= 0;
        const isDisabled = limitExceeded && !isChecked;

        if (isChecked && updatedContacts) {
            selectedCertificate = selectedCertificates[selectedCertificateIndex];
            const activeContactIndex = findIndex(contact => contact.ContactId === selectedCertificate.ContactId, updatedContacts);
            activeContact = updatedContacts[activeContactIndex];
        }

        return {
            ...cert,
            isChecked: isChecked,
            isDisabled: isDisabled,
            selectOwner: isChecked && (cert.selectOwner || selectedCertificate && selectedCertificate.NeedChangeContact),
            activeContact: cert.activeContact || activeContact
        };
    });
};

export const generateContacts = (contacts, OwnerName) => {
    return [
        createEmptyContact(OwnerName),
        ...contacts
    ];
};

export const resolveSetSelectOwner = (certificatesForProlongation, certificateId, OwnerName) => {
    return certificatesForProlongation.map(cert => {
        return {
            ...cert,
            selectOwner: cert.Id === certificateId || cert.selectOwner,
            activeContact: cert.activeContact || (cert.Id === certificateId && createEmptyContact(OwnerName))
        };
    });
};

export const setActiveContact = (certificatesForProlongation, certificateId, activeContact) => {
    return certificatesForProlongation.map(cert => {
        return {
            ...cert,
            activeContact: cert.Id === certificateId ? activeContact : cert.activeContact
        };
    });
};

export const setContactIdToSelectedCertificates = (selectedCertificates, certificateId, contactId) => (selectedCertificates.map(
    cert => {
        if (cert.OldCertificateId === certificateId) {
            return {
                ...cert,
                ContactId: contactId,
                NeedChangeContact: true
            }
        }
        return cert;
    }
));
