import { IconTypes } from "billing-ui/components/Icon";
import { enumInfoMapper } from "billing-ui/helpers/ObjectHelpers";

const certificateActionType = {
    GoToCA: "GoToCA",
    Download: "Download",
    Republish: "Republish",
    Reissue: "Reissue",
    Delete: "Rollback"
};

certificateActionType.getIconType = enumInfoMapper({
    [certificateActionType.GoToCA]: IconTypes.Login,
    [certificateActionType.Download]: IconTypes.Download,
    [certificateActionType.Republish]: IconTypes.Refresh,
    [certificateActionType.Reissue]: IconTypes.Refresh,
    [certificateActionType.Delete]: IconTypes.Trash
});

certificateActionType.getGAOptions = enumInfoMapper({
    [certificateActionType.Delete]: {category: "bill-certs", action: "delete", label: "delete-form"}
});

export default certificateActionType;
