import { INDIVIDUAL_INN_LENGTH, LEGAL_INN_LENGTH } from "./const";
import _ from "underscore";

const INN_WEIGHTS = [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8];
const DIGITS_ONLY_REGEXP = /^\d+$/;

const matchCheckSum = (innDigits, count) => {
    let sum = 0;
    const shift = 11 - count;
    for (let i = 0; i < count; i++) {
        sum += INN_WEIGHTS[shift + i] * innDigits[i];
    }
    return sum % 11 % 10 === innDigits[count];
};

const matchInnCheckSum = _.memoize((inn) => {
    const innDigits = Array.prototype.slice.call(inn).map(char => parseInt(char, 10));
    const isInnLegal = innDigits.length === LEGAL_INN_LENGTH;

    return isInnLegal ? matchCheckSum(innDigits, 9) : (matchCheckSum(innDigits, 10) && matchCheckSum(innDigits, 11));
});

const createValidationResult = (isValid, errorMessage) => ({ isValid, errorMessage });

export const validateInn = (inn) => {
    if (inn.length !== LEGAL_INN_LENGTH && inn.length !== INDIVIDUAL_INN_LENGTH) {
        return createValidationResult(false, "");
    }

    if (!DIGITS_ONLY_REGEXP.test(inn)) {
        return createValidationResult(false, "Некорректный ИНН");
    }

    if (inn === "0000000000" || inn === "000000000000") {
        return createValidationResult(false, "Некорректный ИНН");
    }

    if (!matchInnCheckSum(inn)) {
        return createValidationResult(false, "Некорректный ИНН");
    }

    return createValidationResult(true);
};

export default {
    validateInn
}


