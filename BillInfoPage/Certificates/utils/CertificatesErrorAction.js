﻿const certificatesInfoErrorAction = {
    None: "None",
    Print: "Print",
    ConvertLegalSchemeAndPrint: "ConvertLegalSchemeAndPrint"
};

export default certificatesInfoErrorAction;
