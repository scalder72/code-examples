import { chain, sortBy } from "underscore";
import { replaceByIndex, findIndex } from "billing-ui/helpers/ArrayHelper";
import { createEmptyContact } from "./helpers";
import { createGuid } from "billing-ui/helpers/GuidFactory";

export const createEmptySlot = (count, maxCountToSelect, payerId, payerInfo = {}) => ({
    uid: createGuid(),
    count,
    payerId,
    maxCountToSelect,
    contactId: null,
    activeContact: createEmptyContact(),
    isSelectPayerLightboxOpen: false,
    payerInn: payerInfo.Inn,
    payerKpp: payerInfo.Kpp,
    payerName: payerInfo.RecipientName
});

export const updateContacts = (contactsByPayerIds, allSlots) => {
    const contactsByContactId = Object.keys(contactsByPayerIds)
        .reduce((result, payerId) => [...result, ...contactsByPayerIds[payerId]], [])
        .reduce((result, contact) => {
            if (result[contact.ContactId] === undefined) {
                result[contact.ContactId] = contact;
            }

            return result;
        }, {});

    const reservedContactsIds = allSlots.reduce((result, slot) => {
        if (result[slot.payerId] === undefined) {
            result[slot.payerId] = {};
        }

        if (slot.contactId !== null) {
            result[slot.payerId][slot.contactId] = slot.contactId;
        }

        return result;
    }, {});

    const freeContacts = Object
        .keys(contactsByPayerIds)
        .filter((payerId) => reservedContactsIds[payerId] !== undefined)
        .reduce((result, payerId) => {
            const reservedContactIds = reservedContactsIds[payerId];
            result[payerId] = contactsByPayerIds[payerId].filter((contact) => reservedContactIds[contact.ContactId] === undefined);
            return result;
        }, {});

    return allSlots.map((slot) => {
        const isEmptySlot = slot.contactId === null;
        const firstContact = !isEmptySlot ? contactsByContactId[slot.contactId] : createEmptyContact();
        const restContacts = freeContacts[slot.payerId] || [];

        return {
            ...slot,
            activeContact: firstContact,
            contacts: isEmptySlot ? [firstContact, ...restContacts] : [createEmptyContact(), firstContact, ...restContacts]
        };
    });
};

export const actualizeMaxCountToSelect = (certificateIds, reservedSlotsCount, slots) => {
    return slots.map((slot) => {
        if (slot.contactId !== null) {
            return slot;
        }

        return { ...slot, maxCountToSelect: certificateIds.length - reservedSlotsCount + slot.count };
    });
};

export const mergeSlotToSlots = (slot, oldSlot, slots = []) => {
    let newSlots;

    if (oldSlot !== null) {
        const slotIndex = findIndex(slot => oldSlot === slot, slots);
        newSlots = replaceByIndex(slot, slotIndex, slots);
    } else if (slot !== null) {
        newSlots = [...slots, slot];
    } else {
        newSlots = slots;
    }

    return chain(newSlots)
        .filter((slot) => slot !== null)
        .groupBy((slot) => slot.payerId)
        .reduce((result, groupedSlots, payerId) => result.concat(sortBy(groupedSlots, (slot) => slot.contactId === null)), [])
        .groupBy((slot) => `${slot.payerId}_${slot.contactId}`)
        .values()
        .reduce((result, groupedSlots) => {
            if (groupedSlots.length === 1) {
                return result.concat(groupedSlots);
            }

            const computedCount = groupedSlots.reduce((result, slot) => result + slot.count, 0);
            result.push({ ...groupedSlots[0], count: computedCount });

            return result;
        }, [])
        .value();
};

export const updateSlots = (slot, oldSlot, state) => {
    const { slots, certificateIds, contactsByPayerIds, payerId, payersByPayerIds } = state;
    const newSlots = mergeSlotToSlots(slot, oldSlot, slots, payerId);

    const reservedSlotsCount = newSlots.reduce((result, slot) => result + slot.count, 0);
    const hasFreeCertificates = certificateIds.length - reservedSlotsCount > 0;

    const slotsWithContacts = updateContacts(contactsByPayerIds, newSlots);
    const slotsWithMaxCountToSelect = actualizeMaxCountToSelect(certificateIds, reservedSlotsCount, slotsWithContacts);

    return {
        ...state,
        reservedSlotsCount,
        slots: slotsWithMaxCountToSelect,
        addCertificateSlot: hasFreeCertificates ? createEmptySlot(1, 1, payerId, payersByPayerIds[payerId]) : null
    };
};

export const createInitialState = (initialState, { payerId, contactsByPayerIds, payersByPayerIds }) => ({
    ...initialState,
    payerId,
    contactsByPayerIds,
    payersByPayerIds
});
