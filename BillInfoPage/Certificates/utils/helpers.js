export const createEmptyContact = (defaultFullName = "Без указания владельца") => ({
    ContactId: null,
    FullName: defaultFullName,
    Post: null
});
