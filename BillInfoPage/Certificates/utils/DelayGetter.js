class DelayGetter {
    constructor({ maxDelay = Infinity, minDelay = 100, delayRate = 1 } = {}) {
        this._maxDelay = maxDelay;
        this._minDelay = minDelay;
        this._delayRate = delayRate;

        this._lastDelay = minDelay;
    }

    nextDelay() {
        const nextDelay = Math.floor(this._lastDelay * this._delayRate);
        if (nextDelay < this._maxDelay) {
            this._lastDelay = nextDelay;
        } else {
            this._lastDelay = this._maxDelay;
        }

        return this._lastDelay;
    }

    resetDelay() {
        this._lastDelay = this._minDelay;
    }
}

export const createDelayGetter = (options) => new DelayGetter(options);
