﻿const certificatesInfoErrorLevel = {
    None: "None",
    Info: "Info",
    Error: "Error"
};

export default certificatesInfoErrorLevel;
