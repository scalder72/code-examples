import { enumInfoMapper } from "billing-ui/helpers/ObjectHelpers"

const PayerTypes = {
    LegalEntity: "LegalEntity",
    IndividualBusinessman: "IndividualBusinessman",
    PhysicalPerson: "PhysicalPerson"
};

PayerTypes.getDescription = enumInfoMapper({
    [PayerTypes.LegalEntity]: "Юр. лицо",
    [PayerTypes.IndividualBusinessman]: "ИП",
    [PayerTypes.PhysicalPerson]: "Физ. лицо"
});

PayerTypes.getCaption = enumInfoMapper({
    [PayerTypes.ClientType]: ["Физ. лицо", "ИП"]
});

export default PayerTypes;
