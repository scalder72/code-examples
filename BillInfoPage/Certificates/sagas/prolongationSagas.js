import axios from "billing-ui/libs/axios";
import { takeLatest } from "redux-saga";
import { put, select } from "redux-saga/effects";
import * as actionTypes from "../actionTypes";
import * as actions from "../actions";

import Informer from "Informer";
import * as settings from "../utils/settings";

const buildFetchProlongationParams = (certificatesInfo, action) => {
    const { selectClient, certificatesProlongation, payerId } = certificatesInfo;
    const { certificateType, certificateReason, searchString } = certificatesProlongation;

    const defaultQueryParams = {
        RecipientId: payerId,
        CertificateType: certificateType,
        CertificateReason: certificateReason,
        PageNumber: 1,
        PageSize: settings.PROLONGATION_PAGE_SIZE,
        SearchString: ""
    };

    switch (action.type) {
        case actionTypes.SEARCH_PAYER_SUCCESS:
            return {
                ...defaultQueryParams,
                RecipientId: selectClient.payer && selectClient.payer.RecipientId || null
            };

        case actionTypes.FILTER_PROLONGATION_CERTIFICATES:
            const pageNumber = action.payload && action.payload.pageNumber;
            return {
                ...defaultQueryParams,
                RecipientId: certificatesProlongation.payerId || payerId,
                PageNumber: pageNumber || 1,
                SearchString: searchString
            };

        default:
            return defaultQueryParams;
    }
};

export function* certificatesProlongationWatcher() {
    const actionsToWatch = [
        actionTypes.SEARCH_PAYER_SUCCESS,
        actionTypes.OPEN_PROLONGATION,
        actionTypes.FILTER_PROLONGATION_CERTIFICATES
    ];

    yield* takeLatest(actionsToWatch, function* handleTask(action) {
        const { certificatesInfo, urls } = yield select(({ billInfo }) => billInfo);
        const { CertificateGetForProlongationUrl } = urls;
        const { certificateIds, certificatesLimit } = certificatesInfo.certificatesProlongation;

        const fetchParams = buildFetchProlongationParams(certificatesInfo, action);
        yield fetchProlongationCertificates(CertificateGetForProlongationUrl, certificatesLimit, certificateIds, fetchParams);
    });
}

function* fetchProlongationCertificates(url, certificatesLimit, certificateIds, query) {
    yield put(actions.fetchProlongationBegin());

    try {
        const prolongationResponse = yield axios.get(url, { params: query });
        yield put(actions.fetchProlongationSuccess({ ...prolongationResponse.data, certificateIds }));
        if (certificatesLimit) {
            yield put(actions.actualizeCheckboxesStatuses({ certificatesLimit }));
        }
    } catch (e) {
        Informer.showError("Не удалось получить список сертификатов для продления");
        yield put(actions.fetchProlongationError({ error: e }));
    }
}
