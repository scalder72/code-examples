import axios from "billing-ui/libs/axios";
import { takeLatest } from "redux-saga";
import { put, select } from "redux-saga/effects";
import * as actionTypes from "../actionTypes";
import * as actions from "../actions";

export function* certificateSelectClientWatcher() {
    const actionsToWatch = [
        actionTypes.SEARCH_PAYER_SUCCESS,
        actionTypes.SELECT_PAYER_SUCCESS,
        actionTypes.OPEN_PROLONGATION
    ];

    yield* takeLatest(actionsToWatch, function* handleTask(action) {
        const { certificatesInfo, urls: { ContactsSelectionUrl } } = yield select(({ billInfo }) => billInfo);
        const { payerId } = action.payload;

        try {
            const contactsResponse = yield axios.get(ContactsSelectionUrl, { params: { id: payerId || certificatesInfo.payerId } });
            yield put(actions.fetchContactsSuccess({
                payerId,
                contacts: contactsResponse.data
            }));
        } catch (e) {
            throw new Error(e);
        }
    });
}
