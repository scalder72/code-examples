import axios from "billing-ui/libs/axios";
import { takeLatest, delay } from "redux-saga";
import { take, call, put, fork, cancel, select } from "redux-saga/effects";
import { LOCATION_CHANGE } from "billing-ui/libs/react-router-redux";
import { certificatesProlongationWatcher } from "./sagas/prolongationSagas";
import { certificateSelectClientWatcher } from "./sagas/selectClientSagas";

import * as actionTypes from "./actionTypes";
import * as actions from "./actions";
import { createDelayGetter } from "./utils/DelayGetter";

const delayGetter = createDelayGetter({
    minDelay: 3 * 1000,
    maxDelay: 600 * 1000 / 2,
    delayRate: 1.5
});

function* fetchCertificates() {
    const { CertificatesUrl } = yield select(({ billInfo }) => billInfo.urls);
    yield put(actions.fetchCertificatesBegin({ silent: true }));

    try {
        const certificatesResponse = yield axios.get(CertificatesUrl);
        yield put(actions.fetchCertificatesSuccess({ ...certificatesResponse.data }));
    } catch (e) {
        yield put(actions.fetchCertificatesError(e));
    }
}

function* certificatesAutoSync() {
    try {
        while (true) {
            yield call(delay, delayGetter.nextDelay());
            const { autoSync, isLoading } = yield select(({ billInfo }) => billInfo.certificatesInfo);

            if (autoSync && !isLoading) {
                yield fetchCertificates();
            }
        }
    } catch (e) {
        // error?
    }
}
const isCertificatesPage = (pathname) => /billinfo\/[0-9a-z-]{36}\/certificates/ig.test(pathname);

function* certificatePageLocationWatcher() {
    while (true) {
        const action = yield take(LOCATION_CHANGE);

        if (isCertificatesPage(action.payload.pathname)) {
            yield put(actions.turnCertificatesAutoSyncOn());
        } else {
            yield put(actions.turnCertificatesAutoSyncOff());
        }
    }
}

function* certificatesSyncOnWatcher() {
    const syncAllowingActions = [
        actionTypes.CERTIFICATES_ISSUE_SUCCESS,
        actionTypes.CERTIFICATES_ISSUE_ERROR,
        actionTypes.CLOSE_CERTIFICATES_ISSUE,
        actionTypes.PROLONGATE_CERTIFICATES_SUCCESS,
        actionTypes.CLEAR_PROLONGATION_INFO,
        actionTypes.PERFORM_ACTION_SUCCESS,
        actionTypes.PERFORM_ACTION_ERROR,
        actionTypes.SAVE_IMPORTED_CERTIFICATE_SUCCESS
    ];

    yield* takeLatest(syncAllowingActions, function*() {
        yield put(actions.turnCertificatesAutoSyncOn());
    });
}

function* certificatesSyncOffWatcher() {
    const syncBlockingActions = [
        actionTypes.OPEN_CERTIFICATES_ISSUE,
        actionTypes.CONVERT_SCHEME_ACTION_BEGIN,
        actionTypes.PERFORM_ACTION_BEGIN,
        actionTypes.FETCH_PROLONGATION_BEGIN
    ];

    yield* takeLatest(syncBlockingActions, function*() {
        yield put(actions.turnCertificatesAutoSyncOff());
    });
}

function* certificatesFetchWatcher() {
    const syncBlockingActions = [
        actionTypes.PERFORM_ACTION_SUCCESS,
        actionTypes.CERTIFICATES_ISSUE_SUCCESS,
        actionTypes.PROLONGATE_CERTIFICATES_SUCCESS,
        actionTypes.CONVERT_SCHEME_ACTION_SUCCESS,
        actionTypes.SAVE_IMPORTED_CERTIFICATE_SUCCESS,
        actionTypes.SEND_SELECTED_CERTIFICATE_SUCCESS
    ];

    yield* takeLatest(syncBlockingActions, function*() {
        yield fetchCertificates();
    });
}

function* toggleCertificatesPageAutoSync() {
    let bgSyncTask = null;

    const taskToListen = [
        actionTypes.CERTIFICATES_SYNC_TURNED_ON,
        actionTypes.CERTIFICATES_SYNC_TURNED_OFF
    ];

    yield* takeLatest(taskToListen, function* autoSyncWorker(action) {
        if (action.type === actionTypes.CERTIFICATES_SYNC_TURNED_ON && bgSyncTask === null) {
            bgSyncTask = yield fork(certificatesAutoSync);
        } else if (action.type === actionTypes.CERTIFICATES_SYNC_TURNED_OFF && bgSyncTask !== null) {
            yield cancel(bgSyncTask);
            delayGetter.resetDelay();
            bgSyncTask = null;
        }
    });
}

export function* certificatesRootSaga() {
    yield [
        toggleCertificatesPageAutoSync(),
        certificatePageLocationWatcher(),
        certificatesSyncOnWatcher(),
        certificatesSyncOffWatcher(),
        certificatesFetchWatcher(),
        certificatesProlongationWatcher(),
        certificateSelectClientWatcher()
    ];
}
