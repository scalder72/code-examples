import { Component, PropTypes, cloneElement } from "react";

class ReduxStoreContextProvider extends Component {
    static childContextTypes = {
        store: PropTypes.object
    };

    getChildContext() {
        return { store: this.props.store };
    }

    render() {
        return cloneElement(this.props.children);
    }
}

ReduxStoreContextProvider.propTypes = {
    store: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired
};

export default ReduxStoreContextProvider;
