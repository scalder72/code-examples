import { Component, PropTypes } from "react";
import shouldPureComponentUpdate from "react-pure-render/function";

import ActionsComponent, { Action } from "billing-ui/components/Actions";
import Link from "billing-ui/components/Link";

import certificateActionType from "../utils/CertificateActionType";

const getLinkTarget = actionType => {
    return actionType === certificateActionType.GoToCA ? "_blank" : null;
};

class CertificateActionsWrapper extends Component {
    shouldPureComponentUpdate = shouldPureComponentUpdate;
    _actionsTarget = null;

    state = {
        shouldPopupUpdate: false
    };

    shouldComponentUpdate({ shouldUpdate }, { shouldPopupUpdate }) {
        return shouldUpdate || shouldPopupUpdate;
    }

    _renderSingleAction() {
        const { Actions, styles } = this.props;
        const action = Actions[0];

        return (
            <div className={styles["action-col"]}>
                {action.ActionUrl ? (
                    <Link
                        className={styles.single}
                        href={action.ActionUrl}
                        target={getLinkTarget(action.ActionType)}
                        onClick={evt => { evt.stopPropagation() }}>
                        {action.ActionTitle}
                    </Link>
                ) : (
                    <Link className={styles.single} onClick={evt => { evt.stopPropagation(); this._resolveOnClick(action.ActionType) }}>
                        {action.ActionTitle}
                    </Link>
                )}
            </div>
        );
    }

    _renderActionsPopup() {
        const { Actions, onOpen, styles } = this.props;

        return (
            <div>
                <div className={styles["actions-col"]} ref={node => { this._actionsTarget = node }} onClick={evt => { evt.stopPropagation() }}>
                    <div className={styles.ellipsis}>…</div>
                </div>

                <ActionsComponent getBindItem={() => this._actionsTarget}
                                  position={{top: 0, right: 0}}
                                  onOpen={onOpen}
                                  onClose={() => { this._closePopup() }}
                                  ellipsisClassName={styles["popup-ellipsis"]}
                >
                    {Actions.map(action => action.ActionUrl ? (
                        <Action key={action.ActionType}
                                description={action.ActionTitle}
                                iconType={certificateActionType.getIconType(action.ActionType)}
                                asLink={!!action.ActionUrl}
                                href={action.ActionUrl}
                                target={getLinkTarget(action.ActionType)}
                                onClick={() => {}}
                        />
                    ) : (
                        <Action
                            key={action.ActionType}
                            description={action.ActionTitle}
                            iconType={certificateActionType.getIconType(action.ActionType)}
                            onClick={() => { this._resolveOnClick(action.ActionType) }} />
                    ))}
                </ActionsComponent>
            </div>
        );
    }

    _resolveOnClick(actionType) {
        const { performAction, certificateId, certificatesGroupName } = this.props;

        performAction(actionType, certificateId, certificatesGroupName);
        this._closePopup();
    }

    _closePopup() {
        const { onClose } = this.props;
        const { shouldPopupUpdate } = this.state;

        if (!shouldPopupUpdate) {
            this.setState({
                shouldPopupUpdate: true
            });
        }

        onClose();

        this.setState({
            shouldPopupUpdate: false
        });
    }

    render() {
        const { Actions } = this.props;
        const shouldRenderPopup = Actions.length > 1;

        return (
            <div>
                {shouldRenderPopup ? this._renderActionsPopup() : this._renderSingleAction()}
            </div>
        );
    }
}

CertificateActionsWrapper.propTypes = {
    Actions: PropTypes.arrayOf(PropTypes.shape({
        ActionTitle: PropTypes.string,
        ActionType: PropTypes.oneOf(Object.keys(certificateActionType).map(key => certificateActionType[key])),
        ActionUrl: PropTypes.string
    })).isRequired,
    styles: PropTypes.object.isRequired,
    performAction: PropTypes.func.isRequired,
    shouldUpdate: PropTypes.bool.isRequired,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    certificateId: PropTypes.string.isRequired,
    certificatesGroupName: PropTypes.string.isRequired
};

CertificateActionsWrapper.defaultProps = {
    shouldUpdate: true
};

export default CertificateActionsWrapper;
