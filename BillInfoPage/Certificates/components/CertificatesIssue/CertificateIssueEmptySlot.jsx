import { Component, PropTypes } from "react";

import CertificateIssueLightbox from "./CertificateIssueLightbox";
import Link from "billing-ui/components/Link";
import Icon from "billing-ui/components/Icon";
import { IconTypes } from "billing-ui/components/Icon";

import styles from "../../styles/CertificatesIssue/CertificateIssueEmptySlot.scss";

class CertificateIssueEmptySlot extends Component {
    _handleClick() {
        const { slot, canSelectPayer, addCertificate, openSelectPayer, existingPayer } = this.props;

        if (!canSelectPayer) {
            addCertificate({ activeSlot: slot });
        } else {
            openSelectPayer({ activeSlot: slot, isEmptySlot: true, preloadedPayer: existingPayer });
        }
    }

    render() {
        const { slot, canSelectPayer, selectPayer, closeSelectPayer, AvailableRecipientTypes } = this.props;
        const { isSelectPayerLightboxOpen } = slot;

        return (
            <div>
                <Link className={styles.link} onClick={() => this._handleClick()}>
                    <Icon type={IconTypes.Add} className={styles.icon} />
                    Добавить заявку на сертификат
                </Link>

                { canSelectPayer && isSelectPayerLightboxOpen && (
                    <CertificateIssueLightbox isOpen={true}
                                              onClose={() => isSelectPayerLightboxOpen && closeSelectPayer({ activeSlot: slot, isEmptySlot: true })}
                                              onClientSelect={(payerInfo) => selectPayer({ activeSlot: slot, isEmptySlot: true, payerInfo })}
                                              AvailableRecipientTypes={AvailableRecipientTypes}
                    />
                )}
            </div>
        );
    }

}

CertificateIssueEmptySlot.propTypes = {
    canSelectPayer: PropTypes.bool.isRequired,
    AvailableRecipientTypes: PropTypes.array,
    slot: PropTypes.object.isRequired,
    existingPayer: PropTypes.object,

    openSelectPayer: PropTypes.func.isRequired,
    closeSelectPayer: PropTypes.func.isRequired,
    selectPayer: PropTypes.func.isRequired,
    addCertificate: PropTypes.func.isRequired
};

export default CertificateIssueEmptySlot;
