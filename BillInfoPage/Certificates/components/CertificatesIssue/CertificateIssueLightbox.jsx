import { Component, PropTypes } from "react";
import Lightbox from "billing-ui/components/Lightbox/Lightbox2";
import CertificateSelectClient from "./../CertificateSelectClient/CertificateSelectClient"

class CertificateInfo extends Component {
    render() {
        const { store } = this.context;
        const { isOpen, getOpenLink, onClose, onClientSelect, AvailableRecipientTypes } = this.props;

        return (
            <Lightbox isOpen={isOpen}
                      getOpenLink={getOpenLink}
                      onClose={() => onClose()}
                      title={"Клиент для выпуска сертификата"}
                      width={590}
            >
                <CertificateSelectClient store={store} onClientSelect={onClientSelect} onCancel={onClose} AvailableRecipientTypes={AvailableRecipientTypes} />
            </Lightbox>
        );
    }
}

CertificateInfo.propTypes = {
    AvailableRecipientTypes: PropTypes.array,
    isOpen: PropTypes.bool,
    getOpenLink: PropTypes.func,
    onClose: PropTypes.func,
    onClientSelect: PropTypes.func
};

CertificateInfo.defaultProps = {
    isOpen: false
};

CertificateInfo.contextTypes = {
    store: PropTypes.object
};

export default CertificateInfo;
