import { Component, PropTypes } from "react";
import TransitionGroup from "react-addons-transition-group";
import shouldPureComponentUpdate from "react-pure-render/function";

import Button from "billing-ui/components/Button";
import CertificateIssueSlot from "./CertificateIssueSlot.jsx";
import CertificateIssueEmptySlot from "./CertificateIssueEmptySlot.jsx";

import { slideToggle } from "../Animations/SlideToggle";
const AnimatedCertificateIssueSlot = slideToggle(CertificateIssueSlot);
const AnimatedCertificateIssueEmptySlot = slideToggle(CertificateIssueEmptySlot);

import styles from "../../styles/CertificatesIssue/CertificatesIssueForm.scss";
import { resolveString } from "billing-ui/helpers/PluralStringResolver";

const createSubmitCaption = (slots) => {
    const totalCount = slots.reduce((result, slot) => result + slot.count, 0);

    if (totalCount === 0) {
        return "Опубликовать";
    }

    return `Опубликовать ${totalCount} ${resolveString(totalCount, ["заявку", "заявки", "заявок"])}`;
};

class CertificatesIssueForm extends Component {
    shouldPureComponentUpdate = shouldPureComponentUpdate;

    render() {
        const { slots, addCertificateSlot, canSelectPayer, selectQuantity, certificatesIssue, selectOwner, AvailableRecipientTypes, existingPayer,
                  selectPayer, openSelectPayer, closeSelectPayer, removeCertificate, addCertificate } = this.props;

        const hasSlots = slots.length > 0;

        return (
            <div>
                <h3 className={styles.title}>Новый сертификат</h3>
                <TransitionGroup>
                    {slots.map(slot => (
                        <AnimatedCertificateIssueSlot key={slot.uid}
                                                      slot={slot}
                                                      selectOwner={selectOwner}
                                                      selectQuantity={selectQuantity}
                                                      selectPayer={selectPayer}
                                                      removeCertificate={removeCertificate}
                                                      openSelectPayer={openSelectPayer}
                                                      closeSelectPayer={closeSelectPayer}
                                                      canSelectPayer={canSelectPayer}
                                                      AvailableRecipientTypes={AvailableRecipientTypes}
                        />
                    ))}

                    {addCertificateSlot && (
                        <AnimatedCertificateIssueEmptySlot slot={addCertificateSlot}
                                                           addCertificate={addCertificate}
                                                           selectPayer={selectPayer}
                                                           openSelectPayer={openSelectPayer}
                                                           closeSelectPayer={closeSelectPayer}
                                                           canSelectPayer={canSelectPayer}
                                                           AvailableRecipientTypes={AvailableRecipientTypes}
                                                           existingPayer={existingPayer}
                        />
                    )}
                </TransitionGroup>

                <Button className={styles["send-button"]}
                        disabled={!hasSlots}
                        onClick={() => hasSlots && certificatesIssue()}>
                    {createSubmitCaption(slots)}
                </Button>
            </div>
        );
    }
}

CertificatesIssueForm.propTypes = {
    canSelectPayer: PropTypes.bool.isRequired,
    AvailableRecipientTypes: PropTypes.array,
    certificateIds: PropTypes.array.isRequired,
    reservedSlotsCount: PropTypes.number.isRequired,
    slots: PropTypes.arrayOf(
        PropTypes.shape({
            uid: PropTypes.string,
            count: PropTypes.number
        })
    ),

    addCertificateSlot: PropTypes.object,
    existingPayer: PropTypes.object,

    addCertificate: PropTypes.func.isRequired,
    removeCertificate: PropTypes.func.isRequired,
    selectQuantity: PropTypes.func.isRequired,
    certificatesIssue: PropTypes.func.isRequired,
    selectOwner: PropTypes.func.isRequired,
    selectPayer: PropTypes.func.isRequired,
    openSelectPayer: PropTypes.func.isRequired,
    closeSelectPayer: PropTypes.func.isRequired
};

export default CertificatesIssueForm;
