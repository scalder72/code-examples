import { Component, PropTypes } from "react";
import reactDOM from "react-dom";

import Loader from "react-ui/Loader";
import Popup from "billing-ui/components/PopupWrapper";
import ReduxStoreContextProvider from "./../Helpers/ReduxStoreContextProvider";

import styles from "../../styles/CertificatesIssue/CertificatesApplicationPopup.scss";

class CertificatesApplicationPopup extends Component {
    render() {
        const { onClose, onOpen, children, isActive, showLoader, shouldOpenOnDidMount, shouldUpdateChildren } = this.props;
        const { store } = this.context;

        return (
            <div className="js-popup-base" style={{margin: "0 -40px", padding: "0 40px", position: "relative"}}>
                <Popup isActive={isActive}
                       getBindItem={() => reactDOM.findDOMNode(this)}
                       position={{ top: "20px", left: 0 }}
                       onClose={() => onClose()}
                       onOpen={() => onOpen()}
                       getCloseLink={() => this._closeLink}
                       className={styles.wrapper}
                       shouldOpenOnDidMount={shouldOpenOnDidMount}
                       shouldUpdateChildren={shouldUpdateChildren}
                       fixed={true}>
                    <div style={{margin: "-20px -40px"}}>
                        <Loader active={showLoader} caption="">
                            <div style={{padding: "20px 40px"}}>
                                <div className={styles["close-link"]} ref={(el) => { this._closeLink = el; }} />
                                <ReduxStoreContextProvider store={store}>
                                    {children}
                                </ReduxStoreContextProvider>
                            </div>
                        </Loader>
                    </div>
                </Popup>
            </div>
        )
    }
}

CertificatesApplicationPopup.propTypes = {
    onClose: PropTypes.func.isRequired,
    onOpen: PropTypes.func.isRequired,
    isActive: PropTypes.bool.isRequired,
    showLoader: PropTypes.bool.isRequired,
    shouldOpenOnDidMount: PropTypes.bool.isRequired,
    shouldUpdateChildren: PropTypes.bool.isRequired,
    children: PropTypes.node
};


CertificatesApplicationPopup.contextTypes = {
    store: PropTypes.object
};

export default CertificatesApplicationPopup;
