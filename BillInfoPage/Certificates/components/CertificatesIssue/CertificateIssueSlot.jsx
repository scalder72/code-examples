import { Component, PropTypes } from "react";
import _ from "underscore";
import shouldPureComponentUpdate from "react-pure-render/function";

import { Dropdown, Option } from "billing-ui/components/Dropdown";
import Link from "billing-ui/components/Link";
import Icon from "billing-ui/components/Icon";
import { IconTypes } from "billing-ui/components/Icon";

import CertificateIssueLightbox from "./CertificateIssueLightbox";

import styles from "../../styles/CertificatesIssue/CertificateIssueSlot.scss";

class CertificateIssueSlot extends Component {
    shouldPureComponentUpdate = shouldPureComponentUpdate;

    render() {
        const { slot, removeCertificate, selectOwner, selectQuantity, selectPayer, openSelectPayer, closeSelectPayer, canSelectPayer,
            AvailableRecipientTypes } = this.props;
        const { activeContact, payerName, payerInn, payerKpp, isSelectPayerLightboxOpen } = slot;

        return (
            <div className={styles.slot}>
                <div className={styles.wrapper}>
                    {canSelectPayer && (
                        <div>
                            <Link className={styles["payer-link"]} onClick={() => !isSelectPayerLightboxOpen && openSelectPayer({ activeSlot: slot})}>
                                Сменить получателя
                            </Link>
                            <div className={styles["payer-container"]}>
                                <div className={styles["payer-requisites"]}>
                                    {payerInn}
                                    {payerKpp && <span>-{payerKpp}</span>}
                                </div>
                                <div className={styles["payer-name"]}>{payerName}</div>
                            </div>
                        </div>
                    )}

                    <div className={styles["contact-row"]}>
                        <Dropdown defaultCaption={activeContact.FullName}
                                  value={activeContact.ContactId}
                                  width="90%"
                                  className={styles["owner-dropdown"]}
                                  onSelect={(value) => selectOwner({ activeSlot: slot, contactId: value }) }
                        >
                            {slot.contacts.map((contact) => (
                                <Option key={contact.ContactId} value={contact.ContactId} caption={contact.FullName}>
                                    <div className={styles["contact-name"]}>{contact.FullName}</div>
                                    <div className={styles["contact-job"]}>{contact.Post}</div>
                                </Option>
                            ))}
                        </Dropdown>

                        {slot.contactId === null && slot.maxCountToSelect > 1 && (
                            <Dropdown defaultCaption={slot.count.toString()}
                                      value={slot.count.toString()}
                                      width="16px"
                                      className={styles["quantity-dropdown"]}
                                      onSelect={(value) => selectQuantity({ activeSlot: slot, count: parseInt(value, 10)})}
                            >
                                {_.range(1, slot.maxCountToSelect + 1).map(el => el.toString()).map(el => (
                                    <Option key={el} value={el} caption={el} />
                                ))}
                            </Dropdown>
                        )}

                        <div className={styles.remove} onClick={() => removeCertificate({ activeSlot: slot })}>
                            <Icon type={IconTypes.Delete} />
                        </div>
                    </div>
                </div>

                { isSelectPayerLightboxOpen && (
                    <CertificateIssueLightbox isOpen={true}
                                              onClose={() => isSelectPayerLightboxOpen && closeSelectPayer({ activeSlot: slot })}
                                              onClientSelect={(payerInfo) => selectPayer({ activeSlot: slot, payerInfo })}
                                              AvailableRecipientTypes={AvailableRecipientTypes}
                    />
                )}
            </div>
        );
    }

}

CertificateIssueSlot.propTypes = {
    canSelectPayer: PropTypes.bool.isRequired,
    AvailableRecipientTypes: PropTypes.array,

    slot: PropTypes.shape({
        count: PropTypes.number.isRequired,
        contactId: PropTypes.string,
        maxCountToSelect: PropTypes.number.isRequired,
        payerName: PropTypes.string,
        payerInn: PropTypes.string,
        payerKpp: PropTypes.string,
        isSelectPayerLightboxOpen: PropTypes.bool.isRequired,
        contacts: PropTypes.arrayOf(PropTypes.shape({
            ContactId: PropTypes.string,
            FullName: PropTypes.string.isRequired,
            Post: PropTypes.string
        }))
    }),

    removeCertificate: PropTypes.func.isRequired,
    selectQuantity: PropTypes.func.isRequired,
    selectOwner: PropTypes.func.isRequired,
    selectPayer: PropTypes.func.isRequired,
    openSelectPayer: PropTypes.func.isRequired,
    closeSelectPayer: PropTypes.func.isRequired
};

export default CertificateIssueSlot;
