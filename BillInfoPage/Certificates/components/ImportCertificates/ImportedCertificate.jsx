import { PropTypes } from "react";
import cx from "classnames";
import { datesRangeResolver, innKppResolver } from "billing-ui/helpers/StringHelpers";
import { formatDate } from "billing-ui/libs/moment";

import styles from "../../styles/ApplicationGroups/ImportedCertificate.scss";

const ImportedCertificate = ({ Id, FromDate, ToDate, StatusText, OwnerFio, OrgInn, OrgKpp, OrgName }) => (
    <div className={styles.radio}>
        <div className={styles["date-col"]}>
            <div className={styles.item}>{datesRangeResolver(formatDate(FromDate), formatDate(ToDate))}</div>
            <div className={cx(styles.item, styles.secondary)}>{StatusText}</div>
        </div>
        <div className={styles["owner-col"]}>
            <div className={cx(styles.item, styles.fade)}>{OwnerFio}</div>
            <div className={cx(styles.item, styles.secondary)}>{innKppResolver(OrgInn, OrgKpp)}</div>
            <div className={cx(styles.item, styles.secondary, styles.fade)}>{OrgName}</div>
        </div>
        <div className={styles["status-col"]}>
            <div className={styles.item}>Импортированный</div>
        </div>
    </div>
);

ImportedCertificate.propTypes = {
    Id: PropTypes.string,
    FromDate: PropTypes.string,
    ToDate: PropTypes.string,
    StatusText: PropTypes.string,
    OwnerFio: PropTypes.string,
    OrgInn: PropTypes.string,
    OrgKpp: PropTypes.string,
    OrgName: PropTypes.string
};

export default ImportedCertificate;
