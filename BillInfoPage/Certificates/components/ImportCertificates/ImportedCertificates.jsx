import { Component, PropTypes } from "react";
import ReactDOM from "react-dom";

import Link from "billing-ui/components/Link";
import ImportedCertificatesLightbox from "./ImportedCertificatesLightbox.jsx";
import UploadCertificate from "./UploadCertificate.jsx";

import styles from "../../styles/ApplicationGroups/ImportedCertificates.scss";

class ImportedCertificatesComponent extends Component {
    state = {
        isLightboxOpen: false
    };
    _lightboxTarget = null;

    constructor(props) {
        super(props);

        this._closeLightbox = this._closeLightbox.bind(this);
    }

    render() {
        const { ImportedCertificates, isShortDescription } = this.props;
        const { isLightboxOpen } = this.state;
        const hasImportedCertificates = ImportedCertificates && ImportedCertificates.length > 0;

        return (
            <div className={styles.wrapper}>
                {!isShortDescription && "или"}
                {hasImportedCertificates ? (
                    <div className={styles["link-wrapper"]}>
                        <Link ref={component => { this._lightboxTarget = ReactDOM.findDOMNode(component) }}
                              onClick={() => this.setState({ isLightboxOpen: true })}
                              className={styles.link}>
                            {`${isShortDescription ? "Указать" : "указать"} сертификат другого УЦ`}
                        </Link>

                        <ImportedCertificatesLightbox { ...this.props }
                                isLightboxOpen={isLightboxOpen}
                                closeLightbox={this._closeLightbox}
                                getOpenLink={() => this._lightboxTarget} />
                    </div>
                ) : (
                    <UploadCertificate { ...this.props } />
                )}
            </div>
        );
    }

    _closeLightbox() {
        this.setState({isLightboxOpen: false});
    }
}

ImportedCertificatesComponent.propTypes = {
    ImportedCertificates: PropTypes.array,
    selectedCertificate: PropTypes.string,
    isShortDescription: PropTypes.bool,
    sendSelectedCertificate: PropTypes.func.isRequired,
    selectImportedCertificate: PropTypes.func.isRequired,
    saveImportedCertificate: PropTypes.func.isRequired
};

ImportedCertificatesComponent.defaultProps = {
    isShortDescription: false
};

export default ImportedCertificatesComponent;
