import { Component, PropTypes } from "react";
import cx from "classnames";

import Lightbox from "billing-ui/components/Lightbox/Lightbox2";
import RadioGroup, { RadioButton } from "billing-ui/components/RadioGroup";

import UploadCertificate from "./UploadCertificate.jsx";
import ImportedCertificate from "./ImportedCertificate";

import styles from "../../styles/ApplicationGroups/ImportedCertificatesLightbox.scss";

class ImportedCertificatesLightbox extends Component {
    state = {
        renderUploadLink: false
    };

    constructor(props) {
        super(props);

        this._setUploadLink = this._setUploadLink.bind(this);
        this._resolveSelectCertificate = this._resolveSelectCertificate.bind(this);
    }

    render() {
        const { renderUploadLink } = this.state;
        const { isLightboxOpen, selectedCertificate, selectImportedCertificate, getOpenLink, closeLightbox } = this.props;

        return (
            <Lightbox title="Выберите сертификат"
                      getOpenLink={getOpenLink}
                      onOpen={this._setUploadLink}
                      onClose={closeLightbox}
                      isOpen={isLightboxOpen}
                      width={650}>
                <div className={styles.section}>
                    <RadioGroup className={styles.radios} value={selectedCertificate} onChange={selectImportedCertificate}>
                        {this._getImportedCertificates()}
                    </RadioGroup>

                    {renderUploadLink && (
                        <div>
                            <span className={styles.tip}>или</span>
                            <UploadCertificate { ...this.props } />
                        </div>
                    )}
                </div>

                <div className={styles.actions}>
                    <div className={styles.actionsBar}>
                        <div className={cx(styles.actionSubmit, {[styles.disabled]: !this._isSubmitEnabled()})}
                             onClick={this._resolveSelectCertificate}>
                            Выбрать
                        </div>
                        <div className={styles.actionCancel} onClick={closeLightbox}>
                            Отменить
                        </div>
                    </div>
                </div>
            </Lightbox>
        );
    }

    _resolveSelectCertificate() {
        const { sendSelectedCertificate, selectedCertificate, closeLightbox } = this.props;

        if (this._isSubmitEnabled()) {
            sendSelectedCertificate(selectedCertificate);
            closeLightbox();
        }
    }

    _isSubmitEnabled() {
        const { selectedCertificate } = this.props;
        return selectedCertificate !== null;
    }

    _getImportedCertificates() {
        const { ImportedCertificates } = this.props;

        return ImportedCertificates.map(certificate => (
            <RadioButton key={certificate.Id} value={certificate.Id} wrapperClassName={styles.radio} labelClassName={styles["radio-label"]}>
                <ImportedCertificate { ...certificate } />
            </RadioButton>
        ));
    }

    _setUploadLink() {
        this.setState({ renderUploadLink: true });
    }
}

ImportedCertificatesLightbox.propTypes = {
    ImportedCertificates: PropTypes.arrayOf(
        PropTypes.shape({
            Id: PropTypes.string
        })
    ),
    isLightboxOpen: PropTypes.bool.isRequired,
    selectedCertificate: PropTypes.string,
    getOpenLink: PropTypes.func.isRequired,
    closeLightbox: PropTypes.func.isRequired,
    sendSelectedCertificate: PropTypes.func.isRequired,
    selectImportedCertificate: PropTypes.func.isRequired
};

ImportedCertificatesLightbox.defaultProps = {
    selectedCertificate: null
};

export default ImportedCertificatesLightbox;
