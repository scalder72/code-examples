import { Component, PropTypes } from "react";
import ReactDOM from "react-dom";

import Informer from "Informer";
import Loader from "react-ui/Loader";
import Spinner from "react-ui/Spinner";
import FileUploadForm from "billing-ui/components/FileUpload";
import Tooltip from "billing-ui/components/Tooltip";
import Icon, { IconTypes } from "billing-ui/components/Icon";

import appConfig from "../../../../appConfig";

import styles from "../../styles/ApplicationGroups/UploadCertificate.scss";

class UploadCertificate extends Component {
    state = {
        fileIsLoading: false
    };
    _tooltipTarget = null;

    constructor(props) {
        super(props);

        this._handleError = this._handleError.bind(this);
    }

    _handleError(evt, data = {}) {
        try {
            const response = JSON.parse(data.jqXHR.responseText);
            const errorMessage = response.files[0].extra_info.message;
            Informer.showError(errorMessage ? errorMessage : "Не удалось загрузить файл");
        } catch (e) {
            Informer.showError("Не удалось загрузить файл");
        }
    }

    render() {
        const { saveImportedCertificate, payerId } = this.props;
        const { billInfoUrls: { CertificateUploadUrl } } = appConfig.getUrls();
        const { fileIsLoading } = this.state;

        return (
            <div className={styles.wrapper}>
                <Loader active={fileIsLoading} type={Spinner.Types.mini} caption="">
                    <FileUploadForm accept=".cer"
                                    ref={component => { this._tooltipTarget = ReactDOM.findDOMNode(component) }}
                                    url={`${CertificateUploadUrl}?payerId=${payerId}`}
                                    onFileUploadDone={(evt, data, fileId) => {
                                        saveImportedCertificate(fileId);
                                    }}
                                    onFileUploadFail={this._handleError}
                                    onFileUploadProcessFail={this._handleError}
                                    onFileUploadAdd={() => {}}
                                    onFileUploadSubmit={() => this.setState({ fileIsLoading: true })}
                                    onFileUploadAlways={() => this.setState({ fileIsLoading: false })}>
                        <span>
                            <Icon type={IconTypes.Upload} />
                            <span className={styles.text}>Загрузить сертификат</span>
                        </span>
                    </FileUploadForm>
                </Loader>

                <Tooltip getTarget={() => this._tooltipTarget}>Файл .cer</Tooltip>
            </div>
        );
    }
}

UploadCertificate.propTypes = {
    saveImportedCertificate: PropTypes.func.isRequired,
    payerId: PropTypes.string.isRequired
};

export default UploadCertificate;
