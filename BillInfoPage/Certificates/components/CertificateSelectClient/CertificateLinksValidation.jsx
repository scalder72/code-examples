import { Component, PropTypes } from "react";
import Button from "billing-ui/components/Button";
import { createServiceCenterInfoUrl } from "./../../../Shared/Helpers/UrlHelper";
import styles from "../../styles/CertificateSelectClient.scss";

class CertificateLinksValidation extends Component {
    render() {
        const { ServiceCenterId, ServiceCenterCode, ErrorMessage, BillId } = this.props;

        return (
            <div className={styles["links-validation"]}>
                <span className={styles["links-error-message"]}>
                    {ErrorMessage}
                    {ServiceCenterCode !== null && <span className={styles["sc-code"]}>{ServiceCenterCode}.</span>}
                </span>
                {ServiceCenterCode && ServiceCenterId &&
                    <Button href={createServiceCenterInfoUrl(BillId, ServiceCenterId)} className={styles["show-sc-contacts"]} target="_blank">
                        Посмотреть контакты
                    </Button>
                }
            </div>
        )
    }
}

CertificateLinksValidation.propTypes = {
    ServiceCenterId: PropTypes.string,
    ServiceCenterCode: PropTypes.string,
    ErrorMessage: PropTypes.string,
    BillId: PropTypes.string
};

export default CertificateLinksValidation;
