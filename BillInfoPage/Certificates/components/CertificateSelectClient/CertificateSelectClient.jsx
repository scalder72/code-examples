import { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { throttle } from "underscore";
import cx from "classnames";

import { LEGAL_INN_LENGTH, INDIVIDUAL_INN_LENGTH, KPP_LENGTH } from "../../utils/const";
import InnValidator from "../../utils/InnValidator";


import Link from "billing-ui/components/Link";
import Button from "billing-ui/components/Button";
import TextInputWrapper from "billing-ui/components/TextInput";
import { Dropdown, Option } from "billing-ui/components/Dropdown";
import TransitionGroup from "react-addons-transition-group";
import { slideToggle } from "../Animations/SlideToggle";
const AnimatedCertificateLinksValidation = slideToggle(CertificateLinksValidation);

import CertificateLinksValidation from "./CertificateLinksValidation";
import { searchPayer } from "./../../asyncActions";
import { handlePayerInnChange, handlePayerKppChange, handlePayerNameChange, selectPayerType } from "./../../actions";
import PayerTypes from "../../utils/PayerTypes";

import styles from "../../styles/CertificateSelectClient.scss";

class CertificateSelectClient extends Component {
    constructor(props) {
        super(props);

        this._tryStartSearchPayer = throttle(this._tryStartSearchPayer.bind(this), 200, { leading: false });
    }

    componentDidUpdate(prevProps) {
        const { payerInn, payerKpp, payerType } = this.props;

        if (prevProps.payerInn !== payerInn || prevProps.payerKpp !== payerKpp || prevProps.payerType !== payerType) {
            this._tryStartSearchPayer();
        }
    }

    _tryStartSearchPayer() {
        const { searchPayer, payerInn, payerKpp, payerType } = this.props;

        if (payerInn.length === INDIVIDUAL_INN_LENGTH) {
            searchPayer(payerInn, null, payerType);
        } else if (payerInn.length === LEGAL_INN_LENGTH && payerKpp.length === KPP_LENGTH) {
            searchPayer(payerInn, payerKpp, payerType);
        }
    }

    _handleSubmit() {
        const { payerInn, payerKpp, isPayerExist, payer, payerName, onClientSelect, payerType } = this.props;

        if (isPayerExist) {
            onClientSelect({
                payerName: payer.RecipientName,
                payerId: payer.RecipientId,
                payerInn: payer.Inn,
                payerKpp: payer.Kpp,
                payerType: payer.ClientType
            });
        } else {
            onClientSelect({
                payerName,
                payerKpp,
                payerInn,
                payerId: null,
                payerType
            });
        }
    }

    _isPayerTypeAvailable(type) {
        const { AvailableRecipientTypes } = this.props;

        return AvailableRecipientTypes.indexOf(type) >= 0;
    }

    render() {
        const {
            payerInn, payerKpp, isPayerExist, payer, payerName, payerType, handlePayerInnChange, handlePayerKppChange, selectPayerType, receiverErrors,
            linksError, handlePayerNameChange, inProcess, isRequisitesValid, onCancel, BillId, shouldRenderActions, bodyClassName, canCreateClient
        } = this.props;
        const { InnErrorMessage, KppErrorMessage } = (receiverErrors || {});
        const { ServiceCenterId, ServiceCenterCode, ErrorMessage } = (linksError || {});

        const validateInnResult = InnValidator.validateInn(payerInn);
        const isInnValid = validateInnResult.isValid && InnErrorMessage == null;
        const innErrorCaption = isInnValid ? null : (validateInnResult.errorMessage || InnErrorMessage);

        const isKppFilled = payerKpp.length === KPP_LENGTH;
        const isKppValid = isKppFilled && KppErrorMessage == null;
        const kppErrorCaption = !isKppValid ? KppErrorMessage : null;

        const isInnKppFilled = payerInn.length === INDIVIDUAL_INN_LENGTH || (payerInn.length === LEGAL_INN_LENGTH && isKppFilled);
        const hasName = (!isPayerExist && payerName.length > 0) || isPayerExist;
        const canSelectClient = isInnKppFilled && hasName && isRequisitesValid && !inProcess;

        const defaultPayerType = payerType || PayerTypes.IndividualBusinessman;

        return (
            <div className={cx(styles["lightbox-body"], bodyClassName)}>
                <TransitionGroup>
                    {linksError !== null && (
                        <AnimatedCertificateLinksValidation ServiceCenterId={ServiceCenterId || ""}
                                                            ServiceCenterCode={ServiceCenterCode || ""}
                                                            ErrorMessage={ErrorMessage}
                                                            BillId = {BillId}
                        />
                    )}
                </TransitionGroup>
                <div>
                    <TextInputWrapper placeholder="ИНН"
                                      type="compact"
                                      value={payerInn}
                                      isValid={isInnValid}
                                      tooltipCaption={innErrorCaption}
                                      onChange={(value) => handlePayerInnChange({currentInn: value, payerInn})}
                                      wrapperClassName={styles["inn-input"]}
                                      width={115}
                    />

                    {payerInn.length === INDIVIDUAL_INN_LENGTH && (
                        <Dropdown defaultCaption={PayerTypes.getDescription(defaultPayerType)}
                                  value={defaultPayerType}
                                  width="80px"
                                  className={styles["payer-type-dropdown"]}
                                  onSelect={(value) => selectPayerType({ payerType: value }) }>
                            {this._isPayerTypeAvailable(PayerTypes.PhysicalPerson) && (
                                <Option value={PayerTypes.PhysicalPerson}
                                        caption={PayerTypes.getDescription(PayerTypes.PhysicalPerson)}
                                />
                            )}
                            {this._isPayerTypeAvailable(PayerTypes.IndividualBusinessman) && (
                                <Option value={PayerTypes.IndividualBusinessman}
                                        caption={PayerTypes.getDescription(PayerTypes.IndividualBusinessman)}
                                />
                            )}
                        </Dropdown>
                    )}
                </div>
                <div>
                    {payerInn.length <= LEGAL_INN_LENGTH && <TextInputWrapper placeholder="КПП"
                                                               value={payerKpp}
                                                               isValid={isKppValid}
                                                               tooltipCaption={kppErrorCaption}
                                                               onChange={(value) => handlePayerKppChange({currentKpp: value, payerKpp})}
                                                               type="compact"
                                                               wrapperClassName={styles["kpp-input"]}
                                                               width={115}
                    />}

                    {isPayerExist && (
                        <div className={styles["name-block"]}>
                            <div className={styles["name-title"]}>Наименование</div>
                            <div className={styles["name-caption"]}>{payer.RecipientName}</div>
                        </div>
                    )}

                    {!isPayerExist && canCreateClient && (
                        <TextInputWrapper placeholder="Наименование"
                                          type="compact"
                                          onChange={(value) => handlePayerNameChange({ payerName: value })}
                                          onBlur={(evt) => handlePayerNameChange({ payerName: evt.target.value.trim() })}
                                          value={payerName}
                                          wrapperClassName={styles["name-input"]}
                                          width={400} />
                    )}
                </div>

                {shouldRenderActions && (
                    <div className={styles["lightbox-footer"]}>
                        <Button disabled={!canSelectClient}
                                onClick={() => canSelectClient && this._handleSubmit()}
                        >
                            Выбрать
                        </Button>
                        <Link className={styles.cancel} onClick={() => onCancel()}>Отменить</Link>
                    </div>
                )}
            </div>
        );
    }
}

CertificateSelectClient.propTypes = {
    BillId: PropTypes.string.isRequired,
    payerInn: PropTypes.string.isRequired,
    payerKpp: PropTypes.string.isRequired,
    payerName: PropTypes.string,
    payerType: PropTypes.string,
    isPayerExist: PropTypes.bool.isRequired,
    inProcess: PropTypes.bool.isRequired,

    shouldRenderActions: PropTypes.bool.isRequired,
    canCreateClient: PropTypes.bool.isRequired,
    bodyClassName: PropTypes.string,

    searchPayer: PropTypes.func.isRequired,
    handlePayerInnChange: PropTypes.func,
    handlePayerKppChange: PropTypes.func,
    handlePayerNameChange: PropTypes.func,
    selectPayerType: PropTypes.func,
    receiverErrors: PropTypes.object,
    linksError: PropTypes.object,
    isRequisitesValid: PropTypes.bool.isRequired,
    onClientSelect: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    AvailableRecipientTypes: PropTypes.arrayOf(
        PropTypes.oneOf(Object.keys(PayerTypes).map(key => PayerTypes[key]))
    ).isRequired,
    payer: PropTypes.shape({
        RecipientId: PropTypes.string,
        Inn: PropTypes.string,
        Kpp: PropTypes.string,
        RecipientName: PropTypes.string
    })
};

CertificateSelectClient.defaultProps = {
    shouldRenderActions: true,
    canCreateClient: true,
    onClientSelect: () => {},
    onCancel: () => {}
};

const mapStateToProps = ({ billInfo: { certificatesInfo: { selectClient }, mainBillInfo: { BillId } } }) => ({ ...selectClient, BillId });
const mapDispatchToProps = (dispatch) => bindActionCreators({
    searchPayer,
    handlePayerInnChange,
    handlePayerKppChange,
    handlePayerNameChange,
    selectPayerType
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CertificateSelectClient);
