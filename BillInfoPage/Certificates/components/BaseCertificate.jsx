import { Component, PropTypes } from "react";
import cx from "classnames";
import Link from "billing-ui/components/Link";
import shouldPureComponentUpdate from "react-pure-render/function";

import { formatDate } from "billing-ui/libs/moment";
import { innKppResolver, datesRangeResolver } from "billing-ui/helpers/StringHelpers";
import { createServiceCenterInfoUrl } from "../../Shared/Helpers/UrlHelper";

import Loader from "react-ui/Loader";
import Spinner from "react-ui/Spinner";
import Icon, { IconTypes } from "billing-ui/components/Icon";

import CertificateActionsWrapper from "./CertificateActionsWrapper.jsx";

class BaseCertificate extends Component {
    shouldPureComponentUpdate = shouldPureComponentUpdate;

    state = {
        isActive: false,
        shouldPopupUpdate: true
    };

    _getStatus() {
        const { StatusName, ActivityStatusText } = this.props;

        return { __html: StatusName || ActivityStatusText };
    }

    _resolveClick(evt) {
        const { HasExtendedInfo, openCertificate, Id, certificatesGroupName } = this.props;
        const isTargetActiveLink = evt.target.tagName.toLowerCase() === "a" && evt.target.href;

        if (HasExtendedInfo && !isTargetActiveLink) {
            openCertificate(Id, certificatesGroupName);
        }
    }

    resolveOnOpen() {
        const { isActive, shouldPopupUpdate } = this.state;

        if (isActive === true && shouldPopupUpdate === false) {
            return false;
        }

        this.setState({
            isActive: true,
            shouldPopupUpdate: false
        });
    }

    toggleOnClose() {
        const { isActive } = this.state;

        this.setState({
            isActive: !isActive,
            shouldPopupUpdate: true
        });
    }

    render() {
        const {
                  IsCloud, CreateDate, FromDate, ToDate, HasAcceleratedRelease, OwnerFio, OrgInn, OrgKpp, OrgName, TypeName,
                  ServiceCenterCode, ServiceCenterId, AttentionRequired, styles, isLoading, Id, certificatesGroupName, Actions,
                  performAction, HasExtendedInfo, billId, IsImported
              } = this.props;

        const { isActive, shouldPopupUpdate } = this.state;
        const formattedBeginDate = formatDate(CreateDate || FromDate);
        const formattedEndDate = ToDate && formatDate(ToDate);
        const hasActions = Actions && Actions.length > 0;

        const wrapperClassName = cx(styles.wrapper, {
            [styles.clickable]: HasExtendedInfo,
            [styles.active]: isActive
        });

        return (
            <Loader active={isLoading} type={Spinner.Types.normal} caption="">
                <div className={wrapperClassName}
                     onClick={(evt) => { this._resolveClick(evt) }}>
                    <div className={styles.icon}>
                        {IsCloud ? (
                            <Icon type={IconTypes.Cloud} />
                        ) : (
                            <Icon type={IconTypes.Certificate} />
                        )}
                    </div>

                    <div className={styles["date-col"]}>
                        <span className={styles.item}>
                            {datesRangeResolver(formattedBeginDate, formattedEndDate)}
                        </span>

                        {HasAcceleratedRelease && <div className={styles.accelerated}></div>}
                        <div className={cx(styles.info, { [styles.danger]: AttentionRequired })}
                             dangerouslySetInnerHTML={this._getStatus()}></div>
                    </div>

                    <div className={styles["data-col"]}>
                        {OwnerFio ? (
                            <div className={cx(styles.item, styles.fade)}>{OwnerFio}</div>
                        ) : (
                            <div className={cx(styles.item, styles.important)}>Данные не заполнены</div>
                        )}
                        <div className={styles.info}>{innKppResolver(OrgInn, OrgKpp)}</div>
                        <div className={styles.info}>{OrgName}</div>
                    </div>

                    <div className={cx(styles["sc-col"], {[styles.wide]: Actions.length !== 1})}>
                        {IsImported ? (
                            <div className={styles.item}>Импортированный</div>
                        ) : (
                            <div>
                                <div className={cx(styles.item, styles.fade)}>{TypeName}</div>
                                <div className={styles.info}>
                                    {ServiceCenterId && ServiceCenterCode && (
                                        <Link className={styles.scLink}
                                              target="_blank"
                                              href={createServiceCenterInfoUrl(billId, ServiceCenterId)}
                                        >
                                            {ServiceCenterCode}
                                        </Link>
                                    )}
                                </div>
                            </div>
                        )}
                    </div>

                    {hasActions && (
                        <CertificateActionsWrapper Actions={Actions}
                                                   performAction={performAction}
                                                   certificateId={Id}
                                                   styles={styles}
                                                   shouldUpdate={shouldPopupUpdate}
                                                   onOpen={() => { this.resolveOnOpen() }}
                                                   onClose={() => { this.toggleOnClose() }}
                                                   certificatesGroupName={certificatesGroupName}
                        />
                    )}
                </div>
            </Loader>
        );
    }
}

BaseCertificate.propTypes = {
    Id: PropTypes.string.isRequired,
    FromDate: PropTypes.string,
    ToDate: PropTypes.string,
    CreateDate: PropTypes.string,
    TypeName: PropTypes.string,
    IsCloud: PropTypes.bool,
    HasExtendedInfo: PropTypes.bool,
    AttentionRequired: PropTypes.bool,
    StatusName: PropTypes.string,
    ActivityStatusText: PropTypes.string,
    HasAcceleratedRelease: PropTypes.bool,
    ServiceCenterCode: PropTypes.string,
    ServiceCenterId: PropTypes.string,
    OwnerFio: PropTypes.string,
    OrgInn: PropTypes.string,
    OrgKpp: PropTypes.string,
    OrgName: PropTypes.string,
    Actions: PropTypes.arrayOf(PropTypes.object),
    IsImported: PropTypes.bool,

    billId: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    styles: PropTypes.object.isRequired,
    certificatesGroupName: PropTypes.string.isRequired,
    openCertificate: PropTypes.func.isRequired,
    performAction: PropTypes.func.isRequired
};

BaseCertificate.defaultProps = {
    CreateDate: null,
    FromDate: null,
    ToDate: null,
    HasAcceleratedRelease: false,
    AttentionRequired: false,
    IsImported: false,

    isLoading: false
};

export default BaseCertificate;
