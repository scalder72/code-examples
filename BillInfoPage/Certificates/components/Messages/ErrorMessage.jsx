﻿import styles from "../../../Shared/styles/ErrorMessage.scss";

const ErrorMessage = () => (
    <div className={styles.wrapper}>
        <div className={styles.arrow}></div>
        <div className={styles.header}>Не создан договор</div>
        <div className={styles.message}>
            Чтобы отправить заявку в КабУЦ, создайте<br/>
            договор: напечатайте, отправьте по почте<br/>
            или через Диадок.
        </div>
        <div className={styles.documents}></div>
    </div>
);

export default ErrorMessage;
