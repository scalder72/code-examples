﻿import { PropTypes } from "react";
import Link from "react-router/lib/Link";
import cx from "classnames";

import Button from "billing-ui/components/Button";

import certificatesErrorAction from "../../utils/CertificatesErrorAction";
import styles from "../../styles/InfoMessage.scss";

export const wrapperClass = "certificates-info-message__wrapper";
export const convertSchemeClass = "certificates-info-message__convert";
export const printClass = "certificates-info-message__print";

const InfoMessage = ({ Action, Message, convertSchemeAction, urls: { PrintBillUrl } }) => {
    return (
        <div>
            {Message && (
                <div className={cx(styles.wrapper, wrapperClass)}>
                    <div className={styles.message}>{Message}</div>

                    {Action === certificatesErrorAction.ConvertLegalSchemeAndPrint && (
                        <div className={cx(styles.action, convertSchemeClass)}>
                            <Button onClick={() => { convertSchemeAction(Action); }}
                                    className={styles.button}>
                                Перейти с оферты на договор
                            </Button>
                            <div className={styles.note}>Отменить переход можно только через инцидент.</div>
                        </div>
                    )}

                    {Action === certificatesErrorAction.Print && (
                        <div className={cx(styles.action, printClass)}>
                            <Link to={PrintBillUrl} className={styles.button}>Напечатать</Link>
                        </div>
                    )}
                </div>
            )}
        </div>
    );
};

InfoMessage.propTypes = {
    Action: PropTypes.oneOf(Object.keys(certificatesErrorAction).map(key => certificatesErrorAction[key])),
    Message: PropTypes.string,
    convertSchemeAction: PropTypes.func.isRequired,

    urls: PropTypes.shape({
        PrintBillUrl: PropTypes.string
    })
};

export default InfoMessage;
