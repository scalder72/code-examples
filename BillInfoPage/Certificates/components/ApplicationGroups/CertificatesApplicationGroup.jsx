﻿import { PropTypes } from "react";
import cx from "classnames";

import certificatesErrorLevel from "../../utils/CertificatesErrorLevel";
import certificatesErrorAction from "../../utils/CertificatesErrorAction";

import InfoMessage from "./../Messages/InfoMessage.jsx";
import styles from "../../styles/CertificatesGroup.scss";
import CertificatesApplicationInfoGroupComponent from "./CertificatesApplicationInfoGroup.jsx";

const CertificatesApplicationGroup = props => {
    const { ApplicationGroups, Error, convertSchemeAction, urls } = props;
    const titleClassName = cx(styles.title, {
        [styles["as-warning"]]: Error && Error.Level === certificatesErrorLevel.Info
    });

    return (
        <div className={styles.wrapper}>
            <div className={titleClassName}>Выпустить</div>

            {Error && <InfoMessage { ...Error } urls={urls} convertSchemeAction={convertSchemeAction} />}

            {ApplicationGroups.map(item => (
                <CertificatesApplicationInfoGroupComponent key={item.CertificateIds[0]} {...item} />
            ))}
        </div>
    );
};

CertificatesApplicationGroup.propTypes = {
    Error: PropTypes.shape({
        Level: PropTypes.oneOf(Object.keys(certificatesErrorLevel).map(key => certificatesErrorLevel[key])),
        Action: PropTypes.oneOf(Object.keys(certificatesErrorAction).map(key => certificatesErrorAction[key])),
        Message: PropTypes.string
    }),
    ApplicationGroups: PropTypes.arrayOf(
        PropTypes.shape({
            CertificateIds: PropTypes.array
        })
    ).isRequired,

    convertSchemeAction: PropTypes.func.isRequired,
    urls: PropTypes.object
};

export default CertificatesApplicationGroup;
