﻿import { Component, PropTypes } from "react";
import classnames from "classnames";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import shouldPureComponentUpdate from "react-pure-render/function";

import { toShortProductName } from "billing-ui/helpers/StringHelpers";
import Link from "billing-ui/components/Link";
import CertificatesIssueForm from "../CertificatesIssue/CertificatesIssueForm.jsx";
import CertificatesApplicationPopup from "../CertificatesIssue/CertificatesApplicationPopup.jsx";
import CertificatesProlongation from "./../CertificatesProlongation/CertificatesProlongation.jsx";
import certificatesApplicationReason from "../../utils/CertificateApplicationReason";

import {
    selectQuantity, selectOwner, openCertificatesIssue, closeCertificatesIssue,
    openSelectPayer, closeSelectPayer, removeCertificate, addCertificate,
    prolongationCertificateSelection, closeProlongation, prolongationChangeOwner,
    prolongationSelectOwner, prolongationSearch, openProlongation, filterProlongationCertificates
} from "./../../actions";

import { certificatesIssue, selectPayerAsync, prolongateCertificates } from "./../../asyncActions";

import styles from "../../styles/ApplicationGroups/CertificatesApplicationInfoGroup.scss";

class CertificatesApplicationInfoGroup extends Component {
    shouldPureComponentUpdate = shouldPureComponentUpdate;

    constructor(props) {
        super(props);

        this._handleProlongateClick = this._handleProlongateClick.bind(this);
    }

    _getProductName = () => {
        const { productName } = this.props;
        const trimmedProductName = toShortProductName(productName);
        return trimmedProductName === "Бухгалтерия" ? "Бухгалтерию" : trimmedProductName;
    };

    _handleProlongateClick() {
        const { CertificateIds, CertificateType, CertificateReason, certificatesProlongationActions } = this.props;
        const { openProlongation } = certificatesProlongationActions;

        openProlongation({ CertificateIds, CertificateType, CertificateReason });
    }

    render() {
        const {
            Title, CertificateIds, Properties, HasAcceleratedRelease, CertificateReason, CanPublish, CanProlongate, CanChangeRecipient,
            IsManagedByProduct, certificateIssue, certificateIssueActions, certificatesProlongation, AvailableRecipientTypes, existingPayer
              } = this.props;

        const { slots, inProcess, reservedSlotsCount, addCertificateSlot } = certificateIssue;

        const {
                  removeCertificate, selectQuantity, openCertificatesIssue, certificatesIssue,
                  closeCertificatesIssue, selectOwner, selectPayer, openSelectPayer, closeSelectPayer, addCertificate
              } = certificateIssueActions;

        const issueInProcess = CertificateIds === certificateIssue.certificateIds;
        const wrapperClassNames = classnames(styles.wrapper, {
            [styles.isOpen]: issueInProcess
        });

        const { prolongationLinkDisabled } = certificatesProlongation;
        const prolongationInProcess = CertificateIds === certificatesProlongation.certificateIds;
        const isUnplannedChange = CertificateReason === certificatesApplicationReason.UnplannedChange;

        return (
            <div className={wrapperClassNames}>
                <div>
                    <div className={styles.quantity}>{CertificateIds.length}</div>
                    <div className={styles.title}>{Title}</div>
                    {HasAcceleratedRelease && (
                        <div className={styles.acceleration}>
                            <div className={styles["icon-car"]}></div>
                        </div>
                    )}
                </div>

                <ul className={styles.list}>
                    {Properties.map((property, index) => (
                        <li className={styles.property} key={property}>{property}</li>
                    ))}
                </ul>

                {CanPublish && !IsManagedByProduct && (
                    <Link className={styles.link}
                          onClick={() => openCertificatesIssue({ certificateIds: CertificateIds }) }>
                        Выпустить новый
                    </Link>
                )}

                {CanProlongate && !IsManagedByProduct && (
                    <Link className={styles.link}
                          disabled={prolongationInProcess && prolongationLinkDisabled}
                          onClick={this._handleProlongateClick}
                    >
                        {isUnplannedChange ? "Внеплановая замена" : "Продлить"}
                    </Link>
                )}

                {IsManagedByProduct && (
                    <div className={styles.hint}>
                        Отправится клиенту в {this._getProductName()} автоматически
                    </div>
                )}

                {issueInProcess && (
                    <CertificatesApplicationPopup onClose={() => closeCertificatesIssue()}
                                                  onOpen={() => {}}
                                                  isActive={issueInProcess}
                                                  shouldOpenOnDidMount={true}
                                                  shouldUpdateChildren={true}
                                                  showLoader={inProcess}
                    >
                        <CertificatesIssueForm certificateIds={CertificateIds}
                                               slots={slots}
                                               addCertificateSlot={addCertificateSlot}
                                               addCertificate={addCertificate}
                                               removeCertificate={removeCertificate}
                                               selectQuantity={selectQuantity}
                                               reservedSlotsCount={reservedSlotsCount}
                                               certificatesIssue={certificatesIssue}
                                               selectOwner={selectOwner}
                                               selectPayer={selectPayer}
                                               openSelectPayer={openSelectPayer}
                                               closeSelectPayer={closeSelectPayer}
                                               canSelectPayer={CanChangeRecipient}
                                               AvailableRecipientTypes={AvailableRecipientTypes}
                                               existingPayer={existingPayer}
                        />
                    </CertificatesApplicationPopup>
                )}

                {prolongationInProcess && (
                    <CertificatesProlongation { ...this.props } isUnplannedChange={isUnplannedChange} />
                )}
            </div>
        );
    }
}

CertificatesApplicationInfoGroup.propTypes = {
    Title: PropTypes.string,
    CertificateIds: PropTypes.arrayOf(PropTypes.string),
    Properties: PropTypes.arrayOf(PropTypes.string),
    HasAcceleratedRelease: PropTypes.bool,
    CanPublish: PropTypes.bool,
    CanProlongate: PropTypes.bool,
    CertificateReason: PropTypes.oneOf(Object.keys(certificatesApplicationReason).map(key => certificatesApplicationReason[key])),
    CertificateType: PropTypes.string,
    IsProlongationOnly: PropTypes.bool,
    CanChangeRecipient: PropTypes.bool.isRequired,
    AvailableRecipientTypes: PropTypes.array,
    IsManagedByProduct: PropTypes.bool.isRequired,

    certificateIssue: PropTypes.shape({
        inProcess: PropTypes.bool,
        reservedSlotsCount: PropTypes.number,
        certificateIds: PropTypes.arrayOf(PropTypes.string),
        addCertificateSlot: PropTypes.object,
        slots: PropTypes.arrayOf(
            PropTypes.shape({
                count: PropTypes.number,
                contactId: PropTypes.string
            })
        )
    }),

    certificateIssueActions: PropTypes.shape({
        addCertificate: PropTypes.func,
        removeCertificate: PropTypes.func,
        selectQuantity: PropTypes.func,
        selectOwner: PropTypes.func,
        selectPayer: PropTypes.func,
        certificatesIssue: PropTypes.func,
        openCertificatesIssue: PropTypes.func,
        closeCertificatesIssue: PropTypes.func,
        openSelectPayer: PropTypes.func,
        closeSelectPayer: PropTypes.func
    }).isRequired,

    certificatesProlongationActions: PropTypes.object.isRequired,
    payerId: PropTypes.string,

    certificatesProlongation: PropTypes.shape({
        certificateIds: PropTypes.arrayOf(PropTypes.string),
        prolongationLinkDisabled: PropTypes.bool
    }),

    productName: PropTypes.string,
    retrievedPayer: PropTypes.object,
    existingPayer: PropTypes.object,
    contacts: PropTypes.array
};

const mapStateToProps = ({ billInfo: { mainBillInfo, certificatesInfo } }) => ({
    certificateIssue: certificatesInfo.certificateIssue,
    productName: mainBillInfo.ProductName,
    retrievedPayer: certificatesInfo.selectClient.payer,
    existingPayer: certificatesInfo.payer,
    billId: mainBillInfo.BillId,
    payerId: certificatesInfo.payerId,
    certificatesProlongation: certificatesInfo.certificatesProlongation,
    contacts: certificatesInfo.contacts
});

const mapActionsToProps = dispatch => ({
    certificateIssueActions: bindActionCreators({
        selectQuantity,
        selectOwner,
        selectPayer: selectPayerAsync,
        openCertificatesIssue,
        closeCertificatesIssue,
        certificatesIssue,
        openSelectPayer,
        closeSelectPayer,
        removeCertificate,
        addCertificate
    }, dispatch),
    certificatesProlongationActions: bindActionCreators({
        filterProlongationCertificates,
        prolongationCertificateSelection,
        prolongationChangeOwner,
        prolongationSelectOwner,
        prolongateCertificates,
        openProlongation,
        closeProlongation,
        prolongationSearch
    }, dispatch)
});

export default connect(mapStateToProps, mapActionsToProps)(CertificatesApplicationInfoGroup);
