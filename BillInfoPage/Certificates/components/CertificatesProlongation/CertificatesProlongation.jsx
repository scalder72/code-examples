import { Component, PropTypes } from "react";
import cx from "classnames";
import { debounce } from "underscore";

import GoogleAnalytics from "billing-ui/helpers/GoogleAnalytics";
import { resolveString } from "billing-ui/helpers/PluralStringResolver";
import Lightbox from "billing-ui/components/Lightbox/Lightbox2";
import Paging from "billing-ui/components/Paging";
import Icon, { IconTypes } from "billing-ui/components/Icon";

import CertificateForProlongation from "./CertificateForProlongation.jsx";
import CertificateSelectClient from "../CertificateSelectClient/CertificateSelectClient.jsx";

import styles from "../../styles/CertificatesProlongation/CertificatesProlongationPopup.scss";

class CertificatesProlongation extends Component {
    state = {
        isOpen: true
    };

    constructor(props) {
        super(props);

        this._resolveSearch = debounce(this._resolveSearch.bind(this), 200);
        this._resolveProlongateSubmit = this._resolveProlongateSubmit.bind(this);
    }

    _renderPaging() {
        const { certificatesProlongation: { CurrentPage, PageSize, TotalCount },
            certificatesProlongationActions: { filterProlongationCertificates } } = this.props;

        return <Paging CurrentPage={CurrentPage}
                       PageSize={PageSize}
                       Total={TotalCount}
                       isSmall={true}
                       wrapperClass={styles.paging}
                       onChange={pageNumber => {
                           filterProlongationCertificates({ pageNumber })
                       }} />
    }

    _resolveProlongateSubmit() {
        const { billId, payerId, CertificateIds, certificatesProlongationActions: { prolongateCertificates },
                certificatesProlongation: { selectedCertificates, submitEnabled } } = this.props;

        if (submitEnabled) {
            const formsWithOwnerCount = selectedCertificates.filter(cert => cert.ContactId && cert.ContactId !== null).length;
            const formsWithoutOwnerCount = selectedCertificates.filter(cert => cert.EmptyOwner === true).length;

            if (formsWithOwnerCount > 0) {
                GoogleAnalytics.customVar("Send Prolongation Forms with Owner", formsWithOwnerCount);
            }

            if (formsWithoutOwnerCount > 0) {
                GoogleAnalytics.customVar("Send Prolongation Forms without Owner", formsWithoutOwnerCount);
            }

            prolongateCertificates(billId, payerId, CertificateIds, selectedCertificates);
        }
    }

    _getEmptyBoxMessage() {
        const { retrievedPayer } = this.props;

        if (retrievedPayer === null) {
            return "В Биллинге нет такого клиента";
        }

        return "У клиента нет сертификатов для продления";
    }

    _resolveSearch() {
        const { certificatesProlongationActions: { filterProlongationCertificates } } = this.props;

        filterProlongationCertificates();
    }

    _close() {
        this.setState({
            isOpen: false
        });
    }

    render() {
        const { store } = this.context;
        const { isOpen } = this.state;
        const { isUnplannedChange, certificatesProlongation, certificatesProlongationActions, contacts, CanChangeRecipient,
            AvailableRecipientTypes } = this.props;
        const { closeProlongation, prolongationSearch } = certificatesProlongationActions;
        const { PageSize, TotalCount, CertificatesForProlongation, submitEnabled, updatedContacts, selectedCertificates, certificatesLimit,
                searchString } = certificatesProlongation;
        const hasCertificates = CertificatesForProlongation.length > 0;
        const showCertificates = hasCertificates || searchString || searchString === "";
        const certificatesForProlongateCount = selectedCertificates && selectedCertificates.length;

        return (
            <Lightbox title={isUnplannedChange ? "Внеплановая замена" : "Продление"}
                      width={650}
                      onClose={closeProlongation}
                      isOpen={isOpen}>
                <div className={styles.wrapper}>
                    {CanChangeRecipient && (
                        <div className={cx(styles.section, styles.form)}>
                            <CertificateSelectClient store={store}
                                                     shouldRenderActions={false}
                                                     canCreateClient={false}
                                                     AvailableRecipientTypes={AvailableRecipientTypes}
                                                     bodyClassName={styles.changing} />
                        </div>
                    )}

                    {showCertificates && (
                        <div className={styles.section}>
                            <div className={styles.title}>
                                {`Выберите максимум ${certificatesLimit} ${resolveString(certificatesLimit, ["сертификат", "сертификата", "сертификатов"])}`}
                            </div>

                            <div className={styles.search}>
                                <Icon className={styles["search-icon"]} type={IconTypes.Search} />
                                <input className={styles["search-input"]}
                                       type="text"
                                       id="CertificatesSearch"
                                       onChange={ evt => { prolongationSearch({ searchString: evt.target.value }) } }
                                       onKeyUp={ () => { this._resolveSearch() } }
                                       value={searchString} />
                                {!searchString && (
                                    <label htmlFor="CertificatesSearch" className={styles["search-placeholder"]}>Поиск по фамилии, отпечатку</label>
                                )}
                            </div>

                            {CertificatesForProlongation && CertificatesForProlongation.map(cert => (
                                <CertificateForProlongation
                                    key={cert.Id}
                                    contacts={contacts}
                                    updatedContacts={updatedContacts}
                                    certificatesLimit={certificatesLimit}
                                    certificatesProlongationActions={certificatesProlongationActions}
                                    { ...cert } />
                            ))}

                            {CertificatesForProlongation && CertificatesForProlongation.length === 0 && (
                                <div className={styles.empty}>Ничего не найдено</div>
                            )}
                        </div>
                    )}

                    {!showCertificates && (
                        <div className={styles.section}>
                            <div className={styles.empty}>{this._getEmptyBoxMessage()}</div>
                        </div>
                    )}
                </div>

                {TotalCount > PageSize && (
                    <div className={styles["paging-wrapper"]}>{this._renderPaging()}</div>
                )}

                <div className={styles.actions}>
                    <div className={styles.actionsBar}>
                        <div className={cx(styles.actionSubmit, {[styles.disabled]: !submitEnabled})} onClick={this._resolveProlongateSubmit}>
                            {certificatesForProlongateCount && submitEnabled
                                ? `Опубликовать ${certificatesForProlongateCount} ${resolveString(
                                    certificatesForProlongateCount,
                                    ["заявку", "заявки", "заявок"]
                                )}`
                                : "Опубликовать"}
                        </div>
                        <div className={styles.actionCancel} onClick={() => { this._close() }}>
                            Отменить
                        </div>
                    </div>
                </div>
            </Lightbox>
        );
    }
}

CertificatesProlongation.propTypes = {
    CertificateIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    CertificateType: PropTypes.string,
    CertificateReason: PropTypes.string,
    CanChangeRecipient: PropTypes.bool.isRequired,
    payerId: PropTypes.string.isRequired,
    billId: PropTypes.string.isRequired,
    AvailableRecipientTypes: PropTypes.array,

    isUnplannedChange: PropTypes.bool.isRequired,
    contacts: PropTypes.array,
    retrievedPayer: PropTypes.object,

    certificatesProlongationActions: PropTypes.shape({
        filterProlongationCertificates: PropTypes.func,
        closeProlongation: PropTypes.func,
        prolongateCertificates: PropTypes.func,
        prolongationSearch: PropTypes.func
    }).isRequired,

    certificatesProlongation: PropTypes.shape({
        CurrentPage: PropTypes.number,
        PageSize: PropTypes.number,
        TotalCount: PropTypes.number,
        CertificatesForProlongation: PropTypes.arrayOf(
            PropTypes.shape({
                Id: PropTypes.string
            })
        ),

        certificatesLimit: PropTypes.number,
        updatedContacts: PropTypes.array,
        selectedCertificates: PropTypes.array,
        submitEnabled: PropTypes.bool,
        searchString: PropTypes.string,
        clientSearchCompleted: PropTypes.bool
    }).isRequired
};

CertificatesProlongation.contextTypes = {
    store: PropTypes.object
};

export default CertificatesProlongation;
