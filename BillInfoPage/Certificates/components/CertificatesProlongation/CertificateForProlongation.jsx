import { Component, PropTypes } from "react";
import cx from "classnames";
import { formatDate } from "billing-ui/libs/moment";

import Checkbox from "billing-ui/components/Checkbox";
import { Dropdown, Option } from "billing-ui/components/Dropdown";
import Icon, { IconTypes } from "billing-ui/components/Icon";

import styles from "../../styles/CertificatesProlongation/CertificateForProlongation.scss";

class CertificateForProlongation extends Component {
    render() {
        const {
            certificatesProlongationActions, Id, selectOwner, certificatesLimit, isChecked, isDisabled, OwnerName, activeContact,
            contacts, updatedContacts, ExpirationDate, AttentionRequired
        } = this.props;
        const { prolongationCertificateSelection, prolongationSelectOwner, prolongationChangeOwner } = certificatesProlongationActions;
        const renderChangeOwner = contacts.length > 0;

        return (
            <div className={styles.certificate}>
                <Checkbox wrapperClassName={cx(styles.user, {[styles.full]: !selectOwner})}
                          labelClassName={styles.checkbox}
                          onChange={evt => prolongationCertificateSelection(
                                                { certificateId: Id, certificatesLimit: certificatesLimit }
                                              )}
                          checked={isChecked}
                          disabled={isDisabled}>
                    <Icon type={IconTypes.Certificate} className={styles.icon} />
                    <span className={styles.owner}>{!selectOwner && OwnerName}</span>
                </Checkbox>
                {selectOwner && (
                    <div className={styles.select}>
                        <Dropdown value={activeContact.ContactId}
                                  defaultCaption={activeContact.FullName}
                                  onSelect={value => { prolongationSelectOwner({certificateId: Id, contactId: value}) }}
                                  className={styles.dropdown}>
                            {updatedContacts.map(contact => (
                                <Option key={contact.ContactId} value={contact.ContactId} caption={contact.FullName}>
                                    <div className={styles["contact-name"]}>{contact.FullName}</div>
                                    <div className={styles["contact-job"]}>{contact.Post}</div>
                                </Option>
                            ))}
                        </Dropdown>
                    </div>
                )}
                {renderChangeOwner && !selectOwner && (
                    <div>
                        {isDisabled ? (
                            <div className={styles.action}>
                                <Icon type={IconTypes.Skip} className={styles.icon} />
                                Сменить владельца
                            </div>
                        ) : (
                            <div className={cx(styles.action, styles.active)}
                                 onClick={() => { prolongationChangeOwner(Id, certificatesLimit, true, contacts, OwnerName) }}>
                                <Icon type={IconTypes.Skip} className={styles.icon} />
                                Сменить владельца
                            </div>
                        )}
                    </div>
                )}
                <div className={cx(styles.date, {
                    [styles.warning]: AttentionRequired,
                    [styles.push]: !renderChangeOwner
                })}>
                    {`до ${formatDate(ExpirationDate)}`}
                </div>
            </div>
        );
    }
}

CertificateForProlongation.propTypes = {
    Id: PropTypes.string,
    OwnerName: PropTypes.string,
    ExpirationDate: PropTypes.string,
    AttentionRequired: PropTypes.bool,

    isChecked: PropTypes.bool,
    isDisabled: PropTypes.bool,
    selectOwner: PropTypes.bool,
    activeContact: PropTypes.shape({
        ContactId: PropTypes.string,
        FullName: PropTypes.string
    }),

    certificatesLimit: PropTypes.number.isRequired,
    contacts: PropTypes.array,
    updatedContacts: PropTypes.arrayOf(PropTypes.shape({
        ContactId: PropTypes.string,
        FullName: PropTypes.string.isRequired,
        Post: PropTypes.string
    })),

    certificatesProlongationActions: PropTypes.shape({
        prolongationCertificateSelection: PropTypes.func,
        prolongationChangeOwner: PropTypes.func,
        prolongationSelectOwner: PropTypes.func
    }).isRequired
};

export default CertificateForProlongation;
