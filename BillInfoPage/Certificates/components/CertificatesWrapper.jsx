﻿import { Component, PropTypes } from "react";
import shouldPureComponentUpdate from "react-pure-render/function";

import Loader from "react-ui/Loader";
import Spinner from "react-ui/Spinner";

import certificatesInfoErrorLevel from "../utils/CertificatesErrorLevel";
import ErrorMessage from "./Messages/ErrorMessage.jsx";
import CertificatesApplicationGroup from "./ApplicationGroups/CertificatesApplicationGroup.jsx";
import ImportedCertificatesComponent from "./ImportCertificates/ImportedCertificates.jsx";
import CertificateFormsGroup from "./CertificateForms/CertificateFormsGroup.jsx";
import IssuedCertificatesGroup from "./IssuedCertificates/IssuedCertificatesGroup.jsx";

import styles from "../styles/CertificatesWrapper.scss";

class CertificatesWrapper extends Component {
    shouldPureComponentUpdate = shouldPureComponentUpdate;

    _resolveFetch() {
        const { fetchRequired, fetchCertificates } = this.props;

        if (fetchRequired) {
            fetchCertificates();
        }
    }

    componentDidMount() {
        this._resolveFetch();
    }

    componentDidUpdate() {
        this._resolveFetch();
    }

    render() {
        const { isLoading, Error, ApplicationGroups, Forms, Certificates, wasLoaded, openCertificate, performAction, CanUploadCertificate,
            billId } = this.props;
        const hasError = wasLoaded && Error && Error.Level === certificatesInfoErrorLevel.Error;
        const hasNoErrors = wasLoaded && !hasError;
        const hasApplications = hasNoErrors && ApplicationGroups && ApplicationGroups.length > 0;
        const hasForms = hasNoErrors && Forms && Forms.length > 0;
        const hasCertificates = hasNoErrors && Certificates && Certificates.length > 0;

        return (
            <Loader active={isLoading} type={Spinner.Types.big}>
                <div className={styles.wrapper}>
                    {hasError && <ErrorMessage />}

                    {hasApplications && (
                        <CertificatesApplicationGroup { ...this.props } />
                    )}

                    {CanUploadCertificate && <ImportedCertificatesComponent { ...this.props } isShortDescription={ !hasApplications } />}

                    {hasForms && (
                        <CertificateFormsGroup Forms={Forms}
                                               openCertificate={openCertificate}
                                               performAction={performAction}
                                               billId={billId}
                        />
                    )}

                    {hasCertificates && (
                        <IssuedCertificatesGroup Certificates={Certificates}
                                                 openCertificate={openCertificate}
                                                 performAction={performAction}
                                                 billId={billId}
                        />
                    )}
                </div>
            </Loader>
        );
    }
}

CertificatesWrapper.propTypes = {
    fetchRequired: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    wasLoaded: PropTypes.bool,

    Error: PropTypes.shape({
        Level: PropTypes.oneOf(Object.keys(certificatesInfoErrorLevel).map(key => certificatesInfoErrorLevel[key]))
    }),
    ApplicationGroups: PropTypes.array,
    Forms: PropTypes.array,
    Certificates: PropTypes.array,
    ImportedCertificates: PropTypes.array,
    CanUploadCertificate: PropTypes.bool,
    payerId: PropTypes.string,

    billId: PropTypes.string.isRequired,
    fetchCertificates: PropTypes.func.isRequired,
    convertSchemeAction: PropTypes.func.isRequired,
    openCertificate: PropTypes.func.isRequired,
    performAction: PropTypes.func.isRequired,
    saveImportedCertificate: PropTypes.func.isRequired,
    selectImportedCertificate: PropTypes.func.isRequired,
    sendSelectedCertificate: PropTypes.func.isRequired,
    urls: PropTypes.shape({
        PrintBillUrl: PropTypes.string
    })
};

CertificatesWrapper.defaultProps = {
    isLoading: false
};

export default CertificatesWrapper;
