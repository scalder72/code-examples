import { PropTypes } from "react";
import cx from "classnames";
import TransitionGroup from "react-addons-transition-group";
import { slideToggle } from "./../Animations/SlideToggle";

import BaseCertificate from "../BaseCertificate.jsx";
const AnimatedCertificate = slideToggle(BaseCertificate);

import styles from "../../styles/CertificatesGroup.scss";
import formStyles from "../../styles/CertificateForm.scss";

const CertificateFormsGroup = ({ Forms, billId, openCertificate, performAction }) => (
    <div className={cx(styles.wrapper, styles.wide)}>
        <div className={styles.title}>Выпускается</div>
        <TransitionGroup>
            {Forms.map(form => (
                <AnimatedCertificate key={form.Id} { ...form }
                                     styles={formStyles}
                                     certificatesGroupName="Forms"
                                     billId={billId}
                                     performAction={performAction}
                                     openCertificate={openCertificate}
                />
            ))}
        </TransitionGroup>
    </div>
);

CertificateFormsGroup.propTypes = {
    billId: PropTypes.string.isRequired,
    Forms: PropTypes.arrayOf(
        PropTypes.shape({
            Id: PropTypes.string
        })
    ).isRequired,


    openCertificate: PropTypes.func,
    performAction: PropTypes.func
};

export default CertificateFormsGroup;
