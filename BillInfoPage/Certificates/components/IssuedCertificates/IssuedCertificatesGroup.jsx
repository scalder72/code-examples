import { PropTypes } from "react";
import cx from "classnames";

import TransitionGroup from "react-addons-transition-group";
import { slideToggle } from "./../Animations/SlideToggle";

import BaseCertificate from "../BaseCertificate.jsx";

import styles from "../../styles/CertificatesGroup.scss";
import certStyles from "../../styles/IssuedCertificate.scss";

const AnimatedCertificate = slideToggle(BaseCertificate);

const IssuedCertificatesGroup = ({ Certificates, billId, openCertificate, performAction }) => (
    <div className={cx(styles.wrapper, styles.wide)}>
        <div className={styles.title}>Выпущено</div>
        <TransitionGroup>
            {Certificates.map(certificate => (
                <AnimatedCertificate key={certificate.Id} { ...certificate }
                                     styles={certStyles}
                                     certificatesGroupName="Certificates"
                                     billId={billId}
                                     performAction={performAction}
                                     openCertificate={openCertificate}
                />
            ))}
        </TransitionGroup>
    </div>
);

IssuedCertificatesGroup.propTypes = {
    billId: PropTypes.string.isRequired,
    Certificates: PropTypes.arrayOf(
        PropTypes.shape({
            Id: PropTypes.string
        })
    ).isRequired,

    openCertificate: PropTypes.func,
    performAction: PropTypes.func
};

export default IssuedCertificatesGroup;
