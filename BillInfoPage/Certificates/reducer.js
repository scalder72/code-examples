﻿import reduceReducers from "reduce-reducers";

import fetchCertificatesReducer from "./reducers/fetchCertificatesReducer";
import convertSchemeReducer from "./reducers/convertSchemeReducer";
import certificateModelReducer from "./reducers/certificateModelReducer";
import { certificateIssueReducer } from "./reducers/certificateIssueReducer";
import certificatesProlongationReducer from "./reducers/certificatesProlongationReducer";
import { certificateSelectClientReducer } from "./reducers/certificateSelectClientReducer";
import importedCertificatesReducer from "./reducers/importedCertificatesReducer";
import selectClientWithProlongationReducer from "./reducers/selectClientWithProlongationReducer";

export default reduceReducers(
    convertSchemeReducer,
    fetchCertificatesReducer,
    certificateModelReducer,
    importedCertificatesReducer,
    selectClientWithProlongationReducer,
    (state, action) => ({
        ...state,
        certificatesProlongation: certificatesProlongationReducer(state.certificatesProlongation, action),
        certificateIssue: certificateIssueReducer(state.certificateIssue, action),
        selectClient: certificateSelectClientReducer(state.selectClient, action)
    })
);

