import axios from "billing-ui/libs/axios";
import GoogleAnalytics from "billing-ui/helpers/GoogleAnalytics";

import App from "../../globalApp";
import * as actions from "./actions";

import Informer from "Informer";
import Lightbox from "exports?Lightbox!ContentBase/scripts/Lightbox/Lightbox";
import PayerTypes from "./utils/PayerTypes"

export const fetchCertificates = () => (dispatch, getStore) => {
    const { billInfo: { urls, mainBillInfo } } = getStore();
    const { CertificatesUrl, ContactsSelectionUrl, SearchPayerByIdUrl } = urls;
    const { PayerId } = mainBillInfo;

    dispatch(actions.fetchCertificatesBegin());

    Promise.all(
        [
            axios.get(CertificatesUrl),
            axios.get(ContactsSelectionUrl, { params: { id: PayerId } }),
            axios.get(SearchPayerByIdUrl, { params: { id: PayerId } })
        ])
        .then(([ certificatesResponse, contactsResponse, payerResponse ]) => {
            const combinedData = {
                ...certificatesResponse.data,
                contacts: contactsResponse.data,
                payerId: PayerId,
                payer: payerResponse.data
            };

            return dispatch(actions.fetchCertificatesSuccess(combinedData));
        })
        .catch(e => dispatch(actions.fetchCertificatesError(e)));
};

export const selectPayerAsync = (selectPayerData) => (dispatch, getStore) => {
    const { billInfo: { urls } } = getStore();
    const { SearchPayerByIdUrl } = urls;
    const { payerId } = selectPayerData.payerInfo;

    if (selectPayerData.isEmptySlot) {
        dispatch(actions.addPayer(selectPayerData));
    } else {
        dispatch(actions.selectPayer(selectPayerData));
    }

    if (payerId != null) {
        axios
            .get(SearchPayerByIdUrl, { params: { id: payerId } })
            .then(({ data }) => {
                dispatch(actions.selectPayerSuccess({ payerId, payer: { ...data } }));
            });
    }
};

export const convertSchemeAction = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { ConvertSchemeUrl, PrintBillUrl } } = billInfo;

    dispatch(actions.convertSchemeActionBegin());
    axios
        .post(ConvertSchemeUrl)
        .then(() => {
            Informer.showSuccess("Переход прошел успешно");
            App.history.push(PrintBillUrl);
            return dispatch(actions.convertSchemeActionSuccess());
        })
        .catch(e => {
            Informer.showError("Не удалось сменить счет-оферту");
            return dispatch(actions.convertSchemeActionError(e));
        });
};

export const openCertificate = (certificateId, certificatesGroupName) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { OpenCertificateURL } } = billInfo;
    const url = `${OpenCertificateURL}?certificateId=${certificateId}`;

    dispatch(actions.openCertificateBegin({ certificateId, certificatesGroupName }));

    axios
        .post(url)
        .then(({ data }) => {
            const lightbox = Lightbox.create(data, { fixed: false });
            lightbox.open();

            return dispatch(actions.openCertificateSuccess({ certificateId, certificatesGroupName }));
        })
        .catch(e => {
            Informer.showError("Не удалось открыть сертификат");
            return dispatch(actions.openCertificateError({ certificateId, certificatesGroupName, error: e }));
        });
};

const collectIssueAnalyticsData = (certificatePublishSlots, certificateIds) => {
    setTimeout(() => {
        const formsWithOwnerCount = certificatePublishSlots
            .filter((slot) => slot.contactId === null)
            .reduce((result, slot) => result + slot.count, 0);

        const formsWithoutOwnerCount = certificatePublishSlots
            .filter((slot) => slot.contactId !== null)
            .reduce((result, slot) => result + slot.count, 0);

        if (formsWithOwnerCount > 0) {
            GoogleAnalytics.customVar("Send new Forms with Owner", formsWithOwnerCount);
        }

        if (formsWithoutOwnerCount > 0) {
            GoogleAnalytics.customVar("Send new Forms without Owner", formsWithoutOwnerCount);
        }

        GoogleAnalytics.customVar("Total Certificates count", certificateIds.length);
    }, 0);
};

const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));
export const certificatesIssue = () => (dispatch, getStore) => {
    const {
              urls: { CertificatesIssueUrl },
              certificatesInfo: { certificateIssue: { certificateIds, slots } },
              mainBillInfo: { BillId }
          } = getStore().billInfo;

    const certificatePublishSlots = slots.map((slot) => ({
        payerId: slot.payerId,
        payerInn: slot.payerInn,
        payerKpp: slot.payerKpp,
        payerName: slot.payerName,
        payerType: slot.payerType,
        isSelected: true,
        contactId: slot.contactId,
        count: slot.count
    }));

    collectIssueAnalyticsData(certificatePublishSlots, certificateIds);

    dispatch(actions.certificatesIssueBegin());
    axios.post(CertificatesIssueUrl, { BillId, certificateIds, certificatePublishSlots })
        .then(() => delay(2000))
        .then(() => dispatch(actions.certificatesIssueSuccess()))
        .catch((e = {}) => {
            const { data: { Message = "Не удалось выпустить сертификаты" } = {} } = e;

            Informer.showError(Message);
            return dispatch(actions.certificatesIssueError(e.data));
        });
};

export const performAction = (actionType, certificateId, certificatesGroupName) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { CertificateActionUrl } } = billInfo;
    const requestUrl = `${CertificateActionUrl}/${certificateId}/${actionType}`;

    dispatch(actions.performActionBegin({
        certificateId: certificateId,
        certificatesGroupName: certificatesGroupName
    }));

    axios
        .post(requestUrl)
        .then(({ data, data: { IsSuccess, ErrorMessage } }) => {
            if (IsSuccess) {
                return dispatch(actions.performActionSuccess({
                    certificateId: certificateId,
                    certificatesGroupName: certificatesGroupName,
                    data,
                    certificateAction: actionType
                }));
            }

            throw new Error(ErrorMessage && ErrorMessage);
        })
        .catch(e => {
            Informer.showError(e && e.message ? e.message : "Не удалось выполнить действие");
            return dispatch(actions.performActionError({
                certificateId: certificateId,
                certificatesGroupName: certificatesGroupName,
                error: e
            }));
        });
};

const collectProlongateAnalyticsData = selectedCertificates => {
    setTimeout(() => {
        selectedCertificates.forEach(cert => {
            if (cert.NeedChangeContact && cert.ContactId === null) {
                GoogleAnalytics.triggerEventAsync("bill-certs", "change", "owner-name-old");
            }
        });
    }, 10);
};

export const prolongateCertificates = (billId, payerId, certificateIds, prolongationCertificates) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { certificatesInfo: { certificatesProlongation }, urls: { CertificatesProlongationUrl } } = billInfo;
    const query = {
        BillId: billId,
        PayerId: certificatesProlongation.payerId || payerId,
        CertificateIds: certificateIds,
        ProlongationCertificates: prolongationCertificates
    };

    dispatch(actions.prolongateCertificatesBegin());
    axios
        .post(CertificatesProlongationUrl, query)
        .then(() => {
            collectProlongateAnalyticsData(prolongationCertificates);
            return dispatch(actions.prolongateCertificatesSuccess());
        })
        .catch(e => {
            Informer.showError(e.data && e.data.Message ? e.data.Message : "Не удалось продлить сертификаты");
            return dispatch(actions.prolongateCertificatesError(e));
        });
};

export const searchPayer = (payerInn, payerKpp, payerType) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { SearchPayerByInnKppUrl }, certificatesInfo } = billInfo;
    const certificateIds = certificatesInfo.certificateIssue.certificateIds
        ? certificatesInfo.certificateIssue.certificateIds
        : certificatesInfo.certificatesProlongation.certificateIds;

    dispatch(actions.searchPayerBegin());
    axios.get(SearchPayerByInnKppUrl, {
        params: {
            inn: payerInn,
            kpp: payerKpp,
            applicationId: certificateIds[0],
            clientType: payerType || PayerTypes.LegalEntity
        }})
        .then(({ data }) => {
            return dispatch(actions.searchPayerSuccess({ ...data, payerId: data.CertificateRecipient && data.CertificateRecipient.RecipientId }));
        })
        .catch(e => {
            Informer.showError(e && e.message ? e.message : "Произошла ошибка на сервере");
            return dispatch(actions.searchPayerError(e));
        });
};

export const saveImportedCertificate = (fileId) => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { CertificateOnUploadedUrl } } = billInfo;

    axios
        .post(CertificateOnUploadedUrl, { fileId: fileId })
        .then(({ data: { IsSuccess, ErrorMessage } }) => {
            if (IsSuccess) {
                return dispatch(actions.saveImportedCertificateSuccess());
            }

            throw new Error(ErrorMessage);
        })
        .catch(e => {
            Informer.showError(e && e.message ? e.message : "Не удалось сохранить сертификат");
        });
};

export const sendSelectedCertificate = certificateId => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { SelectUploadedCertificateUrl }, mainBillInfo: { BillId } } = billInfo;

    axios
        .post(SelectUploadedCertificateUrl, { certificateId, billId: BillId })
        .then(() => dispatch(actions.sendSelectedCertificateSuccess()))
        .catch(e => {
            Informer.showError(e && e.data && e.data.Message ? e.data.Message : "Не удалось выбрать сертификат");
        });
};
