﻿import { connect } from "react-redux";

import { fetchCertificates, convertSchemeAction, openCertificate, performAction, saveImportedCertificate, sendSelectedCertificate } from "./asyncActions";
import { selectImportedCertificate } from "./actions";
import CertificatesWrapper from "./components/CertificatesWrapper.jsx";

const mapStateToProps = ({
    billInfo: { mainBillInfo, certificatesInfo, urls }
}) => ({
    ...certificatesInfo,
    billId: mainBillInfo.BillId,
    payerId: mainBillInfo.PayerId,
    urls });
const mapDispatchToProps = { fetchCertificates, convertSchemeAction, openCertificate, performAction, saveImportedCertificate, selectImportedCertificate,
    sendSelectedCertificate };

export default connect(mapStateToProps, mapDispatchToProps)(CertificatesWrapper);
