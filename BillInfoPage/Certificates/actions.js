﻿import { createAction } from "redux-actions";
import * as types from "./actionTypes";
import certificateActionType from "./utils/CertificateActionType";

export const fetchCertificatesBegin = createAction(types.FETCH_CERTIFICATES_BEGIN);
export const fetchCertificatesSuccess = createAction(types.FETCH_CERTIFICATES_SUCCESS);
export const fetchCertificatesError = createAction(types.FETCH_CERTIFICATES_ERROR);

export const turnCertificatesAutoSyncOn = createAction(types.CERTIFICATES_SYNC_TURNED_ON);
export const turnCertificatesAutoSyncOff = createAction(types.CERTIFICATES_SYNC_TURNED_OFF);

export const fetchContactsSuccess = createAction(types.FETCH_CONTACTS_SUCCESS);
export const selectPayerSuccess = createAction(types.SELECT_PAYER_SUCCESS);

export const convertSchemeActionBegin = createAction(types.CONVERT_SCHEME_ACTION_BEGIN);
export const convertSchemeActionSuccess = createAction(types.CONVERT_SCHEME_ACTION_SUCCESS, null, () => ({
    ga: {
        category: "bill-certs",
        action: "click",
        label: "change-offer"
    }
}));
export const convertSchemeActionError = createAction(types.CONVERT_SCHEME_ACTION_ERROR);

export const openCertificatesIssue = createAction(types.OPEN_CERTIFICATES_ISSUE);
export const closeCertificatesIssue = createAction(types.CLOSE_CERTIFICATES_ISSUE);

export const addCertificate = createAction(types.ADD_CERTIFICATE);
export const removeCertificate = createAction(types.REMOVE_CERTIFICATE);
export const selectQuantity = createAction(types.SELECT_QUANTITY);
export const selectOwner = createAction(types.SELECT_OWNER);
export const addPayer = createAction(types.ADD_PAYER);
export const selectPayer = createAction(types.SELECT_PAYER);

export const openSelectPayer = createAction(types.OPEN_SELECT_PAYER);
export const closeSelectPayer = createAction(types.CLOSE_SELECT_PAYER);

export const certificatesIssueBegin = createAction(types.CERTIFICATES_ISSUE_BEGIN);
export const certificatesIssueError = createAction(types.CERTIFICATES_ISSUE_ERROR);
export const certificatesIssueSuccess = createAction(types.CERTIFICATES_ISSUE_SUCCESS);

export const openCertificateBegin = createAction(types.OPEN_CERTIFICATE_BEGIN);
export const openCertificateSuccess = createAction(types.OPEN_CERTIFICATE_SUCCESS);
export const openCertificateError = createAction(types.OPEN_CERTIFICATE_ERROR);

export const performActionBegin = createAction(types.PERFORM_ACTION_BEGIN);
export const performActionSuccess = createAction(types.PERFORM_ACTION_SUCCESS, null, (payload) => {
    if (payload) {
        const { category, action, label } = certificateActionType.getGAOptions(payload.certificateAction);

        if (category && action && label) {
            return { ga: { category, action, label } };
        }
    }

    return null;
});
export const performActionError = createAction(types.PERFORM_ACTION_ERROR);

export const fetchProlongationBegin = createAction(types.FETCH_PROLONGATION_BEGIN);
export const fetchProlongationSuccess = createAction(types.FETCH_PROLONGATION_SUCCESS);
export const fetchProlongationError = createAction(types.FETCH_PROLONGATION_ERROR);

export const prolongationCertificateSelection = createAction(types.PROLONGATION_CERTIFICATE_SELECTION);
export const actualizeCheckboxesStatuses = createAction(types.ACTUALIZE_CHECKBOXES_STATUSES);
export const openProlongation = createAction(types.OPEN_PROLONGATION);
export const closeProlongation = createAction(types.CLOSE_PROLONGATION);
export const filterProlongationCertificates = createAction(types.FILTER_PROLONGATION_CERTIFICATES);
export const prolongationSetSelectOwner = createAction(types.PROLONGATION_SET_SELECT_OWNER, null, () => ({
    ga: {
        category: "bill-certs",
        action: "change",
        label: "owner-name"
    }
}));

export const prolongationChangeOwner = (certificateId, certificatesLimit, setChecked, contacts, OwnerName) => dispatch => {
    dispatch(prolongationCertificateSelection({ certificateId, certificatesLimit, setChecked }));
    return dispatch(prolongationSetSelectOwner({ certificateId, contacts, OwnerName }));
};

export const prolongationSelectOwner = createAction(types.PROLONGATION_SELECT_OWNER);
export const prolongationSearch = createAction(types.PROLONGATION_SEARCH);

export const prolongateCertificatesBegin = createAction(types.PROLONGATE_CERTIFICATES_BEGIN);
export const prolongateCertificatesSuccess = createAction(types.PROLONGATE_CERTIFICATES_SUCCESS);
export const prolongateCertificatesError = createAction(types.PROLONGATE_CERTIFICATES_ERROR);

export const searchPayerBegin = createAction(types.SEARCH_PAYER_BEGIN);
export const searchPayerSuccess = createAction(types.SEARCH_PAYER_SUCCESS);
export const searchPayerError = createAction(types.SEARCH_PAYER_ERROR);

export const handlePayerInnChange = createAction(types.HANDLE_PAYER_INN_CHANGE);
export const handlePayerKppChange = createAction(types.HANDLE_PAYER_KPP_CHANGE);
export const handlePayerNameChange = createAction(types.HANDLE_PAYER_NAME_CHANGE);
export const selectPayerType = createAction(types.SELECT_PAYER_TYPE);

export const selectImportedCertificate = createAction(types.SELECT_IMPORTED_CERTIFICATE);
export const saveImportedCertificateSuccess = createAction(types.SAVE_IMPORTED_CERTIFICATE_SUCCESS);

export const sendSelectedCertificateSuccess = createAction(types.SEND_SELECTED_CERTIFICATE_SUCCESS);
