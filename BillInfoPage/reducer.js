﻿import reduceReducers from "reduce-reducers";
import { combineReducers } from "redux";

import commonReducer from "./reducers/commonReducer";
import billActionsReducer from "./ActionButtonsList/reducer";
import appStateReducer from "./reducers/appStateReducer";
import commentariesReducer from "./reducers/commentariesReducer";

import layoutReducer from "./Layout/reducer";
import purchaseReducer from "./Purchase/reducer";
import tokensReducer from "./Tokens/reducer";
import documentsReducer from "./Documents/reducer";
import certificatesReducer from "./Certificates/reducer";
import subscriptionsReducer from "./Subscriptions/reducer";
import serviceCenterInfoReducer from "./ServiceCenterInfo/reducer";

const rootReducer = reduceReducers(
    combineReducers({
        urls: (state = {}) => state,
        availableTabs: (state = []) => state,
        mainBillInfo: (state = {}) => state,

        commentaries: commentariesReducer,
        billActions: billActionsReducer,
        serviceCenters: serviceCenterInfoReducer,
        purchaseInfo: purchaseReducer,
        tokensInfo: tokensReducer,
        documentsInfo: documentsReducer,
        certificatesInfo: certificatesReducer,
        subscriptionsInfo: subscriptionsReducer,

        appState: appStateReducer,
        layoutState: layoutReducer,

        fetchRequired: (state = true) => state,
        wasLoaded: (state = false) => state,
        isLoading: (state = false) => state
    }),
    commonReducer
);

export default rootReducer;
