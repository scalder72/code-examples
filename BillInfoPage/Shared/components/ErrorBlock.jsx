﻿import { PropTypes } from "react";

const ErrorBlock = ({children}) => (
    <div className="warning-block">
        <span className="warning-icon inlineBlock" />
        <span className="warning-text inlineBlock">
            {children}
        </span>
    </div>
);

ErrorBlock.propTypes = {
    children: PropTypes.node
};

export default ErrorBlock;
