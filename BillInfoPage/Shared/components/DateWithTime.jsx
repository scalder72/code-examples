﻿import { PropTypes } from "react";
import moment from "billing-ui/libs/moment";

export const DateWithTime = ({date, timeMargin = 10, ignoreZeroTime = false, className = ""}) => {
    const formattedDate = moment(date);
    const time = formattedDate.format("LT");

    const showTime = !(ignoreZeroTime && time === "00:00");

    return (
        <span className={className}>
            {formattedDate.format("L")}
            {showTime && <span style={{marginLeft: timeMargin}}>{time}</span>}
        </span>
    );
};

DateWithTime.propTypes = {
    date: PropTypes.string.isRequired,
    timeMargin: PropTypes.number,
    ignoreZeroTime: PropTypes.bool,
    className: PropTypes.string
};

export default DateWithTime;
