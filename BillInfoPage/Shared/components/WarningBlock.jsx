﻿import { PropTypes } from "react";

const WarningBlock = ({ children }) => (
    <div className="warning-block">
        <span className="warning-icon inlineBlock" />
        <span className="warning-text inlineBlock">
            {children}
        </span>
    </div>
);

WarningBlock.propTypes = {
    children: PropTypes.node
};

export default WarningBlock;
