﻿import { PropTypes } from "react";
import Link from "react-router/lib/Link";
import Icon, {IconTypes} from "billing-ui/components/Icon";

import cx from "classnames";
import styles from "./../styles/HeaderBackLink.scss";

const HeaderBackLink = (props) => {
    const { url, to, withHoverEffect, customClass, children } = props;

    const classes = cx(styles.default, {
        [styles["__static"]]: !withHoverEffect,
        [styles[customClass]]: customClass
    });

    if (url) {
        return (
            <a className={classes} href={url}>
                <Icon type={IconTypes.ArrowChevronLeft} />
                <span className={styles.text}>{children}</span>
            </a>
        );
    }

    return (
        <Link className={classes} to={to}>
            <Icon type={IconTypes.ArrowChevronLeft} />
            <span className={styles.text}>{children}</span>
        </Link>
    );
};

HeaderBackLink.propTypes = {
    children: PropTypes.node,

    url: PropTypes.string,
    to: PropTypes.string,
    customClass: PropTypes.string,
    withHoverEffect: PropTypes.bool.isRequired
};

HeaderBackLink.defaultProps = {
    withHoverEffect: false
};

export default HeaderBackLink;
