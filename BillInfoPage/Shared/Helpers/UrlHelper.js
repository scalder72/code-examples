export const createServiceCenterInfoUrl = (billId, serviceCenterId) => `/billinfo/${billId}/sc/${serviceCenterId}`;
export const createBillInfoUrl = billId => `/billinfo/${billId}`;
