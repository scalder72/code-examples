﻿import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { fetchBillInfoData } from "./asyncActions";
import BillInfoWrapper from "./components/BillInfoWrapper";

const mapStateToProps = ({ billInfo }) => billInfo;
const mapDispatchToProps = dispatch => bindActionCreators({ fetchData: fetchBillInfoData }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(BillInfoWrapper);
