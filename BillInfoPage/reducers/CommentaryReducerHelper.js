﻿import { UPDATE_COMMENT_BEGIN, UPDATE_COMMENT_SUCCESS, UPDATE_COMMENT_ERROR, DELETE_COMMENT_ERROR } from "../actions/commentariesActions";
import { replaceByIndex } from "../../Shared/Helpers/ArrayHelper";

const _getCommentIndex = (array, actionCommentaryId) => (
    array.reduce((result, { CommentaryId }, index) => {
        if (CommentaryId === actionCommentaryId) {
            return index;
        }

        return 0;
    }, 0)
);

const mergeElementToArray = (arr, elementIndex, element) => {
    const mergedElement = { ...arr[elementIndex], ...element };
    return replaceByIndex(mergedElement, elementIndex, arr);
};

export default (state = [], action = {}) => {
    const commentaryIndex = _getCommentIndex(state, action.CommentaryId);

    switch (action.type) {
        case UPDATE_COMMENT_BEGIN:
            return mergeElementToArray(state, commentaryIndex, {
                Text: action.Text
            });

        case UPDATE_COMMENT_SUCCESS:
            return mergeElementToArray(state, commentaryIndex, {
                Author: action.Author,
                Date: action.Date,
                isActive: false
            });

        case UPDATE_COMMENT_ERROR:
            return mergeElementToArray(state, commentaryIndex, {
                Text: action.Text,
                userInput: action.userInput,
                isActive: true
            });

        case DELETE_COMMENT_ERROR:
            return mergeElementToArray(state, commentaryIndex, {
                isActive: true
            });

        default:
            return state;
    }
};
