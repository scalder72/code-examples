import { expect } from "chai";
import * as actions from "../../actions/commentariesActions";
import reducer from "../commentariesReducer";
import freeze from "deep-freeze";

describe("commentaries reducer", () => {
    it("should add loading on fetch begin", () => {
        const initState = freeze({
            isLoading: false
        });

        const expectedState = {
            isLoading: true
        };
        const actual = reducer(initState, actions.fetchCommentsBegin());

        expect(actual).to.deep.equal(expectedState);
    });

    it("should remove loading on fetch complete", () => {
        const initState = freeze({
            isLoading: false
        });

        const midState = reducer(initState, actions.fetchCommentsBegin());

        const expectedState = {
            isLoading: false
        };
        const actual = reducer(midState, actions.fetchCommentsComplete());

        expect(actual).to.deep.equal(expectedState);
    });

    it("should add commentaries on fetch success", () => {
        const initState = freeze({
            isLoading: false
        });
        const commentaries = freeze([{
            Author: "author",
            Text: "text",
            Date: "date",
            CommentaryId: "commentaryId"
        }]);

        const expectedState = {
            isLoading: false,
            commentaries: commentaries,
            stubComment: null,
            wasLoaded: true
        };
        const actual = reducer(initState, actions.fetchCommentsSuccess(commentaries));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should add stub comment text on create begin", () => {
        const initState = freeze({
            isLoading: false
        });
        const text = "text";

        const expectedState = {
            isLoading: false,
            stubComment: {
                Text: text
            }
        };
        const actual = reducer(initState, actions.createCommentBegin(text));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should set stub comment on create error", () => {
        const initState = freeze({
            isLoading: false
        });
        const text = "text";

        const expectedState = {
            isLoading: false,
            stubComment: {
                Text: null,
                userInput: text,
                isActive: true
            }
        };
        const actual = reducer(initState, actions.createCommentError(text));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should change text on update begin", () => {
        const commentaryId = "commentaryId";
        const commentaries = [{
            Author: "author",
            Text: "text",
            Date: "date",
            CommentaryId: commentaryId
        }];
        const initState = freeze({
            isLoading: false,
            commentaries: commentaries
        });

        const newText = "newText";
        const expectedState = {
            isLoading: false,
            commentaries: [{
                Author: "author",
                Text: newText,
                Date: "date",
                CommentaryId: commentaryId
            }]
        };
        const actual = reducer(initState, actions.updateCommentBegin(commentaryId, newText));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should change data on update begin", () => {
        const commentaryId = "commentaryId";
        const commentaries = [{
            Author: "author",
            Text: "text",
            Date: "date",
            CommentaryId: commentaryId
        }];
        const initState = freeze({
            isLoading: false,
            commentaries: commentaries
        });

        const newData = {
            Author: "newAuthor",
            Date: "newDate"
        };
        const expectedState = {
            isLoading: false,
            commentaries: [{
                Author: newData.Author,
                Text: "text",
                Date: newData.Date,
                CommentaryId: commentaryId,
                isActive: false
            }]
        };
        const actual = reducer(initState, actions.updateCommentSuccess(commentaryId, newData));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should set active on update error", () => {
        const commentaryId = "commentaryId";
        const oldText = "oldText";
        const commentaries = [{
            Author: "author",
            Text: oldText,
            Date: "date",
            CommentaryId: commentaryId
        }];
        const initState = freeze({
            isLoading: false,
            commentaries: commentaries
        });

        const newText = "newText";
        const expectedState = {
            isLoading: false,
            commentaries: [{
                Author: "author",
                Text: oldText,
                userInput: newText,
                Date: "date",
                CommentaryId: commentaryId,
                isActive: true
            }]
        };
        const actual = reducer(initState, actions.updateCommentError(commentaryId, newText, oldText));

        expect(actual).to.deep.equal(expectedState);
    });

    it("should set active on delete error", () => {
        const commentaryId = "commentaryId";
        const commentaries = [{
            Author: "author",
            Text: "text",
            Date: "date",
            CommentaryId: commentaryId
        }];
        const initState = freeze({
            isLoading: false,
            commentaries: commentaries
        });

        const expectedState = {
            isLoading: false,
            commentaries: [{
                Author: "author",
                Text: "text",
                Date: "date",
                CommentaryId: commentaryId,
                isActive: true
            }]
        };
        const actual = reducer(initState, actions.deleteCommentError(commentaryId));

        expect(actual).to.deep.equal(expectedState);
    });
});
