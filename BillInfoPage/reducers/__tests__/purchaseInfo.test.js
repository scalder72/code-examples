﻿import { expect } from "chai";
import { billActionSucceed } from "../../ActionButtonsList/actions";
import reducer from "../../reducer";
import freeze from "deep-freeze";

describe("purchase info reducer", () => {
    it("created conditional payment", () => {
        const state = freeze({
            purchaseInfo: {
                fetchRequired: false
            }
        });

        expect(state.purchaseInfo.fetchRequired).to.be.false;
        const newState = reducer(state, billActionSucceed());

        expect(newState.purchaseInfo.fetchRequired).to.be.true;
    });
});
