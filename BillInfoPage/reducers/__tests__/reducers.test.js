import { expect } from "chai";
import { billActionSucceed } from "../../ActionButtonsList/actions";
import reducer from "../../reducer";

describe("reducers", () => {
    it("init", () => {
        const newState = reducer({}, billActionSucceed());

        expect(newState.billActions).to.not.be.undefined;
        expect(newState.commentaries).to.not.be.undefined;
        expect(newState.appState).to.not.be.undefined;
        expect(newState.serviceCenters).to.not.be.undefined;
        expect(newState.purchaseInfo).to.not.be.undefined;
        expect(newState.purchaseInfo.fetchRequired).to.be.true;
        expect(newState.tokensInfo).to.not.be.undefined;
    });
});
