import { expect } from "chai";
import * as actions from "../../ActionButtonsList/actions";
import reducer from "../../reducer";
import freeze from "deep-freeze";

describe("bill actions reducer", () => {
    it("start updated action", () => {
        const state = freeze({
            billActions: {
                isLoading: false
            }
        });

        const newState = reducer(state, actions.updateActionButtonsBegin());

        expect(newState.billActions.isLoading).to.be.true;
    });

    it("stop updated action", () => {
        const state = freeze({
            billActions: {
                isLoading: true
            }
        });

        const newState = reducer(state, actions.updateActionButtonsComplete());
        expect(newState.billActions.isLoading).to.be.false;
    });

    it("success updated action", () => {
        const state = freeze({
            billActions: {
                isLoading: true,
                items: {}
            }
        });

        const newItems = {

        };

        const newState = reducer(state, actions.updateActionButtonsSuccess(newItems));
        expect(newState.billActions.items).to.equal(newItems);
    });

    it("error updated action", () => {
        const state = freeze({
            billActions: {
                isLoading: true,
                errorMessage: {}
            }
        });

        const responseText = {
            message: "Упс!"
        };

        let newState = reducer(
            state,
            actions.updateActionButtonsError({ responseText: JSON.stringify(responseText) })
        );
        expect(newState.billActions.errorMessage).to.equal(responseText.message);

        newState = reducer(state, actions.updateActionButtonsError({}));
        expect(newState.billActions.errorMessage).to.equal("Что-то пошло не так =(");
    });
});
