import { expect } from "chai";
import * as actions from "../../ServiceCenterInfo/actions";
import reducer from "../../reducer";
import freeze from "deep-freeze";

describe("service center reducer", () => {
    it("fetch service center", () => {
        const serviceCenterId1 = "serviceCenterId1";
        const serviceCenterId2 = "serviceCenterId2";
        const serviceCenterId3 = "serviceCenterId3";

        const state = freeze({
            serviceCenters: {
                [serviceCenterId1]: {
                    Requisites: {
                        ServiceCenterCode: "0800",
                        RegionCode: "66",
                        City: "Екатеринбург",
                        Title: "Title1",
                        Email: "ekb@gmail.com"
                    },
                    Contacts: {
                        FullName: "Иванов Иван Иванович",
                        Post: "Директор",
                        Phone: "89878876453",
                        Email: "ivanov@gmail.com"
                    }
                },
                [serviceCenterId2]: {
                    Requisites: {
                        ServiceCenterCode: "0790",
                        RegionCode: "79",
                        City: "Биробтджан",
                        Title: "Title2",
                        Email: "eao@gmail.com"
                    },
                    Contacts: {
                        FullName: "Сергеев Сергей Сергеевич",
                        Post: "уборщик",
                        Phone: "87655655423",
                        Email: "sergeev@gmail.com"
                    }
                }
            }
        });

        const newServiceCenter = {
            Requisites: {
                ServiceCenterCode: "0220",
                RegionCode: "22",
                City: "Алтайский",
                Title: "Title3",
                Email: "alty@gmail.com"
            },
            Contacts: {
                FullName: "Васильев Василий Васильевич",
                Post: "повар",
                Phone: "897678524341",
                Email: "vasiliy@gmail.com"
            }
        };

        expect(state.serviceCenters[serviceCenterId3]).to.be.undefined;
        const newState = reducer(
            state,
            actions.fetchServiceCenterSuccess({ serviceCenterId: serviceCenterId3, data: newServiceCenter })
        );

        expect(newState.serviceCenters[serviceCenterId3]).to.deep.equal(newServiceCenter);
    });
});
