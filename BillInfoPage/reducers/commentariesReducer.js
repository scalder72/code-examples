﻿import { actionTypes } from "../actions/commentariesActions";
import commentaryReducerHelper from "./commentaryReducerHelper";

const defaultState = {
    isLoading: false,
    wasLoaded: false,
    commentaries: []
};

export default (state = defaultState, action) => {
    switch (action.type) {

        case actionTypes.FETCH_COMMENTS_BEGIN:
            return {
                ...state,
                isLoading: true
            };

        case actionTypes.FETCH_COMMENTS_COMPLETE:
            return {
                ...state,
                isLoading: false
            };

        case actionTypes.FETCH_COMMENTS_SUCCESS:
            return {
                ...state,
                stubComment: null,
                commentaries: action.commentaries,
                wasLoaded: true
            };

        case actionTypes.CREATE_COMMENT_BEGIN:
            return {
                ...state,
                stubComment: {
                    Text: action.Text
                }
            };

        case actionTypes.CREATE_COMMENT_ERROR:
            return {
                ...state,
                stubComment: {
                    Text: null,
                    userInput: action.userInput,
                    isActive: true
                }
            };

        case actionTypes.UPDATE_COMMENT_BEGIN:
        case actionTypes.UPDATE_COMMENT_SUCCESS:
        case actionTypes.UPDATE_COMMENT_ERROR:
        case actionTypes.DELETE_COMMENT_ERROR:
            return {
                ...state,
                commentaries: commentaryReducerHelper(state.commentaries, action)
            };

        default:
            return state;
    }
};
