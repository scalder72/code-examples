﻿import { handleActions } from "redux-actions";
import * as types from "../Documents/actionTypes";
import * as fetchDataTypes from "./../actionTypes";
import { LOCATION_CHANGE } from "billing-ui/libs/react-router-redux";
import appConfig from "./../../appConfig";

const initialFetchDataState = {
    fetchRequired: true,
    isLoading: false,
    wasLoaded: false
};

const billInfoRouteRegExp = /^\/billinfo\/[0-9a-z-]{36}/i;
let lastBillInfoPathName = null;

export default handleActions({
    [types.DELETE_DOCUMENTS_PACKAGE_SUCCESS]: state => ({
        ...state,
        billActions: {
            ...state.billActions,
            fetchRequired: true
        }
    }),
    [fetchDataTypes.FETCH_BILL_INFO_BEGIN]: (state) => ({
        ...state,
        fetchRequired: false,
        isLoading: true
    }),
    [fetchDataTypes.FETCH_BILL_INFO_ERROR]: (state) => ({
        ...state,
        isLoading: false
    }),
    [fetchDataTypes.FETCH_BILL_INFO_SUCCESS]: (state, { payload }) => {
        const { MainBillInfo, AvailableTabs, Urls } = payload;

        appConfig.addOrUpdateUrls({ billInfoUrls: { ...Urls }});

        return {
            ...state,

            urls: Urls,
            availableTabs: AvailableTabs,
            mainBillInfo: MainBillInfo,

            wasLoaded: true,
            isLoading: false
        };
    },
    [LOCATION_CHANGE]: (state, { payload }) => {
        const pathName = payload.pathname.toLowerCase();
        const billInfoRoutePart = (pathName.match(billInfoRouteRegExp) || [null])[0];
        const wasBillInfoRouteChanged = billInfoRoutePart !== lastBillInfoPathName;

        if (wasBillInfoRouteChanged) {
            lastBillInfoPathName = billInfoRoutePart;
            return initialFetchDataState;
        }

        return state
    }
}, initialFetchDataState);
