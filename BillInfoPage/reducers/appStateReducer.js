﻿import { handleActions } from "redux-actions";

import * as serviceCenterInfoActionTypes from "../ServiceCenterInfo/actionTypes";
import { actionTypes as billDeleteActionTypes } from "../actions/billDeleteActions";

const defaultState = {
    appIsLoading: false,
    dumped: false
};

const reducer = handleActions({
    [billDeleteActionTypes.DELETE_BILL_BEGIN]: state => ({
        ...state,
        appIsLoading: true
    }),
    [billDeleteActionTypes.DELETE_BILL_COMPLETE]: state => ({
        ...state,
        appIsLoading: false
    }),
    [billDeleteActionTypes.DELETE_BILL_SUCCESS]: state => ({
        ...state,
        dumped: true
    }),

    [billDeleteActionTypes.RESTORE_BILL_BEGIN]: state => ({
        ...state,
        appIsLoading: true
    }),
    [billDeleteActionTypes.RESTORE_BILL_COMPLETE]: state => ({
        ...state,
        appIsLoading: false
    }),
    [billDeleteActionTypes.RESTORE_BILL_SUCCESS]: state => ({
        ...state,
        dumped: false
    }),

    [serviceCenterInfoActionTypes.FETCH_SERVICE_CENTER_BEGIN]: state => ({
        ...state,
        appIsLoading: true
    }),
    [serviceCenterInfoActionTypes.FETCH_SERVICE_CENTER_COMPLETE]: state => ({
        ...state,
        appIsLoading: false
    })
}, defaultState);

export default reducer;
