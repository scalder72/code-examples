﻿import axios from "billing-ui/libs/axios";
import * as actions from "./actions";
import { extractUrlFromRouteLocation } from "./../Shared/Helpers/UrlHelper";

export const fetchBillInfoData = (routeParams, routeLocation) => dispatch => {
    const fetchBillInfoUrl = extractUrlFromRouteLocation(routeLocation);

    dispatch(actions.fetchBillInfoBegin());

    axios
        .get(fetchBillInfoUrl)
        .then(response => dispatch(actions.fetchBillInfoSuccess(response.data)))
        .catch((e) => dispatch(actions.fetchBillInfoError(e)));
};
