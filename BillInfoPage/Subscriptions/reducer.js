﻿import { handleActions } from "redux-actions";
import * as types from "./actionTypes";

const initialState = {
    fetchRequired: true,
    isLoading: false
};

export default handleActions({
    [types.FETCH_SUBSCRIPTIONS_BEGIN]: state => ({
        ...state,
        fetchRequired: false,
        isLoading: true
    }),
    [types.FETCH_SUBSCRIPTIONS_SUCCESS]: (state, { payload }) => {
        const { InactiveCodesGroups, ActiveCodes, UnattachedRecipients } = payload;

        const hasInactiveCodesGroups = InactiveCodesGroups && InactiveCodesGroups.length > 0;
        const hasActiveCodes = ActiveCodes && ActiveCodes.length > 0;
        const hasUnattachedRecipients = UnattachedRecipients && UnattachedRecipients.length > 0;

        return {
            ...state,
            InactiveCodesGroups: hasInactiveCodesGroups ? InactiveCodesGroups : null,
            ActiveCodes: hasActiveCodes ? ActiveCodes : null,
            UnattachedRecipients: hasUnattachedRecipients ? UnattachedRecipients : null
        }
    },
    [types.FETCH_SUBSCRIPTIONS_COMPLETE]: state => ({
        ...state,
        isLoading: false
    })
}, initialState);
