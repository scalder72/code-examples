﻿import { Component, PropTypes } from "react";

import Loader from "react-ui/Loader";

import InactiveCodesComponent from "./ActivationCodes/InactiveCodes.jsx";
import ActiveCode from "./ActivationCodes/ActiveCode.jsx";
import UnattachedRecipient from "./ActivationCodes/UnattachedRecipient.jsx";

import styles from "../styles/SubscriptionsWrapper.scss";

class SubscriptionsWrapper extends Component {
    _resolveFetch() {
        const { fetchRequired, fetchSubscriptions } = this.props;

        if (fetchRequired) {
            fetchSubscriptions();
        }
    }

    componentDidMount() {
        this._resolveFetch();
    }

    render() {
        const { isLoading, InactiveCodesGroups, ActiveCodes, UnattachedRecipients, DownloadBalanceDetalizationUrl } = this.props;

        return (
            <Loader active={isLoading} type="big">
                <div className={styles.wrapper}>
                    {InactiveCodesGroups && InactiveCodesGroups.map(group => (
                        <InactiveCodesComponent key={group.Tariff + group.Codes.join(",")} { ...group } />
                    ))}

                    {ActiveCodes && ActiveCodes.map(code => <ActiveCode key={code.Code} { ...code } />)}

                    {UnattachedRecipients && UnattachedRecipients.map(recipient => (
                        <UnattachedRecipient key={recipient.Inn} { ...recipient } DownloadBalanceDetalizationUrl={DownloadBalanceDetalizationUrl} />
                    ))}
                </div>
            </Loader>
        );
    }
}

SubscriptionsWrapper.propTypes = {
    fetchRequired: PropTypes.bool.isRequired,
    fetchSubscriptions: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,

    InactiveCodesGroups: PropTypes.array,
    ActiveCodes: PropTypes.array,
    UnattachedRecipients: PropTypes.array,
    DownloadBalanceDetalizationUrl: PropTypes.string
};

export default SubscriptionsWrapper;
