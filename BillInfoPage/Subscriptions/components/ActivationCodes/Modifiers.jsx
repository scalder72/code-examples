﻿import { PropTypes } from "react";

import styles from "../../styles/InactiveCode.scss";

const ModifiersComponent = ({ Modifiers }) => {
    const hasModifiers = Modifiers && Modifiers.length > 0;

    return (
        <div>
            {hasModifiers && <div className={styles.item}>
                {`— ${Modifiers.join(", ")}`}
            </div>}
        </div>
    );
};

ModifiersComponent.propTypes = {
    Modifiers: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default ModifiersComponent;
