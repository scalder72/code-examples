﻿import { PropTypes } from "react";
import { formatDate } from "billing-ui/libs/moment";
import { innKppResolver } from "billing-ui/helpers/StringHelpers";

import LoginInfo from "./LoginInfo.jsx";
import BalanceAssetsComponent from "./BalanceAssets.jsx";
import ModifiersComponent from "./Modifiers.jsx";

import styles from "../../styles/ActiveCode.scss";

const ActiveCode = ({ Code, Recipient: { Inn, Kpp, Name, Logins, ExpirationDate }, Tariff, Modifiers, BalanceAssets }) => (
    <div className={styles.wrapper}>
        <div className={styles.main}>
            <div className={styles.header}>
                <div className={styles.icon}></div>
                <span className={styles.requisites}>{innKppResolver(Inn, Kpp) + " "}</span>
                <span className={styles.code}>{Code}</span>
            </div>

            <div>
                <div className={styles.item}>{Name}</div>
                {Logins.map((login, index, logins) => {
                    const isLoginLast = index === logins.length - 1;
                    return <LoginInfo key={login.Login} delimiter={!isLoginLast && ", "} { ...login } />;
                })}
            </div>
        </div>

        <div className={styles.tariff}>
            <div className={styles.dates}>
                {ExpirationDate && `до ${formatDate(ExpirationDate)}`}
            </div>
            {Tariff && <div className={styles["tariff-item"]}>{Tariff}</div>}
            {Modifiers && Modifiers.length > 0 && <ModifiersComponent Modifiers={Modifiers} />}
            {BalanceAssets && BalanceAssets.length > 0 && <BalanceAssetsComponent assets={BalanceAssets} />}
        </div>
    </div>
);

ActiveCode.propTypes = {
    Code: PropTypes.string.isRequired,
    Recipient: PropTypes.shape({
        Inn: PropTypes.string,
        Kpp: PropTypes.string,
        Name: PropTypes.string,
        Logins: PropTypes.arrayOf(
            PropTypes.shape({
                Phone: PropTypes.string,
                Login: PropTypes.string
            })
        ).isRequired,
        ExpirationDate: PropTypes.string
    }).isRequired,
    Tariff: PropTypes.string,
    Modifiers: PropTypes.arrayOf(PropTypes.string),
    BalanceAssets: PropTypes.arrayOf(PropTypes.string)
};

export default ActiveCode;
