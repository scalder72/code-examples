﻿import { Component, PropTypes } from "react";

import Icon, { IconTypes } from "billing-ui/components/Icon";

import CodeActionsWrapper from "./CodeActionsWrapper.jsx";
import BalanceAssetsComponent from "./BalanceAssets.jsx";
import ModifiersComponent from "./Modifiers.jsx";

import styles from "../../styles/InactiveCode.scss";

class InactiveCode extends Component {
    render() {
        const { Codes, Tariff, Modifiers, BalanceAssets, urls, IsImported } = this.props;
        const code = Codes[0];

        return (
            <div className={styles.wrapper}>
                <div className={styles.header}>
                    <div className={styles["header-inner"]}>
                        <span className={styles.icon}>
                            <Icon type={IconTypes.Warning} />
                        </span>
                        <span className={styles.title}>
                            {`${code} не активирован`}
                        </span>
                    </div>
                </div>

                <CodeActionsWrapper urls={urls} clipboardText={code} IsImported={IsImported} />

                <div className={styles.content}>
                    <div className={styles.item}>
                        {Tariff}
                    </div>
                    <ModifiersComponent Modifiers={Modifiers} />
                    <BalanceAssetsComponent assets={BalanceAssets} />
                </div>
            </div>
        );
    }
}

InactiveCode.propTypes = {
    Codes: PropTypes.arrayOf(PropTypes.string).isRequired,
    Tariff: PropTypes.string.isRequired,
    Modifiers: PropTypes.arrayOf(PropTypes.string).isRequired,
    BalanceAssets: PropTypes.arrayOf(PropTypes.string).isRequired,
    urls: PropTypes.object,
    IsImported: PropTypes.bool
};

export default InactiveCode;
