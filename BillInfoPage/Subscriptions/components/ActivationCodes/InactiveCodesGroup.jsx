﻿import { Component, PropTypes } from "react";
import cx from "classnames";

import { resolveString } from "../../../../Shared/Helpers/PluralStringResolver";
import Icon, { IconTypes } from "billing-ui/components/Icon";
import { publishGAEvent } from "../../utils/gaHelper";

import CodeActionsWrapper from "./CodeActionsWrapper.jsx";
import CodesPopup from "./CodesPopup.jsx";
import BalanceAssetsComponent from "./BalanceAssets.jsx";
import ModifiersComponent from "./Modifiers.jsx";

import styles from "../../styles/InactiveCode.scss";

class InactiveCodesGroup extends Component {
    _popupTarget = null;

    render() {
        const { Codes, Tariff, Modifiers, BalanceAssets, urls, IsImported } = this.props;
        const codesCount = Codes.length;
        const headerText = `${codesCount} ${resolveString(codesCount, ["код", "кода", "кодов"])} не активированы`;
        const codesList = Codes.join(", ");

        return (
            <div className={styles.wrapper}>
                <div className={cx(styles.header, styles.active)}
                     ref={node => { this._popupTarget = node }}
                     onClick={() => { publishGAEvent("show", "popup-codes") }}>
                    <div className={styles["header-inner"]}>
                        <span className={styles.icon}>
                            <Icon type={IconTypes.Warning} />
                        </span>
                        <span className={styles.title}>
                            {headerText}
                        </span>
                    </div>
                </div>

                <CodeActionsWrapper urls={urls} clipboardText={codesList} IsImported={IsImported} />

                <CodesPopup
                    IsImported={IsImported}
                    clipboardText={codesList}
                    codesList={Codes}
                    urls={urls}
                    getBindItem={() => this._popupTarget}
                    headerText={headerText} />

                <div className={styles.content}>
                    <div className={styles.item}>
                        {Tariff}
                    </div>
                    <ModifiersComponent Modifiers={Modifiers} />
                    <BalanceAssetsComponent assets={BalanceAssets} />
                </div>
            </div>
        );
    }
}

InactiveCodesGroup.propTypes = {
    Codes: PropTypes.arrayOf(PropTypes.string).isRequired,
    Tariff: PropTypes.string.isRequired,
    Modifiers: PropTypes.arrayOf(PropTypes.string).isRequired,
    BalanceAssets: PropTypes.arrayOf(PropTypes.string).isRequired,
    urls: PropTypes.object.isRequired,
    IsImported: PropTypes.bool
};

export default InactiveCodesGroup;
