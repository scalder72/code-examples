﻿import { PropTypes } from "react";
import { formatDate } from "billing-ui/libs/moment";
import { innKppResolver } from "billing-ui/helpers/StringHelpers";
import cx from "classnames";

import Link from "billing-ui/components/Link";
import LoginInfo from "./LoginInfo.jsx";

import styles from "../../styles/ActiveCode.scss";

const UnattachedRecipient = ({ Inn, Kpp, Name, Logins, ExpirationDate, HasDetalization, DownloadBalanceDetalizationUrl }) => (
    <div className={styles.wrapper}>
        <div className={cx(styles.main, styles["as-recipient"])}>
            <div className={styles.header}>
                <span className={styles.requisites}>{innKppResolver(Inn, Kpp)}</span>
                {HasDetalization && <Link className={styles.link} href={DownloadBalanceDetalizationUrl}>скачать трафик</Link>}
            </div>

            <div>
                <div className={styles.item}>{Name}</div>
                {Logins.map((login, index, logins) => {
                    const isLoginLast = index === logins.length - 1;
                    return <LoginInfo key={login.Login} delimiter={!isLoginLast && ", "} { ...login } />;
                })}
            </div>
        </div>

        <div className={styles.tariff}>
            <div className={styles.dates}>
                {ExpirationDate && `до ${formatDate(ExpirationDate)}`}
            </div>
        </div>
    </div>
);

UnattachedRecipient.propTypes = {
    Inn: PropTypes.string.isRequired,
    Kpp: PropTypes.string.isRequired,
    Name: PropTypes.string.isRequired,
    Logins: PropTypes.arrayOf(
        PropTypes.shape({
            Phone: PropTypes.string,
            Login: PropTypes.string
        })
    ).isRequired,
    ExpirationDate: PropTypes.string,
    HasDetalization: PropTypes.bool,
    DownloadBalanceDetalizationUrl: PropTypes.string
};

export default UnattachedRecipient;
