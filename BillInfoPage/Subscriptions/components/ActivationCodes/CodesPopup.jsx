﻿import { Component, PropTypes } from "react";
import { throttle } from "underscore";
import events from "add-event-listener";

import Icon, { IconTypes } from "billing-ui/components/Icon";
import Popup from "billing-ui/components/PopupWrapper";
import Link from "billing-ui/components/Link";
import Clipboard from "billing-ui/components/ClipboardWrapper";

import { publishGAEvent } from "../../utils/gaHelper";

import styles from "../../styles/CodesPopup.scss";

class CodesPopup extends Component {
    _closeLink = null;
    state = {
        popupWidth: null,
        hotReloadPopup: false,
        updatePopup: false
    };

    _setPopupWidth() {
        this.setState({
            popupWidth: this._getWidth(),
            hotReloadPopup: true
        });
    }

    _closePopup() {
        this.setState({
            updatePopup: true
        });
    }

    _getWidth() {
        const { getBindItem } = this.props;
        const bindItem = getBindItem();
        return bindItem && bindItem.offsetWidth;
    }

    componentDidMount() {
        this._setPopupWidth();

        events.addEventListener(window, "resize", throttle(this._setPopupWidth.bind(this), 50));
    }

    render() {
        const { codesList, getBindItem, headerText, urls: { SendBillUrl }, clipboardText, IsImported } = this.props;
        const { popupWidth, hotReloadPopup, updatePopup } = this.state;

        return (
            <Popup width={popupWidth}
                   shouldUpdate={!hotReloadPopup || updatePopup}
                   updateWithoutClosing={hotReloadPopup}
                   className={styles.popup}
                   getBindItem={getBindItem}
                   getOpenLink={getBindItem}
                   getCloseLink={() => this._closeLink}
                   position={{top: 0, left: 0}}
            >
                <div className={styles.wrapper}>
                    <div className={styles.header}>
                        <span className={styles.icon}><Icon type={IconTypes.Warning} /></span>
                        <span className={styles.title} ref={node => { this._closeLink = node }}>{headerText}</span>
                    </div>
                    <div className={styles.actions}>
                        <Clipboard value={clipboardText} className={styles.action} onSuccess={this._closePopup.bind(this)}>
                            <Icon type={IconTypes.Copy} />
                            <span className={styles["action-text"]} onClick={() => { publishGAEvent("copy", "popup-codes") }}>Скопировать</span>
                        </Clipboard>
                        {!IsImported && <Link href={SendBillUrl} target="_blank" className={styles.action}>
                            <Icon type={IconTypes.Mail2} />
                            <span className={styles["action-text"]} onClick={() => { publishGAEvent("send", "popup-codes") }}>Отправить</span>
                        </Link>}
                    </div>

                    <div className={styles.codes}>
                        {codesList.map(item => <div key={item} className={styles.code}>{item}</div>)}
                    </div>
                </div>
            </Popup>
        );
    }
}

CodesPopup.propTypes = {
    getBindItem: PropTypes.func.isRequired,
    headerText: PropTypes.string.isRequired,
    clipboardText: PropTypes.string.isRequired,
    codesList: PropTypes.array,
    urls: PropTypes.object,
    IsImported: PropTypes.bool
};

export default CodesPopup;
