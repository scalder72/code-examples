﻿import { Component, PropTypes } from "react";
import TransitionGroup from "react-addons-transition-group";

import { copyCommandSupportChecker } from "billing-ui/helpers/QueryCommandSupportChecker";

import CodeActions from "./CodeActions.jsx";

const copyToClipboardAvailableCheck = copyCommandSupportChecker();

class CodeActionsWrapper extends Component {
    state = {copyToClipboardAvailable: null};

    _resolveCopyToClipboard() {
        copyToClipboardAvailableCheck
            .then(() => { this.setState({copyToClipboardAvailable: true}) })
            .catch(() => { this.setState({copyToClipboardAvailable: false}) });
    }

    componentDidMount() {
        this._resolveCopyToClipboard();
    }

    render() {
        const { copyToClipboardAvailable } = this.state;
        const { IsImported } = this.props;

        return (
            <TransitionGroup>
                <CodeActions { ...this.props } copyToClipboardAvailable={copyToClipboardAvailable} IsImported={IsImported} />
            </TransitionGroup>
        );
    }
}

CodeActionsWrapper.propTypes = {
    clipboardText: PropTypes.string.isRequired,
    urls: PropTypes.object,
    IsImported: PropTypes.bool
};

export default CodeActionsWrapper;
