﻿import { PropTypes } from "react";

import styles from "../../styles/InactiveCode.scss";

const BalanceAssets = ({ assets }) => (
    <div>
        {assets.map(asset => (
            <div key={asset} className={styles.item}>
                {`— ${asset}`}
            </div>
        ))}
    </div>
);

BalanceAssets.propTypes = {
    assets: PropTypes.arrayOf(PropTypes.string)
};

export default BalanceAssets;
