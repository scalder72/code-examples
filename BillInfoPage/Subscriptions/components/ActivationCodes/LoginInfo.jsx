﻿import { PropTypes } from "react";
import cx from "classnames";

import styles from "../../styles/ActiveCode.scss";

export const loginClass = "login-info__login";
export const phoneClass = "login-info__phone";

const LoginInfo = ({ Phone, Login, delimiter }) => (
    <span className={styles.contact}>
        {Login && (
            <span className={loginClass}>
                {Login}
                {Phone && <span className={cx(styles.phone, phoneClass)}>({Phone} )</span>}
                {delimiter}
            </span>
        )}

        {!Login && (
            <span className={phoneClass}>{Phone}</span>
        )}
    </span>
);

LoginInfo.propTypes = {
    Phone: PropTypes.string,
    Login: PropTypes.string,
    delimiter: PropTypes.string
};

export default LoginInfo;
