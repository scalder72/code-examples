﻿import { Component, PropTypes } from "react";
import { connect } from "react-redux";

import InactiveCodesGroup from "./InactiveCodesGroup.jsx";
import InactiveCode from "./InactiveCode.jsx";

export class InactiveCodes extends Component {
    render() {
        const { Codes } = this.props;
        var hasMoreThanOneCode = Codes.length > 1;

        return (
            <div>
                {hasMoreThanOneCode ? <InactiveCodesGroup { ...this.props } /> : <InactiveCode { ...this.props } />}
            </div>
        );
    }
}

InactiveCodes.propTypes = {
    Codes: PropTypes.array.isRequired,
    Tariff: PropTypes.string,
    Modifiers: PropTypes.array,
    BalanceAssets: PropTypes.array
};

const mapStateToProps = ({ billInfo: { urls, mainBillInfo: { IsImported } }} = {}) => ({ urls, IsImported });
export default connect(mapStateToProps, null)(InactiveCodes);
