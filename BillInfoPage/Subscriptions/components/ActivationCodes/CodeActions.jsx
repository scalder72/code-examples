﻿import { Component, PropTypes } from "react";
import { findDOMNode } from "react-dom";
import TweenLite from "gsap";

import Actions, { Action } from "billing-ui/components/Actions";
import { IconTypes } from "billing-ui/components/Icon";
import Clipboard from "billing-ui/components/ClipboardWrapper";
import Link from "billing-ui/components/Link";
import App from "./../../../../globalApp";

import { publishGAEvent } from "../../utils/gaHelper";

import styles from "../../styles/CodeActions.scss";

class CodeActions extends Component {
    _actionsTarget = null;
    state = {
        isActive: true
    };

    _shouldRenderPopup() {
        const { copyToClipboardAvailable, IsImported } = this.props;

        return copyToClipboardAvailable && !IsImported;
    }

    _closePopup() {
        this.setState({
            isActive: false
        });
    }

    _renderSingleActionLink() {
        const { copyToClipboardAvailable, IsImported } = this.props;

        if (copyToClipboardAvailable) {
            const { clipboardText } = this.props;

            return (
                <Clipboard value={clipboardText}>
                    <div className={styles.single} onClick={() => { publishGAEvent("copy", "tab") }}>Скопировать</div>
                </Clipboard>
            );
        }

        if (!IsImported) {
            const { urls: { SendBillUrl } } = this.props;

            return (<Link className={styles.single} href={SendBillUrl} target="_blank" onClick={() => { publishGAEvent("send", "tab") }}>Отправить</Link>);
        }
    }

    componentWillEnter(callback) {
        const node = findDOMNode(this);
        TweenLite.fromTo(node, .3, {opacity: 0}, {opacity: 1, onComplete: callback});
    }

    render() {
        const { urls: { SendBillUrl }, clipboardText } = this.props;
        const { isActive } = this.state;
        var shouldRenderPopup = this._shouldRenderPopup();


        return (
            <div className={styles.actions}>
                {shouldRenderPopup && (
                    <div>
                        <div className={styles.ellipsis} ref={node => { this._actionsTarget = node }}>…</div>
                        <Actions getBindItem={() => this._actionsTarget} position={{top: 2, right: -6}} isActive={isActive}>
                            <Clipboard value={clipboardText} onSuccess={this._closePopup.bind(this)}>
                                <Action
                                    iconType={IconTypes.Copy}
                                    description="Скопировать"
                                    onClick={() => { publishGAEvent("copy", "tab") }} />
                            </Clipboard>

                            <Action iconType={IconTypes.Mail2}
                                    description="Отправить"
                                    onClick={() => { publishGAEvent("send", "tab"); App.history.push(SendBillUrl); }}
                            />
                        </Actions>
                    </div>
                )}
                {!shouldRenderPopup && this._renderSingleActionLink()}
            </div>
        );
    }
}

CodeActions.propTypes = {
    clipboardText: PropTypes.string.isRequired,
    urls: PropTypes.object,
    copyToClipboardAvailable: PropTypes.bool,
    IsImported: PropTypes.bool
};

export default CodeActions;
