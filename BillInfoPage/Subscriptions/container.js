﻿import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchSubscriptions } from "./actions";
import SubscriptionsWrapper from "./components/SubscriptionsWrapper.jsx";

const mapStateToProps = ({ billInfo }) => ({ ...billInfo.subscriptionsInfo, DownloadBalanceDetalizationUrl: billInfo.urls.DownloadBalanceDetalizationUrl });
const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchSubscriptions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionsWrapper);
