import { expect } from "chai";
import { renderIntoDocument, scryRenderedDOMComponentsWithClass } from "react-addons-test-utils";
import { wrap } from "react-stateless-wrapper"

import LoginInfo, { loginClass, phoneClass } from "../components/ActivationCodes/LoginInfo.jsx";

const WrappedLoginInfo = wrap(LoginInfo);

describe("login info component", () => {
    it("should render login and phone if both passed", () => {
        const initState = {
            Login: "Login",
            Phone: "Phone"
        };
        const info = renderIntoDocument(<WrappedLoginInfo {...initState} />);

        const login = scryRenderedDOMComponentsWithClass(info, loginClass);
        const phone = scryRenderedDOMComponentsWithClass(info, phoneClass);

        expect(login.length).to.equal(1);
        expect(phone.length).to.equal(1);
    });

    it("should render only login if phone not passed", () => {
        const initState = {
            Login: "Login"
        };
        const info = renderIntoDocument(<WrappedLoginInfo {...initState} />);

        const login = scryRenderedDOMComponentsWithClass(info, loginClass);
        const phone = scryRenderedDOMComponentsWithClass(info, phoneClass);

        expect(login.length).to.equal(1);
        expect(phone.length).to.equal(0);
    });

    it("should render only phone if login not passed", () => {
        const initState = {
            Phone: "Phone"
        };
        const info = renderIntoDocument(<WrappedLoginInfo {...initState} />);

        const login = scryRenderedDOMComponentsWithClass(info, loginClass);
        const phone = scryRenderedDOMComponentsWithClass(info, phoneClass);

        expect(login.length).to.equal(0);
        expect(phone.length).to.equal(1);
    });
});
