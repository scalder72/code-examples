import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducer";

describe("subscriptions reducer", () => {
    it("should handle begin request", () => {
        const initialState = freeze({
            fetchRequired: true,
            isLoading: false
        });

        const actual = reducer(initialState, actions.fetchSubscriptionsBegin());

        expect(actual.isLoading).to.be.true;
        expect(actual.fetchRequired).to.be.false;
    });

    it("should handle complete request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.fetchSubscriptionsComplete());

        expect(actual.isLoading).to.be.false;
    });

    it("should handle success request", () => {
        const initialState = freeze({});
        const responseData = freeze({
            InactiveCodesGroups: [1],
            ActiveCodes: [1, 2],
            UnattachedRecipients: [1, 2, 3]
        });
        const expected = {
            InactiveCodesGroups: [1],
            ActiveCodes: [1, 2],
            UnattachedRecipients: [1, 2, 3]
        };

        const actual = reducer(initialState, actions.fetchSubscriptionsSuccess(responseData));

        expect(actual).to.deep.equal(expected);
    });

    it("should handle success request with empty data", () => {
        const initialState = freeze({});
        const responseData = freeze({
            InactiveCodesGroups: [],
            ActiveCodes: [],
            UnattachedRecipients: []
        });
        const expected = {
            InactiveCodesGroups: null,
            ActiveCodes: null,
            UnattachedRecipients: null
        };

        const actual = reducer(initialState, actions.fetchSubscriptionsSuccess(responseData));

        expect(actual).to.deep.equal(expected);
    });
});
