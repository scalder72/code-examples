import { expect } from "chai";
import { renderIntoDocument, scryRenderedDOMComponentsWithClass } from "react-addons-test-utils";
import { InactiveCodes, __RewireAPI__ as InactiveCodesRewireAPI } from "../components/ActivationCodes/InactiveCodes.jsx";

describe("InactiveCodes component", () => {
    const inactiveCodeClass = "InactiveCode";
    const inactiveCodesGroupClass = "InactiveCodesGroup";

    InactiveCodesRewireAPI.__Rewire__("InactiveCode", React.createClass({
        render: () => <div className={inactiveCodeClass}></div>
    }));
    InactiveCodesRewireAPI.__Rewire__("InactiveCodesGroup", React.createClass({
        render: () => <div className={inactiveCodesGroupClass}></div>
    }));

    it("should render InactiveCode component for 1 code", () => {
        const initialState = {
            Codes: ["code"]
        };

        const inactiveCodes = renderIntoDocument(
            <InactiveCodes { ...initialState } />
        );

        const inactiveCode = scryRenderedDOMComponentsWithClass(inactiveCodes, inactiveCodeClass);
        const inactiveCodesGroup = scryRenderedDOMComponentsWithClass(inactiveCodes, inactiveCodesGroupClass);

        expect(inactiveCode.length).to.equal(1);
        expect(inactiveCodesGroup.length).to.equal(0);
    });

    it("should render InactiveCodesGroup component for >= 2 codes", () => {
        const initialState = {
            Codes: ["code1", "code2", "code3"]
        };

        const inactiveCodes = renderIntoDocument(
            <InactiveCodes { ...initialState } />
        );

        const inactiveCode = scryRenderedDOMComponentsWithClass(inactiveCodes, inactiveCodeClass);
        const inactiveCodesGroup = scryRenderedDOMComponentsWithClass(inactiveCodes, inactiveCodesGroupClass);

        expect(inactiveCode.length).to.equal(0);
        expect(inactiveCodesGroup.length).to.equal(1);
    });
});
