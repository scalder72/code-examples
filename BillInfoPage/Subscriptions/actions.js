﻿import proxy from "billing-ui/helpers/Proxy";
import { createAction } from "redux-actions";
import * as types from "./actionTypes";

export const fetchSubscriptionsBegin = createAction(types.FETCH_SUBSCRIPTIONS_BEGIN);
export const fetchSubscriptionsComplete = createAction(types.FETCH_SUBSCRIPTIONS_COMPLETE);
export const fetchSubscriptionsSuccess = createAction(types.FETCH_SUBSCRIPTIONS_SUCCESS);
export const fetchSubscriptionsError = createAction(types.FETCH_SUBSCRIPTIONS_ERROR);

export const fetchSubscriptions = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { SubscriptionsUrl } } = billInfo;

    dispatch(fetchSubscriptionsBegin());
    proxy
        .get(SubscriptionsUrl)
        .then(({ data }) => dispatch(fetchSubscriptionsSuccess(data)))
        .catch((e) => dispatch(fetchSubscriptionsError(e)))
        .finally(() => dispatch(fetchSubscriptionsComplete()));
};
