﻿import GoogleAnalytics from "billing-ui/helpers/GoogleAnalytics";

export const publishGAEvent = (action, label) => {
    GoogleAnalytics.triggerEventAsync("bill-subs", action, label);
};
