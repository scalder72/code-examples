﻿import { PropTypes } from "react";
import styles from "./BillTrackItem.scss";

const BillTrackItem = ({ title, children }) => (
    <div className={styles.wrapper}>
        <p className={styles.title}>{title}</p>
        <p>{children}</p>
    </div>
);

BillTrackItem.propTypes = {
    children: PropTypes.node,
    title: PropTypes.string.isRequired
};

export default BillTrackItem;
