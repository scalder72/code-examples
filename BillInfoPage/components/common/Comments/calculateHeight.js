﻿const HIDDEN_NODE_STYLE = `
  min-height:none !important;
  max-height:none !important;
  height:0 !important;
  visibility:hidden !important;
  overflow:hidden !important;
  position:absolute !important;
  z-index:-1000 !important;
  top:-10000em !important;
  left:-10000em !important
`;

const SIZING_STYLE = [
    "letter-spacing",
    "line-height",
    "padding-top",
    "padding-bottom",
    "font-family",
    "font-weight",
    "font-size",
    "text-rendering",
    "text-transform",
    "white-space",
    "word-wrap",
    "width",
    "padding-left",
    "padding-right",
    "border-width",
    "box-sizing"
];

let computedStyleCache = {};
let hiddenNode;

export default function calculateHeight(node, useCache = false) {
    if (!hiddenNode) {
        hiddenNode = document.createElement("div");
        document.body.appendChild(hiddenNode);
    }

    // Copy all CSS properties that have an impact on the height of the content in
    // the textbox
    let {
        paddingSize, borderSize,
        boxSizing, sizingStyle
        } = calculateStyling(node, useCache);

    // Need to have the overflow attribute to hide the scrollbar otherwise
    // text-lines will not calculated properly as the shadow will technically be
    // narrower for content
    hiddenNode.setAttribute("style", sizingStyle + ";" + HIDDEN_NODE_STYLE);
    $(hiddenNode).text($(node).text());

    let height = hiddenNode.scrollHeight;
    hiddenNode.className = hiddenNode.className + "";

    if (boxSizing === "border-box") {
        // border-box: add border, since height = content + padding + border
        height = height + borderSize;
    } else if (boxSizing === "content-box") {
        // remove padding, since height = content
        height = height - paddingSize;
    }
    return height;
}

function calculateStyling(node, useCache = false) {
    let nodeRef = (
        node.getAttribute("id") ||
        node.getAttribute("data-reactid") ||
        node.getAttribute("name")
    );

    if (useCache && computedStyleCache[nodeRef]) {
        return computedStyleCache[nodeRef];
    }

    let style = window.getComputedStyle(node);

    let boxSizing = (
        style.getPropertyValue("box-sizing") ||
        style.getPropertyValue("-moz-box-sizing") ||
        style.getPropertyValue("-webkit-box-sizing")
    );

    let paddingSize = (
        parseFloat(style.getPropertyValue("padding-bottom")) +
        parseFloat(style.getPropertyValue("padding-top"))
    );

    let borderSize = (
        parseFloat(style.getPropertyValue("border-bottom-width")) +
        parseFloat(style.getPropertyValue("border-top-width"))
    );

    let sizingStyle = SIZING_STYLE
        .map(name => `${name}:${style.getPropertyValue(name)}`)
        .join(";");

    let nodeInfo = {
        sizingStyle,
        paddingSize,
        borderSize,
        boxSizing
    };

    if (useCache && nodeRef) {
        computedStyleCache[nodeRef] = nodeInfo;
    }

    return nodeInfo;
}
