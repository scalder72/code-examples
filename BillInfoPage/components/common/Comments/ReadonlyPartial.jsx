﻿import { Component, PropTypes } from "react";

import calculateHeight from "./calculateHeight";
import utils from "../../../../Shared/utils"

import cx from "classnames";
import styles from "./CommentsBox.scss";

class ReadonlyPartial extends Component {
    maxHeight = 90;
    fullHeight = 80;
    textBlock = null;

    state = {
        hasOverflow: false
    }

    componentDidMount = () => {
        const { onOverflowCalculated } = this.props;

        this.textBlock = $("." + styles["readonly_text"])[0];// когда будет более одного комментария — передать сюда index

        if (utils.isIE8) {
            setTimeout(() => this._resolveOverflow(), 0);
        } else {
            this._resolveOverflow();
        }

        onOverflowCalculated(this.state.hasOverflow);
    };

    componentDidUpdate = () => {
        const { onOverflowCalculated } = this.props;
        onOverflowCalculated(this.state.hasOverflow);
    };

    render() {
        const {savedComment, masked} = this.props;
        const text = savedComment || "Написать комментарий";

        let toggleText = masked ? "показать полностью" : "скрыть";
        let textClasses = cx(styles["readonly_text"], {
            [styles["readonly_overflowing-text"]]: this.state.hasOverflow,
            [styles["readonly_overflowing-text__masked"]]: masked
        });

        return (
            <div className={styles.readonly}>
                <div className={textClasses}
                     style={{height: this.state.hasOverflow ? (masked ? this.maxHeight : this.fullHeight) : ""}}>
                    {text}
                </div>
                {this.state.hasOverflow && <span className={styles["toggle-full-comment"]}>{toggleText}</span>}
            </div>
        );
    }

    _resolveOverflow = () => {
        this._updateFullHeight();
        this._updateOverflow();
    };

    _updateFullHeight = () => {
        this.fullHeight = calculateHeight(this.textBlock);
    };

    _updateOverflow = () => {
        if (this.state.hasOverflow !== this.fullHeight > this.maxHeight) {
            this.setState({ hasOverflow: this.fullHeight > this.maxHeight });
        }
    }
}

ReadonlyPartial.propTypes = {
    onOverflowCalculated: PropTypes.func.isRequired,
    savedComment: PropTypes.string,
    masked: PropTypes.bool
};

export default ReadonlyPartial;
