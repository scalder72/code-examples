﻿import { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Loader from "react-ui/Loader";

import {
    fetchCommentaries,
    deleteCommentary,
    createCommentary,
    updateCommentary
} from "../../../actions/commentariesActions";

import Comment from "./Comment.jsx";
import styles from "./CommentsBox.scss";

class CommentsBox extends Component {
    componentDidMount() {
        const { wasLoaded, url, billId, fetchCommentaries } = this.props;

        if (!wasLoaded) {
            fetchCommentaries(url, billId);
        }
    }

    _renderCommentary = (commentary) => (
        <Comment key={commentary.CommentaryId} {...commentary}
                 onSave={(text, commentaryId, oldMessage) => this._onUpdate(text, commentaryId, oldMessage)}
                 onDelete={(CommentaryId) => this._onDelete(CommentaryId)}
        />
    );

    _renderCommentaries = (partnersCommentaries = []) => (
        <div>
            {partnersCommentaries.length > 0 && partnersCommentaries.map(this._renderCommentary)}
            {partnersCommentaries.length === 0 && (
                <Comment stubComment={this.props.stubComment} onSave={(text) => this._onCreate(text)} />
            )}
        </div>
    );

    _renderReadOnlyCommentaries = (partnersCommentaries = []) => {
        return partnersCommentaries.map((commentary) => (
            <Comment key={commentary.CommentaryId} {...commentary} readOnly={true} />
        ));
    };

    render() {
        const { commentaries = [], isLoading, wasLoaded, IsImported } = this.props;

        if (IsImported && commentaries.length === 0) {
            return null;
        }

        return (
            <div className={styles.wrapper}>
                <Loader active={isLoading} caption="">
                    {wasLoaded && IsImported && this._renderReadOnlyCommentaries(commentaries)}
                    {wasLoaded && !IsImported && this._renderCommentaries(commentaries)}
                </Loader>
            </div>
        );
    }

    _onDelete = CommentaryId => {
        const { url, billId, deleteCommentary } = this.props;
        deleteCommentary(url, CommentaryId, billId);
    };

    _onCreate = (text) => {
        const { url, createCommentary, billId } = this.props;
        createCommentary(url, billId, text);
    };

    _onUpdate = (text, CommentaryId, oldMessage) => {
        const { url, updateCommentary, billId } = this.props;
        updateCommentary(url, billId, text, CommentaryId, oldMessage);
    }
}

CommentsBox.propTypes = {
    IsImported: PropTypes.bool.isRequired,

    url: PropTypes.string.isRequired,
    billId: PropTypes.string.isRequired,
    isLoading: PropTypes.bool.isRequired,
    wasLoaded: PropTypes.bool.isRequired,
    fetchCommentaries: PropTypes.func.isRequired,
    deleteCommentary: PropTypes.func.isRequired,
    createCommentary: PropTypes.func.isRequired,
    updateCommentary: PropTypes.func.isRequired,
    commentaries: PropTypes.array,
    stubComment: PropTypes.object
};

CommentsBox.defaultProps = {
    isLoading: false,
    wasLoaded: false
};

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchCommentaries,
    deleteCommentary,
    createCommentary,
    updateCommentary
}, dispatch);

const mapStateToProps = ({ billInfo: { commentaries, mainBillInfo: { IsImported } } }) => ({
    ...commentaries,
    IsImported
});

export default connect(mapStateToProps, mapDispatchToProps)(CommentsBox);
