﻿import { Component, PropTypes } from "react";
import Textarea from "../../../../Shared/react-textarea-autosize";
import Icon, { IconTypes } from "billing-ui/components/Icon";

import styles from "./CommentsBox.scss";
import cx from "classnames";

class EditablePartial extends Component {
    limit = 2000;

    constructor(props) {
        super(props);
        this.state = { text: props.editableText };
    }

    componentDidMount() {
        this._focusTextarea();
    }

    componentWillReceiveProps() {
        this._focusTextarea();
    }

    handleTextChange = (e) => {
        let text = e.target.value;

        if (text.length > this.limit) {
            text = text.substr(0, this.limit)
        }

        this.setState({text: text});
        this.props.onUserInput(text);
    };

    render() {
        const {savedComment, onCancelClick, onDeleteClick} = this.props;
        let saveButtonStyles = cx(styles.save, {[styles.disabled]: !this._canSave()});

        return (
            <form>
                <Textarea id="CommentTextarea"
                          placeholder="Написать комментарий"
                          maxLength={this.limit}
                          value={this.state.text}
                          onKeyUp={this._handleEscape}
                          onChange={this.handleTextChange} />

                <div className={styles.controls}>
                    <button className={saveButtonStyles} onClick={this._onSaveClick} type="submit">
                        <span className={styles.buttonspan}>Сохранить</span>
                    </button>

                    <div className={styles.cancel} onClick={onCancelClick}>Отменить</div>

                    {savedComment && (
                        <div className={styles.delete} onClick={onDeleteClick}><Icon type={IconTypes.Trash} />
                            Удалить
                        </div>
                    )}
                </div>
            </form>
        );
    }

    _canSave = () => {
        const trimmedInput = $.trim(this.state.text);
        return trimmedInput.length !== 0 && trimmedInput !== $.trim(this.props.savedComment)
    };

    _onSaveClick = e => {
        e.preventDefault();
        if (this._canSave()) {
            this.props.onSaveClick();
        }
    };

    _handleEscape = (e) => {
        const ENTER = 13;
        const ESCAPE = 27;
        const {onCancelClick} = this.props;

        if (e.keyCode === ESCAPE) {
            onCancelClick();
        } else if (e.ctrlKey && e.keyCode === ENTER) {
            this._onSaveClick(e);
        }
    };

    _focusTextarea = () => {
        setTimeout(() => {
            const $textarea = $("#CommentTextarea");
            const val = this.state.text;

            if ($.trim(val).length > 1 && val.charAt(val.length - 1) !== " ") {
                this.setState({ text: this.state.text + " " });
            }

            $textarea
                .focus()
                .val("")
                .val(this.state.text);
        }, 0);
    };
}

EditablePartial.propTypes = {
    onCancelClick: PropTypes.func.isRequired,
    onDeleteClick: PropTypes.func.isRequired,
    onSaveClick: PropTypes.func.isRequired,
    onUserInput: PropTypes.func.isRequired,
    savedComment: PropTypes.string,
    editableText: PropTypes.string
};

export default EditablePartial;
