﻿import { Component, PropTypes } from "react";
import listensToClickOutside from "react-onclickoutside/decorator";
import cx from "classnames";

import Icon, { IconTypes } from "billing-ui/components/Icon";

import ReadonlyPartial from "./ReadonlyPartial.jsx";
import EditablePartial from "./EditablePartial.jsx";
import DateWithTime from "../../../Shared/components/DateWithTime.jsx";
import { safeDecodeURI } from "../../../../Shared/utils";

import styles from "./CommentsBox.scss";

@listensToClickOutside()
class Comment extends Component {
    userInput = null;
    hasOverflow = false;

    state = {
        isActive: false,
        isMasked: false
    };

    componentDidUpdate = () => {
        const { enableOnClickOutside, disableOnClickOutside } = this.props;

        if (this.state.isActive) {
            enableOnClickOutside();
        } else {
            disableOnClickOutside();
        }

        setTimeout(() => {
            $("." + styles["wrapper"]).toggleClass(styles["ie8fix"]);
        }, 0);
    };

    componentWillReceiveProps = (nextProps) => {
        const stubComment = nextProps.stubComment;

        if (!stubComment) {
            if (nextProps.isActive === undefined) {
                this.setState({ isActive: false });
            } else {
                this.setState({ isActive: nextProps.isActive });
            }

            if (nextProps.userInput) {
                this.userInput = safeDecodeURI(nextProps.userInput);
            }
        } else {
            if (stubComment.userInput) {
                this.userInput = safeDecodeURI(stubComment.userInput);
            }

            if (stubComment.isActive) {
                this.setState({ isActive: stubComment.isActive });
            }
        }
    };

    _getStateView = (savedComment, editableText) => {
        if (this.state.isActive) {
            return (
                <EditablePartial editableText={editableText}
                                 savedComment={savedComment}
                                 onSaveClick={this._onSave}
                                 onCancelClick={this._onCancel}
                                 onDeleteClick={this._onDelete}
                                 onUserInput={this._storeUserInput}
                />
            );
        } else {
            return (
                <ReadonlyPartial savedComment={savedComment}
                                 masked={this.state.isMasked}
                                 onOverflowCalculated={(overflow) => this._handleOverflow(overflow)}
                />
            );
        }
    };

    render() {
        const { Author, Date, Text, stubComment, readOnly } = this.props;
        const decodedText = safeDecodeURI(Text);
        const stubDecodedText = stubComment && safeDecodeURI(stubComment.Text);
        const decodedUserInput = safeDecodeURI(this.userInput);

        const itemClasses = cx(styles.item, {
            [styles["is-editable"]]: !readOnly,
            [styles["is-active"]]: this.state.isActive,
            [styles["is-empty"]]: !decodedText,
            [styles["is-masked"]]: this.state.isMasked,
            [styles["has-overflow"]]: this.hasOverflow
        });

        return (
            <div className={itemClasses} onClick={(e) => { this._onClick(e); }}>
                <Icon className={styles.icon} type={IconTypes.CommentLite} />

                {(Author || Date) && (
                    <div className={styles.created}>
                        {Author && <div className={styles.author}>{Author}</div> }
                        {Date && <DateWithTime date={Date} className={styles.date} /> }
                    </div>
                )}

                {this._getStateView(
                    this._getReadonlyText(stubDecodedText, decodedText),
                    this._getEditableText(decodedUserInput, decodedText)
                )}
            </div>
        );
    }

    _onClick = (e) => {
        const { readOnly } = this.props;
        const maskedStateToggler = $(e.currentTarget).find("." + styles["toggle-full-comment"])[0];

        if (this.state.isMasked || e.target === maskedStateToggler) {
            this.setState({ isMasked: !this.state.isMasked });
        } else if (!readOnly && !this.state.isActive) {
            this.setState({ isActive: true });
        }
    };

    handleClickOutside = () => {
        this.setState({ isActive: false });
    };

    _handleOverflow = (readonlyOverflow) => {
        if (this.hasOverflow !== readonlyOverflow) {
            this.hasOverflow = readonlyOverflow;

            if (readonlyOverflow) {
                this.setState({ isMasked: true });
            } else {
                this.setState({ isMasked: false });
            }
        }
    };

    _onCancel = () => {
        this.setState({ isActive: false });
        this._clearUserInput();
    };

    _onSave = () => {
        const newMessage = $.trim(this.userInput);
        this._clearUserInput();

        const { onSave, CommentaryId, Text } = this.props;
        onSave && onSave(newMessage, CommentaryId, Text);
    };

    _onDelete = () => {
        this._onCancel();

        const { onDelete, CommentaryId } = this.props;
        onDelete(CommentaryId);
    };

    _storeUserInput = (userInput) => {
        this.userInput = userInput;
    };

    _clearUserInput = () => {
        this.userInput = null;
    };

    _getEditableText = (userInput, savedComment) => userInput || savedComment;

    _getReadonlyText = (stubCommentText, savedComment) => stubCommentText || savedComment;
}

Comment.propTypes = {
    readOnly: PropTypes.bool.isRequired,

    enableOnClickOutside: PropTypes.func,
    disableOnClickOutside: PropTypes.func,

    stubComment: PropTypes.object,
    onSave: PropTypes.func,
    onDelete: PropTypes.func,

    CommentaryId: PropTypes.string,
    Text: PropTypes.string,
    Author: PropTypes.string,
    Date: PropTypes.string
};

Comment.defaultProps = {
    readOnly: false
};

export default Comment;
