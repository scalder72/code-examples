﻿import { PropTypes } from "react";

import DocumentTitle from "./../../Components/Shared/components/DocumentTitle";
import FetchDataContainer from "./../../Components/Shared/components/FetchDataContainer";
import styles from "./../Layout/styles/BillInfoPage.scss";

const ClientWrapper = (props) => {
    const { mainBillInfo: { BillNumber, PayerName } = {} } = props;
    const pageTitle = BillNumber && PayerName ? `${PayerName} | Счет №${BillNumber}` : null;

    return (
        <DocumentTitle title={pageTitle}>
            <FetchDataContainer showLoader={true} {...props}>
                <div className="content-body content-body__fluid">
                    <div className={styles.billInfoPage}>
                        {props.children}
                    </div>
                </div>
            </FetchDataContainer>
        </DocumentTitle>
    );
};

ClientWrapper.propTypes = {
    children: PropTypes.node,
    mainBillInfo: PropTypes.object
};

export default ClientWrapper;
