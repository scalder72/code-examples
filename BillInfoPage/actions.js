﻿import { createAction } from "redux-actions";
import * as types from "./actionTypes";

export const fetchBillInfoBegin = createAction(types.FETCH_BILL_INFO_BEGIN);
export const fetchBillInfoSuccess = createAction(types.FETCH_BILL_INFO_SUCCESS);
export const fetchBillInfoError = createAction(types.FETCH_BILL_INFO_ERROR);
