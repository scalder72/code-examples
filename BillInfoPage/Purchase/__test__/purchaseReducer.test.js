import { expect } from "chai";
import * as actions from "../actions";
import freeze from "deep-freeze";
import reducer from "../reducer";

describe("purchase fetch reducer", () => {
    it("should handle begin request", () => {
        const initialState = freeze({
            fetchRequired: true,
            isLoading: false
        });

        const actual = reducer(initialState, actions.fetchPurchaseBegin());

        expect(actual.isLoading).to.be.true;
        expect(actual.fetchRequired).to.be.false;
    });

    it("should handle error request", () => {
        const initialState = freeze({
            isLoading: true
        });
        const actual = reducer(initialState, actions.fetchPurchaseError());

        expect(actual.isLoading).to.be.false;
    });

    it("should handle success request", () => {
        const initialState = freeze({});
        const responseData = freeze({
            PackageInfos: [1, 2]
        });
        const expected = {
            PackageInfos: [1, 2],
            isLoading: false
        };

        const actual = reducer(initialState, actions.fetchPurchaseSuccess(responseData));

        expect(actual).to.deep.equal(expected);
    });
});
