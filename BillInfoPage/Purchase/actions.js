﻿import { createAction } from "redux-actions";
import axios from "billing-ui/libs/axios";
import Informer from "Informer";

import * as actions from "./actionTypes";

export const fetchPurchaseBegin = createAction(actions.FETCH_PURCHASE_BEGIN);
export const fetchPurchaseSuccess = createAction(actions.FETCH_PURCHASE_SUCCESS);
export const fetchPurchaseError = createAction(actions.FETCH_PURCHASE_ERROR);

export const handleConditionalPaymentUpdate = createAction(actions.CONDITIONAL_PAYMENT_UPDATED);

export const fetchPurchase = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { PurchaseUrl } } = billInfo;

    dispatch(fetchPurchaseBegin());
    axios
        .get(PurchaseUrl)
        .then((response) => dispatch(fetchPurchaseSuccess(response.data)))
        .catch(({ data }) => {
            Informer.show({ message: data && data.Message ? data.Message : "Произошла ошибка", status: InformerType.error, iconType: IconType.error});
            return dispatch(fetchPurchaseError());
        });
};
