﻿import { handleActions } from "redux-actions";
import * as actions from "./actionTypes";
import { CONDITIONAL_PAYMENT_CREATED } from "../ActionButtonsList/actionTypes";

export default handleActions({
    [actions.FETCH_PURCHASE_BEGIN]: (state, action) => ({
        ...state,
        fetchRequired: false,
        isLoading: true
    }),
    [actions.FETCH_PURCHASE_SUCCESS]: (state, action) => ({
        ...state,
        ...action.payload,
        isLoading: false
    }),
    [actions.FETCH_PURCHASE_ERROR]: (state, action) => ({
        ...state,
        isLoading: false
    }),

    [CONDITIONAL_PAYMENT_CREATED]: (state) => ({
        ...state,
        fetchRequired: true
    }),

    [actions.CONDITIONAL_PAYMENT_UPDATED]: (state) => ({
        ...state,
        fetchRequired: true
    })
}, {
    fetchRequired: true,
    isLoading: false
});
