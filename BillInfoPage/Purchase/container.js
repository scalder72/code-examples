﻿import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchPurchase } from "./actions";
import PurchaseWrapper from "./components/PurchaseWrapper.jsx";


const mapStateToProps = ({ billInfo: { purchaseInfo } = {}}) => ({ ...purchaseInfo });
const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchPurchase }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseWrapper);
