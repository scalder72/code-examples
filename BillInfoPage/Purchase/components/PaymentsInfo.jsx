﻿import { PropTypes } from "react";
import cx from "classnames";
import { formatDate } from "billing-ui/libs/moment";

import Icon, {IconTypes} from "billing-ui/components/Icon";
import PaymentPriceFormat from "./PaymentPriceFormat.jsx";
import ConditionalPayment from "./ConditionalPayment.jsx";
import PaymentComponent from "./Payment.jsx";

import { PaymentTypes, resolvePaymentType, customizePaymentStatuses, resolvePaymentStatus } from "./../utils/paymentsInfoHelper";
import conditionalPaymentType from "./../utils/conditionalPaymentType";
import conditionalPaymentState from "./../utils/conditionalPaymentState";
import styles from "./../styles/PaymentsInfo.scss";

customizePaymentStatuses(styles, IconTypes);

const renderConditionalPayments = (items = [], fullyPaid) => {
    return items
        .filter(item => item.ConditionalPaymentType !== conditionalPaymentType.WithoutDocuments)
        .map(item => <ConditionalPayment key={item.ConditionalPaymentId} active={!fullyPaid} {...item} />);
};

const renderPayments = (items = [], fullyPaid) => items.map((item, index) => (
    <PaymentComponent key={item.PaymentId} active={!fullyPaid} {...item} />
));

const paidBySupportHeader = (status, conditionalPayment) => (
    <div className={styles["paid-support-header"]}>
        <Icon className={styles["clock-icon"]} type={IconTypes.Clock} />
        Условно оплачено техподдержкой до {formatDate(conditionalPayment.ExpirationDate)}
    </div>
);

const PaymentsInfo = ({ TotalAmount, PaidAmount, Payments, ConditionalPayments, IsNonRepayable }) => {
    const type = resolvePaymentType(PaidAmount, TotalAmount, ConditionalPayments, IsNonRepayable);
    const status = resolvePaymentStatus(type);

    const fullyPaid = type === PaymentTypes.FullyPaid;
    const hasPaidAmount = PaidAmount > 0;

    const hasConditionalPayments = ConditionalPayments.length > 0;

    const conditionalPaymentsBySupport = ConditionalPayments.filter(item => {
        return item.ConditionalPaymentType === conditionalPaymentType.WithoutDocuments &&
            item.ConditionalPaymentState === conditionalPaymentState.Accepted
    });

    const paidBySupport = conditionalPaymentsBySupport.length > 0 && !fullyPaid;
    const showConditionalPayments = hasConditionalPayments && !paidBySupport;

    const hasPayments = Payments.length > 0;
    const hasSinglePayment = Payments.length === 1;
    const showPayments = hasPayments && !hasSinglePayment;

    const totalAmountClasses = cx(styles["total-amount"], { [styles["with-paid"]]: hasPaidAmount || IsNonRepayable });
    const singleBlock = !showPayments || (!hasConditionalPayments || paidBySupport);

    return (
        <div>
            <div className={styles.block}>
                { paidBySupport && paidBySupportHeader(status, conditionalPaymentsBySupport[0])}

                <div className={cx(styles.table, { [styles.inactive]: paidBySupport }) }>
                    <div>
                        { paidBySupport && (
                            <span>
                                { status.text } { hasSinglePayment && formatDate(Payments[0].Date) }
                            </span>
                        )}

                        { !paidBySupport && (
                            <span>
                                {status.iconType && <Icon className={status.iconClass} type={status.iconType} />}
                                {status.text} {hasSinglePayment && formatDate(Payments[0].Date)}
                            </span>
                        )}

                        { (hasPaidAmount || IsNonRepayable) && (
                            <div className={styles["to-right"]}>
                                { IsNonRepayable && "безвозмездно" }
                                { !IsNonRepayable && <PaymentPriceFormat amount={PaidAmount} /> }
                            </div>
                        )}

                        { showPayments && renderPayments(Payments, fullyPaid || paidBySupport) }
                    </div>

                    <div className={totalAmountClasses}>
                        <PaymentPriceFormat amount={TotalAmount} />
                    </div>
                </div>

                { singleBlock && showConditionalPayments && renderConditionalPayments(ConditionalPayments, fullyPaid || paidBySupport) }
            </div>

            { showConditionalPayments && !singleBlock && (
                <div className={styles.block}>
                    { renderConditionalPayments(ConditionalPayments, fullyPaid || paidBySupport) }
                </div>
            )}
        </div>
    );
};

PaymentsInfo.propTypes = {
    TotalAmount: PropTypes.number.isRequired,
    PaidAmount: PropTypes.number.isRequired,
    IsNonRepayable: PropTypes.bool.isRequired,

    Payments: PropTypes.arrayOf(PropTypes.shape({
        PaymentId: PropTypes.string.isRequired
    })).isRequired,

    ConditionalPayments: PropTypes.arrayOf(PropTypes.shape({
        ConditionalPaymentId: PropTypes.string.isRequired,
        ConditionalPaymentType: PropTypes.string.isRequired,
        ConditionalPaymentState: PropTypes.string.isRequired,
        ExpirationDate: PropTypes.string.isRequired
    })).isRequired
};

export default PaymentsInfo;
