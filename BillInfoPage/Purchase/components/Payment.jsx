﻿import { PropTypes } from "react";
import moment from "billing-ui/libs/moment";
import cx from "classnames";

import PaymentPriceFormat from "./PaymentPriceFormat.jsx";
import paymentInfoStyles from "./../styles/PaymentsInfo.scss";
import styles from "./../styles/Payment.scss";

const Payment = ({ Date, Sum, active }) => (
    <div className={cx(paymentInfoStyles.detail, { [styles.inactive]: !active })}>
        {moment(Date).format("L")}
        <div className={paymentInfoStyles["to-right"]}>
            <PaymentPriceFormat amount={Sum} />
        </div>
    </div>
);

Payment.propTypes = {
    active: PropTypes.bool.isRequired,
    Sum: PropTypes.number.isRequired,
    Date: PropTypes.string.isRequired
};

export default Payment;
