﻿import { Component, PropTypes } from "react";
import debounce from "debounce";

import Loader from "react-ui/Loader";
import Spinner from "react-ui/Spinner";

import PaymentsInfoComponent from "./PaymentsInfo.jsx";
import PackageInfo from "./PackageInfo.jsx";


const renderPackageInfos = (items = []) => items.map((item, index, allItems) => {
    const showIndex = allItems.length > 1;
    const orderNumber = index + 1;

    return <PackageInfo {...item} key={index} number={orderNumber} showIndex={showIndex } />
});

class PurchaseWrapper extends Component {
    constructor(props) {
        super(props);
        this._fetchPurchase = debounce(props.fetchPurchase, 200);
    }

    componentDidMount() {
        this._resolveDataFetching(this.props);
    }

    componentDidUpdate() {
        this._resolveDataFetching(this.props);
    }

    _resolveDataFetching = ({ fetchRequired }) => {
        if (fetchRequired) {
            this._fetchPurchase();
        }
    };

    render() {
        const { PackageInfos, PaymentsInfo, isLoading } = this.props;

        return (
            <Loader active={isLoading} type={Spinner.Types.big}>
                {renderPackageInfos(PackageInfos)}
                {PaymentsInfo && <PaymentsInfoComponent {...PaymentsInfo} />}
            </Loader>
        );
    }
}

PurchaseWrapper.propTypes = {
    fetchPurchase: PropTypes.func.isRequired,
    fetchRequired: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    PackageInfos: PropTypes.array,
    PaymentsInfo: PropTypes.object
};

export default PurchaseWrapper;
