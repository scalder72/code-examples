﻿import { PropTypes } from "react";
import PriceFormat from "billing-ui/components/PriceFormat";

const PaymentPriceFormat = ({ amount }) => (
    <PriceFormat amount={amount} fractionSeparator="," alwaysWithFraction={true} />
);

PaymentPriceFormat.propTypes = {
    amount: PropTypes.number.isRequired
};

export default PaymentPriceFormat;
