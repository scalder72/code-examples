﻿/* global ConditionalPaymentPopupControl */

import { Component, PropTypes } from "react";
import reactDOM from "react-dom";

import PopupByLink from "popup-by-link";

class ConditionalPaymentLink extends Component {
    componentDidMount() {
        this._initConditionalPaymentControls();
    }

    componentDidUpdate() {
        this._removeConditionalPaymentControls();
        this._initConditionalPaymentControls();
    }

    componentWillUnmount() {
        this._removeConditionalPaymentControls();
    }

    _initConditionalPaymentControls() {
        const { canUpdate, onUpdate } = this.props;

        if (canUpdate) {
            const link = reactDOM.findDOMNode(this);

            this._conditionalPayment = new ConditionalPaymentPopupControl({
                link: link,
                popupClassName: "center",
                popupPosition: {
                    top: link.offsetHeight + 12
                }
            });

            this._conditionalPayment.onFormActionSuccess(onUpdate);
        } else {
            this._popupByLink = new PopupByLink({ link: reactDOM.findDOMNode(this) });
        }
    }

    _removeConditionalPaymentControls() {
        if (this._popupByLink) {
            this._popupByLink.remove();
            this._popupByLink = null;
        }

        if (this._conditionalPayment) {
            this._conditionalPayment.remove();
            this._conditionalPayment = null;
        }
    }

    render() {
        const { children, url, className } = this.props;

        return (
            <a className={className} href={url}>{children}</a>
        );
    }
}

ConditionalPaymentLink.propTypes = {
    url: PropTypes.string.isRequired,
    canUpdate: PropTypes.bool.isRequired,
    className: PropTypes.string,
    children: PropTypes.node,
    onUpdate: PropTypes.func.isRequired
};

export default ConditionalPaymentLink;
