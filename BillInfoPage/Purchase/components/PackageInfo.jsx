﻿import { PropTypes } from "react";

import cx from "classnames";
import styles from "./../styles/PackageInfo.scss";

const renderPropertiesList = (items = []) => items.map((val, index) => (
    <div key={index} className={styles.prop}>{val}</div>
));

const renderPackageGoodsItems = (items = []) => items.map((item, index) => (
    <div key={index} className={styles.item}>
        {item.Title}
        {renderPropertiesList(item.Props)}
    </div>
));

const Package = ({ Title, TariffCount, PackageGoods, number, showIndex }) => {
    const itemClasses = cx(styles.wrapper, {
        [styles["with-numbers"]]: showIndex
    });

    return (
        <div className={itemClasses}>
            <div className={styles.header}>
                {showIndex && <div className={styles.number}>{number}.</div>}
                <div className={styles.count}>{TariffCount}</div>
                <div className={styles.title}>{Title}</div>
            </div>

            {PackageGoods && renderPackageGoodsItems(PackageGoods)}
        </div>
    );
};

Package.propTypes = {
    number: PropTypes.number,
    showIndex: PropTypes.bool,
    Title: PropTypes.string,
    TariffCount: PropTypes.number,
    PackageGoods: PropTypes.arrayOf(PropTypes.shape({
        Title: PropTypes.string,
        Props: PropTypes.array
    }))
};

export default Package;
