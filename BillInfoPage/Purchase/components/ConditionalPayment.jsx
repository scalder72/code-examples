﻿import { PropTypes } from "react";
import cx from "classnames";
import { connect } from "react-redux";
import { formatDate } from "billing-ui/libs/moment";

import ConditionalPaymentLink from "./ConditionalPaymentLink.jsx";

import state from "./../utils/conditionalPaymentState";
import type from "./../utils/conditionalPaymentType";
import { handleConditionalPaymentUpdate } from "./../actions";

import paymentInfosStyles from "./../styles/PaymentsInfo.scss";
import styles from "./../styles/ConditionalPayment.scss";

const createConditionalPaymentUrl = (billId, conditionalPaymentId, canUpdate, getInfoUrl, getUpdateUrl) => {
    if (canUpdate) {
        return `${getUpdateUrl}?billId=${billId}&conditionalPaymentId=${conditionalPaymentId}`;
    } else {
        return `${getInfoUrl}?conditionalPaymentId=${conditionalPaymentId}`;
    }
};

const ConditionalPayment = (props) => {
    const { ExpirationDate, ConditionalPaymentState, ConditionalPaymentType, ConditionalPaymentId, CanUpdate,
              active, getInfoUrl, getUpdateUrl, billId, onUpdateConditionalPayment } = props;

    const title = `${type.getDescription(ConditionalPaymentType)} до ${formatDate(ExpirationDate)}`;

    const blockClassName = cx(paymentInfosStyles.detail, styles.block, { [styles.inactive]: !active });
    const stateClassName = cx(paymentInfosStyles["to-right"], styles.state, styles[ConditionalPaymentState.toLowerCase()]);
    const conditionalPaymentUrl = createConditionalPaymentUrl(billId, ConditionalPaymentId, CanUpdate, getInfoUrl, getUpdateUrl);

    return (
        <div className={blockClassName}>
            <ConditionalPaymentLink url={conditionalPaymentUrl}
                                    className={styles.link}
                                    canUpdate={CanUpdate}
                                    onUpdate={onUpdateConditionalPayment}
            >
                { title }
            </ConditionalPaymentLink>

            <div className={stateClassName}>
                { state.getDescription(ConditionalPaymentState) }
            </div>
        </div>
    );
};

ConditionalPayment.propTypes = {
    getInfoUrl: PropTypes.string.isRequired,
    getUpdateUrl: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    billId: PropTypes.string.isRequired,

    ExpirationDate: PropTypes.string.isRequired,
    CanUpdate: PropTypes.bool.isRequired,
    ConditionalPaymentId: PropTypes.string.isRequired,
    ConditionalPaymentType: PropTypes.string.isRequired,
    ConditionalPaymentState: PropTypes.string.isRequired,

    onUpdateConditionalPayment: PropTypes.func.isRequired
};

const mapStateToProps = ({ billInfo: { urls = {}, mainBillInfo = {} }}) => ({
    billId: mainBillInfo.BillId,
    getInfoUrl: urls.ConditionalPaymentInfoUrl,
    getUpdateUrl: urls.GetUpdateConditionalPaymentFormUrl
});

export default connect(mapStateToProps, { onUpdateConditionalPayment: handleConditionalPaymentUpdate })(ConditionalPayment);
