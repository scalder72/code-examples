﻿import conditionalPaymentType from "./conditionalPaymentType";
import conditionalPaymentState from "./conditionalPaymentState";

const isConditionallyPaid = ({ ConditionalPaymentState, ConditionalPaymentType }) => {
    return ConditionalPaymentType !== conditionalPaymentType.WithoutDocuments &&
            ConditionalPaymentState === conditionalPaymentState.Accepted;
};

export const PaymentTypes = {
    NotPaid: "NotPaid",
    FullyPaid: "FullyPaid",
    PartiallyPaid: "PartiallyPaid",
    ConditionallyPaid: "ConditionallyPaid"
};

export const resolvePaymentType = (paidAmount, totalAmount, conditionalPayments, isNonRepayable) => {
    if (paidAmount >= totalAmount || isNonRepayable) {
        return PaymentTypes.FullyPaid;
    }

    if (conditionalPayments.some(item => isConditionallyPaid(item))) {
        return PaymentTypes.ConditionallyPaid;
    }

    if (paidAmount <= 0) {
        return PaymentTypes.NotPaid;
    }

    return PaymentTypes.PartiallyPaid;
};

const PaymentStatusByType = {};
export const customizePaymentStatuses = (styles = {}, iconTypes = {}) => {
    PaymentStatusByType[PaymentTypes.NotPaid] = {
        iconClass: styles["warning-icon"],
        iconType: iconTypes.Warning,
        text: "Не оплачено"
    };

    PaymentStatusByType[PaymentTypes.FullyPaid] = {
        iconClass: styles["tick-icon"],
        iconType: iconTypes.Tick,
        text: "Оплачено"
    };

    PaymentStatusByType[PaymentTypes.PartiallyPaid] = {
        iconClass: styles["clock-icon"],
        iconType: iconTypes.Clock,
        text: "Частично оплачено"
    };

    PaymentStatusByType[PaymentTypes.PartiallyPaid] = {
        iconClass: styles["clock-icon"],
        iconType: iconTypes.Clock,
        text: "Частично оплачено"
    };

    PaymentStatusByType[PaymentTypes.ConditionallyPaid] = {
        iconClass: styles["clock-icon"],
        iconType: iconTypes.Clock,
        text: "Условно оплачено"
    };
};

export const resolvePaymentStatus = (paymentType) => {
    const statusByType = PaymentStatusByType[paymentType];

    if (!statusByType) {
        throw new Error(`Unresolved paymentType "${paymentType}"`);
    }

    return statusByType;
};
