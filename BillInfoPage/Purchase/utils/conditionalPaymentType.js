﻿import { getDescriptionCreator } from "../../../Shared/utils";

const type = {
    GuaranteeLetter: "GuaranteeLetter",
    PaymentOrder: "PaymentOrder",
    OfficialLetter: "OfficialLetter",
    WithoutDocuments: "WithoutDocuments"
};

type.getDescription = getDescriptionCreator({
    [type.GuaranteeLetter]: "Гарантийное письмо",
    [type.PaymentOrder]: "Платежное поручение",
    [type.OfficialLetter]: "Служебное письмо",
    [type.WithoutDocuments]: "Без документов"
});

export default type;
