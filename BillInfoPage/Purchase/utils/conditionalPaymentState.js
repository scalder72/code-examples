﻿import { getDescriptionCreator } from "../../../Shared/utils";

const type = {
    Created: "Created",
    Accepted: "Accepted",
    Rejected: "Rejected",
    Expired: "Expired",
    Completed: "Completed",
    Replaced: "Replaced"
};

type.getDescription = getDescriptionCreator({
    [type.Created]: "заявлено",
    [type.Accepted]: "принято",
    [type.Rejected]: "отклонено",
    [type.Expired]: "просрочено",
    [type.Completed]: "завершено",
    [type.Replaced]: "заменено"
});

export default type;
