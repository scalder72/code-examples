﻿import { Component, PropTypes } from "react";
import ServiceCenter from "./ServiceCenter.jsx";

class ServiceCenterWrapper extends Component {
    componentDidMount = () => {
        const { serviceCenters, params, fetchServiceCenter, urls } = this.props;
        const { serviceCenterId } = params;
        const { ServiceCenterInfoUrl } = urls;

        if (!serviceCenters[serviceCenterId]) {
            fetchServiceCenter(serviceCenterId, ServiceCenterInfoUrl);
        }
    };

    render() {
        const { serviceCenters, params, BillId } = this.props;
        const { serviceCenterId } = params;
        const serviceCenter = serviceCenters[serviceCenterId];

        return (
            <div>
                {serviceCenter && <ServiceCenter {...serviceCenter} billId={BillId} />}
            </div>
        );
    }
}

ServiceCenterWrapper.propTypes = {
    serviceCenters: PropTypes.object.isRequired,
    fetchServiceCenter: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    urls: PropTypes.object.isRequired,
    BillId: PropTypes.string.isRequired
};

export default ServiceCenterWrapper;
