﻿import { PropTypes } from "react";
import HeaderBackLink from "../../Shared/components/HeaderBackLink.jsx";
import ServiceCenterInfo from "./ServiceCenterInfo.jsx";
import ServiceCenterContact from "./ServiceCenterContact.jsx";

import styles from "./../styles/ServiceCenterInfo.scss";
import headerStyles from "../../Layout/styles/Header.scss";

const ServiceCenter = ({ Requisites, Contacts, billId }) => (
    <div>
        <div className={headerStyles.wrapper}>
            <HeaderBackLink to={`/billinfo/${billId}`} customClass="service-center" withHoverEffect={true}>счет</HeaderBackLink>
            <span className="inlineBlock">Сервисный центр</span>
        </div>
        <div className={styles.content}>
            <ServiceCenterInfo {...Requisites} />
            <div className={styles.contacts}>
                {(Contacts || []).map((contact, index) => <ServiceCenterContact key={index} {...contact} />)}
            </div>
        </div>
    </div>
);

ServiceCenter.propTypes = {
    Requisites: PropTypes.object,
    Contacts: PropTypes.array,
    billId: PropTypes.string.isRequired
};

export default ServiceCenter;
