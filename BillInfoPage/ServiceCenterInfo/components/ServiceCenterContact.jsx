﻿import { PropTypes } from "react";
import styles from "./../styles/ServiceCenterInfo.scss";

const ServiceCenterContact = ({ FullName, Post, Phone, Email }) => (
    <div className={styles["contacts_item"]}>
        <p>{FullName}</p>
        {Post && <p className={styles["contacts_post"]}>{Post}</p>}
        {Phone && <p>{Phone}</p>}
        {Email && <p><a href={`mailto:${Email}`}>{Email}</a></p>}
    </div>
);

ServiceCenterContact.propTypes = {
    FullName: PropTypes.string.isRequired,
    Post: PropTypes.string,
    Phone: PropTypes.string,
    Email: PropTypes.string
};

export default ServiceCenterContact;
