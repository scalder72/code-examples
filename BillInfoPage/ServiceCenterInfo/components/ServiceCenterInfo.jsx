﻿import { PropTypes } from "react";
import styles from "./../styles/ServiceCenterInfo.scss";

const ServiceCenterInfo = (props) => {
    const { ServiceCenterCode, RegionCode, City, Title, Email } = props;

    return (
        <div>
            <div className={styles["general-info"]}>
                <div className={styles["general-info__code"]}>{ServiceCenterCode}</div>
                <div className={styles["general-info__region"]}>{RegionCode}—{City}</div>
            </div>
            <div className={styles.title}>
                <p>{Title}</p>
                <p><a href={`mailto:${Email}`}>{Email}</a></p>
            </div>
        </div>
    );
};

ServiceCenterInfo.propTypes = {
    ServiceCenterCode: PropTypes.string,
    RegionCode: PropTypes.string,
    City: PropTypes.string,
    Title: PropTypes.string,
    Email: PropTypes.string
};

export default ServiceCenterInfo;
