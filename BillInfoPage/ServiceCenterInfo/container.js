﻿import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchServiceCenter } from "./actions";
import ServiceCenterWrapper from "./components/ServiceCenterWrapper.jsx";


const mapStateToProps = ({ billInfo: { serviceCenters, mainBillInfo, urls }}) => ({
    serviceCenters,
    urls,
    ...mainBillInfo
});
const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchServiceCenter }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(ServiceCenterWrapper);
