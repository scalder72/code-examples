﻿import axios from "billing-ui/libs/axios";
import { createAction } from "redux-actions";

import * as types from "./actionTypes";

export const fetchServiceCenterBegin = createAction(types.FETCH_SERVICE_CENTER_BEGIN);
export const fetchServiceCenterComplete = createAction(types.FETCH_SERVICE_CENTER_COMPLETE);
export const fetchServiceCenterSuccess = createAction(types.FETCH_SERVICE_CENTER_SUCCESS);

export const fetchServiceCenter = (serviceCenterId, url) => dispatch => {
    dispatch(fetchServiceCenterBegin());

    axios
        .get(`${url}/${serviceCenterId}`)
        .then(({ data }) => dispatch(fetchServiceCenterSuccess({ serviceCenterId, data })))
        .finally(() => dispatch(fetchServiceCenterComplete()));
};
