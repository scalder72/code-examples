﻿import { handleActions } from "redux-actions";
import * as types from "./actionTypes";

const initialState = {};
const reducer = handleActions({
    [types.FETCH_SERVICE_CENTER_SUCCESS]: (state, { payload }) => ({
        ...state,
        [payload.serviceCenterId]: payload.data
    })
}, initialState);

export default reducer;
