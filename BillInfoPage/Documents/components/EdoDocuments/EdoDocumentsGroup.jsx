﻿import { Component, PropTypes } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { debounce } from "underscore";
import cx from "classnames";

import SlideToggle from "./../Animation/SlideToggle";
import EdoDocumentsDepartmentGroup from "./EdoDocumentsDepartmentGroup.jsx";
import EdoData from "./EdoData.jsx";
import { toggleEdoData } from "../../actions";

import styles from "../../styles/DocumentsGroup.scss";

class EdoDocumentsGroup extends Component {
    render() {
        const { showEdoData, ElectronicDocumentsGroupModels, toggleEdoData, canRemoveLegalDocuments, canConvertToContract, searchedDocumentId } = this.props;

        return (
            <div className={styles.group}>
                <div className={styles.header}>
                    Отправлены через Диадок

                    <span className={cx(styles.link)} onClick={debounce(toggleEdoData, 200)}>
                        Данные получателя
                    </span>
                </div>

                <SlideToggle show={showEdoData}>
                    <EdoData { ...this.props } renderDepartment={ ElectronicDocumentsGroupModels.length === 1 } />
                </SlideToggle>

                {ElectronicDocumentsGroupModels.map(group => (
                    <EdoDocumentsDepartmentGroup key={group.DepartmentName} { ...group }
                                                 renderDepartment={ ElectronicDocumentsGroupModels.length > 1 }
                                                 canRemoveLegalDocuments={canRemoveLegalDocuments}
                                                 canConvertToContract={canConvertToContract}
                                                 searchedDocumentId={searchedDocumentId}
                    />
                ))}
            </div>
        );
    }
}

EdoDocumentsGroup.propTypes = {
    showEdoData: PropTypes.bool.isRequired,
    canRemoveLegalDocuments: PropTypes.object.isRequired,
    canConvertToContract: PropTypes.object.isRequired,
    toggleEdoData: PropTypes.func,
    searchedDocumentId: PropTypes.string,

    SenderName: PropTypes.string.isRequired,
    EnvoyName: PropTypes.string.isRequired,
    PayerName: PropTypes.string.isRequired,
    ElectronicDocumentsGroupModels: PropTypes.arrayOf(
        PropTypes.shape({
            DepartmentName: PropTypes.string.isRequired
        })
    )
};

const mapDispatchToProps = dispatch => bindActionCreators({ toggleEdoData }, dispatch);
export default connect(null, mapDispatchToProps)(EdoDocumentsGroup);
