﻿import { PropTypes } from "react";
import Department from "./Department.jsx";
import styles from "../../styles/EdoData.scss";

const EdoData = ({ SenderName, EnvoyName, PayerName, renderDepartment, ElectronicDocumentsGroupModels }) => (
    <div>
        <div className={styles.wrapper}>
            <div>
                <span>{SenderName}</span>
                <span className={styles.envoy}>{EnvoyName}</span>
            </div>
            <div className={styles.payer}>{PayerName}</div>

            { renderDepartment && (
                <Department departmentName={ElectronicDocumentsGroupModels[0].DepartmentName} />
            ) }
        </div>
    </div>
);

EdoData.propTypes = {
    renderDepartment: PropTypes.bool.isRequired,

    SenderName: PropTypes.string.isRequired,
    EnvoyName: PropTypes.string.isRequired,
    PayerName: PropTypes.string.isRequired,
    ElectronicDocumentsGroupModels: PropTypes.arrayOf(
        PropTypes.shape({
            DepartmentName: PropTypes.string.isRequired
        })
    )
};

export default EdoData;
