﻿import { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { toggleDocumentHistory, uploadDocumentSucceed } from "../../actions";
import { deleteDocumentsPackage, switchContract } from "../../asyncActions";

import Department from "./Department.jsx";
import Document from "./../PaperDocuments/Document.jsx";
import LegalDocumentsComponent from "./../PaperDocuments/PaperLegalDocuments.jsx";

import styles from "../../styles/DocumentsGroup.scss";

const EdoDocumentsDepartmentGroup = (props) => {
    const { renderDepartment, DepartmentName, Documents, LegalDocuments, documentHistoryDisplayMap,
            deleteDocumentsPackage, toggleDocumentHistory, uploadDocumentSucceed, switchContract, canRemoveLegalDocuments, canConvertToContract,
            searchedDocumentId } = props;

    return (
        <div className={styles["department-group-wrapper"]}>
            {renderDepartment && (
                <div className={styles["department-wrapper"]}>
                    <Department departmentName={DepartmentName} />
                </div>
            )}

            {Documents.map(document => (
                <Document key={document.DocumentId} { ...document }
                          documentHistoryDisplay={documentHistoryDisplayMap[document.DocumentId]}
                          toggleDocumentHistory={toggleDocumentHistory}
                          uploadDocumentSucceed={uploadDocumentSucceed}
                          isHighlighted={searchedDocumentId === document.DocumentId}
                />
            ))}

            {LegalDocuments.length > 0 && (
                <LegalDocumentsComponent LegalDocuments={LegalDocuments}
                                         documentHistoryDisplayMap={documentHistoryDisplayMap}
                                         uploadDocumentSucceed={uploadDocumentSucceed}
                                         toggleDocumentHistory={toggleDocumentHistory}
                                         deleteDocumentsPackage={deleteDocumentsPackage}
                                         switchContract={switchContract}
                                         canRemoveLegalDocuments={canRemoveLegalDocuments}
                                         canConvertToContract={canConvertToContract}
                                         searchedDocumentId={searchedDocumentId}
                />
            )}
        </div>
    );
};

EdoDocumentsDepartmentGroup.propTypes = {
    renderDepartment: PropTypes.bool.isRequired,
    canRemoveLegalDocuments: PropTypes.object.isRequired,
    canConvertToContract: PropTypes.bool.isRequired,
    searchedDocumentId: PropTypes.string.isRequired,

    documentHistoryDisplayMap: PropTypes.object.isRequired,
    deleteDocumentsPackage: PropTypes.func.isRequired,
    uploadDocumentSucceed: PropTypes.func.isRequired,
    toggleDocumentHistory: PropTypes.func.isRequired,
    switchContract: PropTypes.func.isRequired,

    DepartmentName: PropTypes.string.isRequired,
    LegalDocuments: PropTypes.array.isRequired,
    Documents: PropTypes.arrayOf(
        PropTypes.shape({
            DocumentId: PropTypes.string.isRequired
        })
    )
};

const mapStateToProps = ({ billInfo: { documentsInfo: { documentHistoryDisplayMap } }}) => ({ documentHistoryDisplayMap });
const mapDispatchToProps = (dispatch) => bindActionCreators({deleteDocumentsPackage, switchContract, toggleDocumentHistory, uploadDocumentSucceed}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(EdoDocumentsDepartmentGroup);
