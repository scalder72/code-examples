﻿import { PropTypes } from "react";
import Icon from "react-ui/Icon";
import styles from "../../styles/Department.scss";

const Department = ({ departmentName }) => (
    <div className={styles.department}>
        <span className={styles.icon}><Icon name="enter" /></span>
        { departmentName }
    </div>
);

Department.propTypes = {
    departmentName: PropTypes.string.isRequired
};

export default Department;
