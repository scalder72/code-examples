﻿import { Component, PropTypes } from "react";
import pureRender from "react-pure-render/function"
import cx from "classnames";

import Loader from "react-ui/Loader";

import PaperDocumentsGroup from "./PaperDocuments/PaperDocumentsGroup.jsx";
import EdoDocumentsGroup from "./EdoDocuments/EdoDocumentsGroup.jsx";
import NoDocuments from "./NoDocuments.jsx";

import styles from "../styles/DocumentsGroup.scss";

class DocumentsWrapper extends Component {
    shouldComponentUpdate: pureRender;

    _resolveFetch() {
        const { fetchRequired, fetchDocuments } = this.props;

        if (fetchRequired) {
            fetchDocuments();
        }
    }

    componentDidMount() {
        this._resolveFetch();
    }

    componentDidUpdate() {
        this._resolveFetch();
    }

    render() {
        const { isLoading, wasLoaded, showEdoData, ElectronicDocflowInfoModel, CanConvertKeDaoBudgetToKeContract,
                  PaperDocumentsInfoModel, CanRemoveLegalDocuments, searchedDocumentId } = this.props;

        return (
            <div className={cx(styles.wrapper, { [styles["is-loading"]]: isLoading })}>
                <Loader active={isLoading} type="big">
                    {ElectronicDocflowInfoModel && (
                        <EdoDocumentsGroup { ...ElectronicDocflowInfoModel } showEdoData={showEdoData}
                                                                             canRemoveLegalDocuments={CanRemoveLegalDocuments}
                                                                             canConvertToContract={CanConvertKeDaoBudgetToKeContract}
                                                                             searchedDocumentId={searchedDocumentId}
                        />
                    )}

                    {PaperDocumentsInfoModel && (
                        <PaperDocumentsGroup { ...PaperDocumentsInfoModel } canRemoveLegalDocuments={CanRemoveLegalDocuments}
                                                                            canConvertToContract={CanConvertKeDaoBudgetToKeContract}
                                                                            searchedDocumentId={searchedDocumentId}
                        />
                    )}

                    { wasLoaded && !ElectronicDocflowInfoModel && !PaperDocumentsInfoModel && <NoDocuments className={styles["no-documents"]} /> }
                </Loader>
            </div>
        );
    }
}

DocumentsWrapper.propTypes = {
    fetchRequired: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    wasLoaded: PropTypes.bool.isRequired,
    fetchDocuments: PropTypes.func.isRequired,
    showEdoData: PropTypes.bool.isRequired,
    searchedDocumentId: PropTypes.string,

    CanRemoveLegalDocuments: PropTypes.object,
    CanConvertKeDaoBudgetToKeContract: PropTypes.object,
    ElectronicDocflowInfoModel: PropTypes.object,
    PaperDocumentsInfoModel: PropTypes.object
};

export default DocumentsWrapper;
