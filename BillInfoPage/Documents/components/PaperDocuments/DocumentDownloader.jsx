﻿import { PropTypes } from "react";

import { getUrlWithQuery } from "../../../../Shared/Helpers/UrlHelper";
import Icon from "react-ui/Icon";

import styles from "../../styles/DocumentDownloader.scss";

const DocumentDownloader = ({ downloadUrl, documentId }) => (
    <a href={getUrlWithQuery(downloadUrl, {documentId: documentId})} className={styles.link}>
        <span className={styles.button}>
            <Icon name="clip" />
            <span className={styles.text}>Вложение</span>
        </span>
    </a>
);

DocumentDownloader.propTypes = {
    downloadUrl: PropTypes.string.isRequired,
    documentId: PropTypes.string.isRequired
};

export default DocumentDownloader;
