﻿import { Component, PropTypes } from "react";

import { toLowerFirstLetter } from "billing-ui/helpers/StringHelpers";
import Tooltip from "billing-ui/components/Tooltip";
import Actions, { Action } from "billing-ui/components/Actions";
import Icon, { IconTypes } from "billing-ui/components/Icon";

import appConfig from "../../../../appConfig";
import styles from "../../styles/DocumentActions.scss";

const defaultRemoveRejectReason = "Удалить пакет нельзя";
const hasLinkedLegalDocumentsText = "вместе со связанными<br/>документами в других пакетах";

const renderReason = (defaultReason, customReason) => {
    if (customReason) {
        return `${defaultReason} — ${toLowerFirstLetter(customReason)}`;
    }

    return defaultReason;
};

class DocumentActions extends Component {
    _tooltipTarget = null;
    _actionsTarget = null;

    _shouldRenderPopup() {
        const { canConvertToContract: { IsSuccess, RejectReason }, canDownloadDocumentsPackage } = this.props;

        return IsSuccess || RejectReason || canDownloadDocumentsPackage;
    }

    _renderInlineActions = () => {
        const { canRemoveLegalDocuments: { CanRemove, RemoveRejectReason, HasLinkedLegalDocumentsInOtherBills }, deleteDocumentsPackage } = this.props;

        if (CanRemove) {
            return (
                <div>
                    <div onClick={deleteDocumentsPackage} ref={ node => { this._tooltipTarget = node }} className={styles.link}>
                        <Icon name="trash" />
                        Удалить
                    </div>

                    {HasLinkedLegalDocumentsInOtherBills && (
                        <Tooltip getTarget={() => this._tooltipTarget}>
                            {hasLinkedLegalDocumentsText}
                        </Tooltip>
                    )}
                </div>
            );
        }

        return (
            <div>
                <div ref={ node => { this._tooltipTarget = node }} className={styles.prohibited}>
                    <Icon name="trash" />
                </div>
                <Tooltip getTarget={() => this._tooltipTarget}>
                    {renderReason(defaultRemoveRejectReason, RemoveRejectReason)}
                </Tooltip>
            </div>
        );
    };

    _renderPopupActions = () => {
        const { canRemoveLegalDocuments: { CanRemove, RemoveRejectReason } = {}, deleteDocumentsPackage, switchContract,
                canDownloadDocumentsPackage, canConvertToContract: { IsSuccess, RejectReason } } = this.props;
        const { billInfoUrls: { DownloadDocumentsPackageUrl }} = appConfig.getUrls();

        return (
            <div>
                <div className={styles.ellipsis} ref={node => { this._actionsTarget = node }}>…</div>
                <Actions getBindItem={() => this._actionsTarget} position={{top: 2, right: -6}} className={styles.popup}>
                    {canDownloadDocumentsPackage && <Action asLink={true}
                                                            href={DownloadDocumentsPackageUrl}
                                                            iconType={IconTypes.Download}
                                                            description="Скачать пакет" />
                    }

                    {IsSuccess && <Action onClick={switchContract}
                                                     iconType={IconTypes.DocumentConvert}
                                                     description="Перейти с договора на контракт" />}

                    {!IsSuccess && RejectReason && (
                        <div className={styles["action-disabled"]}>
                            <span className={styles.prohibited}><Icon type={IconTypes.DocumentConvert} /></span>
                            <span className={styles["action-text"]}>
                                {`Перейти на контракт нельзя — ${RejectReason}`}
                            </span>
                        </div>
                    )}

                    {CanRemove && <Action onClick={deleteDocumentsPackage}
                                          iconType={IconTypes.Trash}
                                          description="Удалить" />}

                    {!CanRemove && (
                        <div className={styles["action-disabled"]}>
                            <span className={styles.prohibited}><Icon type={IconTypes.Trash} /></span>
                            <span className={styles["action-text"]}>
                                {renderReason(defaultRemoveRejectReason, RemoveRejectReason)}
                            </span>
                        </div>
                    )}
                </Actions>
            </div>
        );
    };

    render() {
        const shouldRenderPopup = this._shouldRenderPopup();

        return (
            <div className={styles["header-action"]}>
                {!shouldRenderPopup && this._renderInlineActions()}
                {shouldRenderPopup && this._renderPopupActions()}
            </div>
        );
    }
}

DocumentActions.propTypes = {
    deleteDocumentsPackage: PropTypes.func.isRequired,
    switchContract: PropTypes.func.isRequired,

    canDownloadDocumentsPackage: PropTypes.bool.isRequired,
    canRemoveLegalDocuments: PropTypes.shape({
        CanRemove: PropTypes.bool.isRequired,
        RemoveRejectReason: PropTypes.string.isRequired,
        HasLinkedLegalDocumentsInOtherBills: PropTypes.bool.isRequired
    }),
    canConvertToContract: PropTypes.shape({
        IsSuccess: PropTypes.bool,
        RejectReason: PropTypes.string
    }).isRequired
};

export default DocumentActions;
