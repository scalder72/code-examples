﻿import {Component, PropTypes} from "react";
import cx from "classnames";

import FileUploadForm from "./../FileUpload/FileUploadForm.jsx";
import styles from "../../styles/DocumentUploader.scss";

const uploadParams = {
    acceptFileTypes: /(.|\/)(pdf|jpe?g|tif?f|bmp|rtf|doc|docx|xls|xlsx)$/i,
    maxFileSize: 5 * 1024 * 1024,
    messages: {
        acceptFileTypes: "Недопустимый формат файла. Используйте сканы, форматы Word, Excel или pdf",
        maxFileSize: "Размер файла не должен превышать 5 Мб"
    }
};

const handleUploadDocumentSucceed = (handler) => {
    // eslint-disable-next-line no-undef
    Informer.show({ message: "файл успешно прикреплен", status: InformerType.success, iconType: IconType.success });
    handler();
};

const handleUploadDocumentFailed = (evt, { files = [] } = {}) => {
    const errorMessage = files
        .filter(({ error = "" }) => !!error)
        .map(({ error }) => error)
        .join("; ");

    // eslint-disable-next-line no-undef
    Informer.show({ message: errorMessage, status: InformerType.error, iconType: IconType.error });
};

class DocumentUploader extends Component {
    state = {
        fileIsLoading: false
    };

    render() {
        const { uploadDocumentSucceed, documentId, url, className, children, onClick } = this.props;

        return (
            <FileUploadForm className={cx(styles.link, className, {[styles.inactive]: this.state.fileIsLoading})}
                            url={url}
                            onFileUploadDone={() => handleUploadDocumentSucceed(uploadDocumentSucceed)}
                            onFileUploadProgressAll={() => handleUploadDocumentSucceed(uploadDocumentSucceed)}
                            onFileUploadFail={handleUploadDocumentFailed}
                            onFileUploadProcessFail={handleUploadDocumentFailed}

                            onFileUploadAdd={() => {}}

                            onFileUploadSubmit={() => this.setState({ fileIsLoading: true })}
                            onFileUploadAlways={() => this.setState({ fileIsLoading: false })}

                            onClick={onClick}

                            fileUploadParams={uploadParams}>
                {children}
                <input type="hidden" name="documentId" value={documentId} />
            </FileUploadForm>
        );
    }
}

DocumentUploader.propTypes = {
    url: PropTypes.string.isRequired,
    documentId: PropTypes.string.isRequired,
    uploadDocumentSucceed: PropTypes.func.isRequired,
    className: PropTypes.string,
    children: PropTypes.node,
    onClick: PropTypes.func
};

export default DocumentUploader;
