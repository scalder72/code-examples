﻿import { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Document from "./Document.jsx";
import PaperLegalDocuments from "./PaperLegalDocuments.jsx";

import { uploadDocumentSucceed, toggleDocumentHistory } from "../../actions";
import { deleteDocumentsPackage, switchContract } from "../../asyncActions";
import styles from "../../styles/DocumentsGroup.scss";

class PaperDocumentsGroup extends Component {
    render() {
        const { Documents, LegalDocuments, canRemoveLegalDocuments, canConvertToContract, documentHistoryDisplayMap,
                uploadDocumentSucceed, toggleDocumentHistory, deleteDocumentsPackage, searchedDocumentId, switchContract } = this.props;

        return (
            <div className={styles.group}>
                <div className={styles.header}>Созданы</div>

                {Documents.map(document => (
                    <Document key={document.DocumentId} { ...document }
                              toggleDocumentHistory={toggleDocumentHistory}
                              uploadDocumentSucceed={uploadDocumentSucceed}
                              documentHistoryDisplay={documentHistoryDisplayMap[document.DocumentId]}
                              isHighlighted={searchedDocumentId === document.DocumentId}
                    />
                ))}

                {LegalDocuments.length > 0 && (
                    <PaperLegalDocuments LegalDocuments={LegalDocuments}
                                         canRemoveLegalDocuments={canRemoveLegalDocuments}
                                         canConvertToContract={canConvertToContract}
                                         documentHistoryDisplayMap={documentHistoryDisplayMap}
                                         deleteDocumentsPackage={deleteDocumentsPackage}
                                         switchContract={switchContract}
                                         toggleDocumentHistory={toggleDocumentHistory}
                                         uploadDocumentSucceed={uploadDocumentSucceed}
                                         searchedDocumentId={searchedDocumentId}
                    />
                )}
            </div>
        );
    }
}

PaperDocumentsGroup.propTypes = {
    LegalDocuments: PropTypes.array.isRequired,
    Documents: PropTypes.arrayOf(
        PropTypes.shape({
            DocumentId: PropTypes.string.isRequired
        })
    ).isRequired,


    uploadDocumentSucceed: PropTypes.func,
    toggleDocumentHistory: PropTypes.func,
    deleteDocumentsPackage: PropTypes.func,
    switchContract: PropTypes.func,
    searchedDocumentId: PropTypes.string,

    documentHistoryDisplayMap: PropTypes.object.isRequired,
    canRemoveLegalDocuments: PropTypes.object.isRequired,
    canConvertToContract: PropTypes.object.isRequired
};

const mapStateToProps = ({ billInfo: { documentsInfo: { documentHistoryDisplayMap } }}) => ({ documentHistoryDisplayMap });
const mapDispatchToProps = dispatch => bindActionCreators({ uploadDocumentSucceed, toggleDocumentHistory, deleteDocumentsPackage, switchContract }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(PaperDocumentsGroup);
