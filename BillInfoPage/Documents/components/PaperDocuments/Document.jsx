﻿import { Component, PropTypes } from "react";
import moment from "billing-ui/libs/moment";
import cx from "classnames";

import Gapped from "react-ui/Gapped";

import HistoryItem from "./../History/HistoryItem.jsx";
import HistoryItemsList from "./../History/HistoryItemsList.jsx";
import DocumentUploader from "./DocumentUploader.jsx";
import DocumentDownloader from "./DocumentDownloader.jsx";
import SlideToggle from "./../Animation/SlideToggle";

import Actions, { Action } from "billing-ui/components/Actions";
import { IconTypes } from "billing-ui/components/Icon";

import styles from "../../styles/Document.scss";

import { downloadDocumentFile } from "./../../utils/documentsHelper";
import appConfig from "../../../../appConfig";
import docflowState from "./../../utils/DocflowState";

const showHistoryText = "Показать историю";
const attachFileText = "Прикрепить файл";
let wasMouseEntered = false;

class Document extends Component {
    constructor(props) {
        super(props);

        this.state = { actionsActive: false };
        this._handleActionsPopupOpen = this._handleActionsPopupOpen.bind(this);
        this._handleActionsPopupClose = this._handleActionsPopupClose.bind(this);
        this._handleMouseEnterEvent = this._handleMouseEnterEvent.bind(this);
    }


    componentDidUpdate() {
        if (this.state.actionsActive && !this._canShowActionsPopup()) {
            this._handleActionsPopupClose();
        }
    }

    _canShowActionsPopup() {
        const { DocflowHistory, HasAttachedFile, CanAttachFile, documentHistoryDisplay } = this.props;

        const hasAttachFileAction = !HasAttachedFile && CanAttachFile;
        const showHistoryButton = !documentHistoryDisplay && DocflowHistory.length > 1;
        return showHistoryButton && hasAttachFileAction;
    }

    _handleActionsPopupOpen() {
        this.setState({ actionsActive: true });
    }

    _handleActionsPopupClose() {
        this.setState({ actionsActive: false });
    }

    _handleMouseEnterEvent() {
        const { isHighlighted } = this.props;

        if (!wasMouseEntered && isHighlighted) {
            wasMouseEntered = true;
            this.setState({});
        }
    }

    render() {
        const { DocumentId, Title, Date, DocflowHistory, DocflowStatus, HasAttachedFile, CanAttachFile,
                  documentHistoryDisplay, toggleDocumentHistory, uploadDocumentSucceed, IsPrinted, isHighlighted } = this.props;

        const { Author, StatusDate, ResultStatus, DocflowStateDescription, Commentary } = DocflowStatus;

        const hasHistory = DocflowHistory.length > 0;
        const showHistoryButton = !documentHistoryDisplay && hasHistory && DocflowHistory.length > 1;
        const showFullHistory = !!documentHistoryDisplay && hasHistory;

        const hasAttachFileAction = !HasAttachedFile && CanAttachFile;
        const showActionsPopup = this._canShowActionsPopup();
        const { billInfoUrls: { DocumentUploadUrl, DocumentDownloadUrl } } = appConfig.getUrls();

        var rootClassNames = cx(styles["document"], {
            [styles["actions-active"]]: this.state.actionsActive,
            [styles["disable-download"]]: !IsPrinted,
            [styles["highlighted"]]: !wasMouseEntered && isHighlighted
        });

        return (
            <div className={rootClassNames} onMouseEnter={this._handleMouseEnterEvent}>
                <div className={cx(styles["inner-document"], { [styles["compact"]]: showActionsPopup })}
                     onClick={() => IsPrinted && downloadDocumentFile(DocumentId)}>
                    <div className={styles.header}>
                        {showHistoryButton && !showActionsPopup && (
                            <span className={styles["header-action"]}>
                                <span className={styles.link} onClick={(evt) => { evt.stopPropagation(); toggleDocumentHistory(DocumentId); }}>
                                    {showHistoryText}
                                </span>
                            </span>
                        )}

                        {hasAttachFileAction && !showActionsPopup && (
                            <span className={styles["header-action"]}>
                                <DocumentUploader url={DocumentUploadUrl}
                                                  documentId={DocumentId}
                                                  uploadDocumentSucceed={uploadDocumentSucceed}>
                                    {attachFileText}
                                </DocumentUploader>
                            </span>
                        )}

                        <div className={styles["info-col"]}>{moment(Date).format("L")}</div>
                        <div className={styles["content-col"]}>
                            <div className={styles["title"]}>{Title}</div>

                            {HasAttachedFile && (
                                <Gapped gap={10}>
                                    <DocumentDownloader documentId={DocumentId} downloadUrl={DocumentDownloadUrl} />
                                    {CanAttachFile && (
                                        <DocumentUploader url={DocumentUploadUrl}
                                                          documentId={DocumentId}
                                                          uploadDocumentSucceed={uploadDocumentSucceed}
                                                          className={styles["replace-attachment"]}>
                                            Заменить вложение
                                        </DocumentUploader>
                                    )}
                                </Gapped>
                            )}
                        </div>
                    </div>

                    <SlideToggle show={!showFullHistory}>
                        <div className={styles.container}>
                            <HistoryItem date={StatusDate}
                                         status={DocflowStateDescription}
                                         resultStatus={ResultStatus}
                                         commentary={Commentary}
                                         disabled={!IsPrinted}
                            />
                        </div>
                    </SlideToggle>

                    <SlideToggle show={showFullHistory}>
                        <HistoryItemsList items={DocflowHistory} />
                    </SlideToggle>

                    {Author && (
                        <div className={cx(styles.item, styles["is-sender"])}>
                            <div className={styles["info-col"]}>Отправил:</div>
                            <div className={styles["content-col"]}>
                                <span>{Author}</span>
                            </div>
                        </div>
                    )}
                </div>

                {showActionsPopup && (
                    <div>
                        <div className={styles["actions"]} ref={node => { this._actionsTarget = node }}>
                            <div className={styles["actions-ellipsis"]}>…</div>
                        </div>

                        <Actions getBindItem={() => this._actionsTarget}
                                 position={{top: 0, right: 0}}
                                 ellipsisClassName={styles["action-popup-ellipsis"]}
                                 isActive={this.state.actionsActive}
                                 onOpen={this._handleActionsPopupOpen}
                                 onClose={this._handleActionsPopupClose}
                        >
                            <Action iconType={IconTypes.ArchivePack}
                                    onClick={() => { toggleDocumentHistory(DocumentId); }}
                                    description={showHistoryText} />

                            <DocumentUploader url={DocumentUploadUrl}
                                              documentId={DocumentId}
                                              uploadDocumentSucceed={uploadDocumentSucceed}
                                              className={styles["actions-attach-wrapper"]}>
                                <Action iconType={IconTypes.Attach} description={attachFileText} />
                            </DocumentUploader>
                        </Actions>
                    </div>
                )}
            </div>
        );
    }
}

Document.propTypes = {
    documentHistoryDisplay: PropTypes.bool,
    toggleDocumentHistory: PropTypes.func.isRequired,
    uploadDocumentSucceed: PropTypes.func.isRequired,
    isHighlighted: PropTypes.bool.isRequired,

    Date: PropTypes.string.isRequired,
    DocumentId: PropTypes.string.isRequired,
    Title: PropTypes.string.isRequired,
    CanAttachFile: PropTypes.bool.isRequired,
    HasAttachedFile: PropTypes.bool.isRequired,
    DocflowHistory: PropTypes.array.isRequired,
    IsPrinted: PropTypes.bool.isRequired,

    DocflowStatus: PropTypes.shape({
        StatusDate: PropTypes.string.isRequired,
        DocflowStateDescription: PropTypes.string.isRequired,

        Author: PropTypes.string,
        Commentary: PropTypes.string,
        ResultStatus: PropTypes.oneOf(Object.keys(docflowState).map(key => docflowState[key]))
    })
};

export default Document;
