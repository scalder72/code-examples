﻿import { PropTypes } from "react";

import PaperDocument from "./Document.jsx";
import DocumentActions from "./DocumentsGroupActions.jsx";
import styles from "../../styles/Document.scss";

const PaperLegalDocuments = (props) => {
    const { LegalDocuments, canRemoveLegalDocuments, canConvertToContract, uploadDocumentSucceed, toggleDocumentHistory, switchContract,
              documentHistoryDisplayMap, deleteDocumentsPackage, searchedDocumentId } = props;

    return (
        <div>
            <div className={styles["paper-header"]}>
                <div className={styles["paper-header-inner"]}>
                    Пакет юридических документов
                    <DocumentActions canRemoveLegalDocuments={canRemoveLegalDocuments}
                                     canConvertToContract={canConvertToContract}
                                     switchContract={switchContract}
                                     canDownloadDocumentsPackage={LegalDocuments.some(document => document.IsPrinted)}
                                     deleteDocumentsPackage={deleteDocumentsPackage} />
                </div>
            </div>

            {LegalDocuments.map(document => (
                <PaperDocument key={document.DocumentId} { ...document }
                               documentHistoryDisplay={documentHistoryDisplayMap[document.DocumentId]}
                               toggleDocumentHistory={toggleDocumentHistory}
                               uploadDocumentSucceed={uploadDocumentSucceed}
                               isHighlighted={searchedDocumentId === document.DocumentId}
                />
            ))}
        </div>
    );
};

PaperLegalDocuments.propTypes = {
    documentHistoryDisplayMap: PropTypes.object.isRequired,
    searchedDocumentId: PropTypes.string,

    toggleDocumentHistory: PropTypes.func.isRequired,
    uploadDocumentSucceed: PropTypes.func.isRequired,
    deleteDocumentsPackage: PropTypes.func.isRequired,
    switchContract: PropTypes.func.isRequired,

    canRemoveLegalDocuments: PropTypes.object.isRequired,
    canConvertToContract: PropTypes.object.isRequired,
    LegalDocuments: PropTypes.arrayOf(
        PropTypes.shape({
            DocumentId: PropTypes.string.isRequired,
            IsPrinted: PropTypes.bool.isRequired
        })
    )
};

export default PaperLegalDocuments;
