﻿import { PropTypes } from "react";

import cx from "classnames";
import styles from "../../Shared/styles/ErrorMessage.scss";

const NoDocuments = ({ className }) => (
    <div className={cx(styles.wrapper, className)}>
        <div className={styles.arrow}></div>
        <div className={styles.header}>Документы по счету не созданы</div>
        <div className={styles.message}>
            Чтобы создать документы, напечатайте их,<br/>
            отправьте по почте или через Диадок
        </div>
        <div className={styles.documents}></div>
    </div>
);

NoDocuments.propTypes = {
    className: PropTypes.string
};

export default NoDocuments;
