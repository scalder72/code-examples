﻿import { PropTypes } from "react";
import TransitionGroup from "react-addons-transition-group";
import Transition from "./SlideToggleTransition";

const SlideToggleWrapper = ({ children, show, duration }) => (
    <TransitionGroup>
        { show && <Transition duration={duration}>{children}</Transition> }
    </TransitionGroup>
);

SlideToggleWrapper.propTypes = {
    show: PropTypes.bool.isRequired,
    duration: PropTypes.number,
    children: PropTypes.node.isRequired
};

export default SlideToggleWrapper;
