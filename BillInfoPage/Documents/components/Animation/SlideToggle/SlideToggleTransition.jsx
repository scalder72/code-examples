﻿import { Component, PropTypes } from "react";
import { findDOMNode } from "react-dom";
import TweenLite from "gsap";

class SlideToggleTransition extends Component {
    componentWillEnter(callback) {
        const node = findDOMNode(this);
        const { duration } = this.props;

        TweenLite.fromTo(node, duration, {height: 0, opacity: 0}, {height: node.offsetHeight, opacity: 1, onComplete: callback});
    }

    componentWillLeave(callback) {
        const node = findDOMNode(this);
        const { duration } = this.props;

        TweenLite.fromTo(node, duration, {height: node.offsetHeight, opacity: 1}, {height: 0, opacity: 0, onComplete: callback});
    }

    render() {
        return (
            <div>{this.props.children}</div>
        );
    }
}

SlideToggleTransition.propTypes = {
    children: PropTypes.node.isRequired,
    duration: PropTypes.number.isRequired
};

SlideToggleTransition.defaultProps = {
    duration: 0.2
};

export default SlideToggleTransition;
