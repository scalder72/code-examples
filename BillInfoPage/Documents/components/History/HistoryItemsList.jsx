﻿import { PropTypes } from "react";
import HistoryItem from "./HistoryItem.jsx";
import docflowState from "./../../utils/DocflowState";

import styles from "../../styles/HistoryItem.scss";

const HistoryItemsList = ({ items }) => (
    <div className={styles["hidden-wrapper"]}>
        {items.map((history, index) => (
            <HistoryItem key={`${history.EventDate}_${index}`}
                         status={history.Status}
                         resultStatus={history.ResultStatus}
                         commentary={history.Comment}
                         date={history.EventDate} />
        ))}
    </div>
);

HistoryItemsList.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            EventDate: PropTypes.string.isRequired,
            Status: PropTypes.string.isRequired,
            ResultStatus: PropTypes.oneOf(Object.keys(docflowState).map(key => docflowState[key])),
            Comment: PropTypes.string
        })
    )
};

export default HistoryItemsList;
