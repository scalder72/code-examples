﻿import { PropTypes } from "react";
import cx from "classnames";
import moment from "billing-ui/libs/moment";

import Icon from "react-ui/Icon";
import docflowState from "./../../utils/DocflowState";
import styles from "./../../styles/HistoryItem.scss";

const HistoryItem = ({ date, status, resultStatus, commentary, disabled }) => {
    const momentDate = moment(date);
    const isRejected = resultStatus === docflowState.rejected;
    const isAccepted = resultStatus === docflowState.accepted;

    return (
        <div className={cx(styles.item, {[styles.rejected]: isRejected, [styles.accepted]: isAccepted})}>
            <div className={styles["info-col"]}>
                {isRejected && (
                    <span className={styles.icon}>
                        <Icon name="error" />
                    </span>
                )}

                {isAccepted && (
                    <span className={styles.icon}>
                        <Icon name="ok" />
                    </span>
                )}

                {momentDate.format("L")}
            </div>

            <div className={styles["content-col"]}>
                <span>
                    {momentDate.format("LT")}
                </span>
                <span className={cx(styles.description, { [styles["disabled"]]: disabled })}>
                    {status}
                </span>
                {commentary && <div className={styles.commentary}>{commentary}</div>}
            </div>
        </div>
    );
};

HistoryItem.propTypes = {
    disabled: PropTypes.bool.isRequired,
    resultStatus: PropTypes.oneOf(Object.keys(docflowState).map(key => docflowState[key])),
    status: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    commentary: PropTypes.string
};

HistoryItem.defaultProps = {
    disabled: false
};

export default HistoryItem;
