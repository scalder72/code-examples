﻿import { Component } from "react";
import css from "./FileUpload.scss"

class FileUploadInput extends Component {
    static uid = 0;
    _uid = FileUploadInput.uid++;

    render() {
        return (
            <input
                type="file"
                name={`fileUploadInput_${this._uid}`}
                className={css.fileInput} />
        );
    }
}

export default FileUploadInput;
