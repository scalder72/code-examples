﻿import { createAction } from "redux-actions";
import * as types from "./actionTypes";

export const fetchDocumentsBegin = createAction(types.FETCH_DOCUMENTS_BEGIN);
export const fetchDocumentsComplete = createAction(types.FETCH_DOCUMENTS_COMPLETE);
export const fetchDocumentsSuccess = createAction(types.FETCH_DOCUMENTS_SUCCESS);
export const fetchDocumentsError = createAction(types.FETCH_DOCUMENTS_ERROR);

export const deleteDocumentsPackageBegin = createAction(types.DELETE_DOCUMENTS_PACKAGE_BEGIN);
export const deleteDocumentsPackageSuccess = createAction(types.DELETE_DOCUMENTS_PACKAGE_SUCCESS);
export const deleteDocumentsPackageComplete = createAction(types.DELETE_DOCUMENTS_PACKAGE_COMPLETE);
export const deleteDocumentsPackageError = createAction(types.DELETE_DOCUMENTS_PACKAGE_ERROR);

export const toggleEdoData = createAction(types.TOGGLE_EDO_DATA);
export const toggleDocumentHistory = createAction(types.TOGGLE_DOCUMENT_HISTORY);
export const uploadDocumentSucceed = createAction(types.UPLOAD_DOCUMENT_SUCCEED, null, () => ({
    ga: {
        category: "bill-docs",
        action: "attach",
        label: "legal-attachment"
    }
}));

export const switchContractBegin = createAction(types.SWITCH_CONTRACT_BEGIN);
export const switchContractSuccess = createAction(types.SWITCH_CONTRACT_SUCCESS);
export const switchContractError = createAction(types.SWITCH_CONTRACT_ERROR);
