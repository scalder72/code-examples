﻿import axios from "billing-ui/libs/axios";
import Informer from "Informer";
import App from "../../globalApp";

import * as actions from "./actions";

export const fetchDocuments = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { DocumentsUrl } } = billInfo;

    dispatch(actions.fetchDocumentsBegin());

    axios
        .get(DocumentsUrl)
        .then(response => dispatch(actions.fetchDocumentsSuccess(response.data)))
        .catch((e) => dispatch(actions.fetchDocumentsError(e)))
        .finally(() => dispatch(actions.fetchDocumentsComplete()));
};

export const deleteDocumentsPackage = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { DocumentsPackageDeleteUrl } } = billInfo;

    dispatch(actions.deleteDocumentsPackageBegin());

    axios
        .delete(DocumentsPackageDeleteUrl)
        .then(({ data }) => dispatch(actions.deleteDocumentsPackageSuccess(data)))
        .catch((e) => dispatch(actions.deleteDocumentsPackageError(e)))
        .finally(() => dispatch(actions.deleteDocumentsPackageComplete()));
};

export const switchContract = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { ConvertContractUrl, PrintBillUrl } } = billInfo;

    dispatch(actions.switchContractBegin());

    axios
        .post(ConvertContractUrl)
        .then(({ data: { IsSuccess, RejectReason } }) => {
            if (IsSuccess) {
                Informer.show({ message: "Переход прошел успешно", status: InformerType.success, iconType: IconType.tick});
                App.history.push(PrintBillUrl);
                return dispatch(actions.switchContractSuccess());
            } else {
                throw new Error(RejectReason);
            }
        })
        .catch((Error) => {
            Informer.show({
                message: Error && Error.message ? Error.message : "Не удалось сменить договор",
                status: InformerType.error,
                iconType: IconType.error
            });
            return dispatch(actions.switchContractError());
        });
};
