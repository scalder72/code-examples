﻿import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchDocuments } from "./asyncActions";
import DocumentsWrapper from "./components/DocumentsWrapper.jsx";

const mapStateToProps = ({ billInfo: { documentsInfo }} = {}) => ({ ...documentsInfo });
const mapDispatchToProps = (dispatch) => bindActionCreators({ fetchDocuments }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DocumentsWrapper);
