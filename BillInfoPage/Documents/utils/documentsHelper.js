﻿import appConfig from "../../../appConfig";

export const downloadFile = (url = null) => {
    if (url) {
        document.location.href = url;
    }
};

export const generateDownloadDocumentUrl = (baseUrl, documentId) => {
    const time = new Date().getTime();
    return `${baseUrl}?documentId=${documentId}&t=${time}`;
};

export const downloadDocumentFile = (documentId) => {
    const { billInfoUrls: { PrinterDocumentDownloadUrl } } = appConfig.getUrls();
    const url = generateDownloadDocumentUrl(PrinterDocumentDownloadUrl, documentId);
    downloadFile(url);
};
