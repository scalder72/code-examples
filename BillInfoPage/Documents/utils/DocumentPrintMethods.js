﻿const documentPrintMethods = {
    None: "None",
    ExternalFile: "ExternalFile",
    PrinterFile: "PrinterFile"
};

export const contains = (arr, element) => arr.indexOf(element) !== -1;
export default documentPrintMethods;
