﻿const docflowState = {
    none: "None",
    accepted: "Accepted",
    rejected: "Rejected"
};

export default docflowState;
