﻿import { handleActions } from "redux-actions";
import { LOCATION_CHANGE } from "billing-ui/libs/react-router-redux";
import * as types from "./actionTypes";

const isDocumentsRoute = /^\/billinfo\/[0-9a-z-]{36}\/documents$/i;
const isDocumentsWithDocumentIdRoute = /^\/billinfo\/[0-9a-z-]{36}\/documents\/[0-9a-z-]{36}$/i;

const initialState = {
    fetchRequired: true,
    isLoading: false,
    wasLoaded: false,
    showEdoData: false,
    searchedDocumentId: null,
    documentHistoryDisplayMap: {}
};

const showDocumentHistoryMapReducer = handleActions({
    [types.TOGGLE_DOCUMENT_HISTORY]: (state, { payload: documentId }) => {
        if (!documentId) {
            return state;
        }

        const containsDocumentId = state[documentId] !== undefined;

        return ({
            ...state,
            [documentId]: containsDocumentId ? !state[documentId] : true
        });
    }
}, {});

export default handleActions({
    [types.FETCH_DOCUMENTS_BEGIN]: (state) => ({
        ...state,
        fetchRequired: false,
        isLoading: true
    }),
    [types.FETCH_DOCUMENTS_ERROR]: (state) => ({
        ...state,
        isLoading: false
    }),
    [types.FETCH_DOCUMENTS_SUCCESS]: (state, { payload }) => {
        const { ElectronicDocflowInfoModel, PaperDocumentsInfoModel, CanRemoveLegalDocuments, CanConvertKeDaoBudgetToKeContract } = payload;

        const hasEdoDocuments = ElectronicDocflowInfoModel &&
            ElectronicDocflowInfoModel.ElectronicDocumentsGroupModels.length > 0;

        const hasPaperDocuments = PaperDocumentsInfoModel &&
            (PaperDocumentsInfoModel.Documents.length > 0 || PaperDocumentsInfoModel.LegalDocuments.length > 0);

        return {
            ...state,
            wasLoaded: true,
            isLoading: false,
            ElectronicDocflowInfoModel: hasEdoDocuments ? ElectronicDocflowInfoModel : null,
            PaperDocumentsInfoModel: hasPaperDocuments ? PaperDocumentsInfoModel : null,
            CanRemoveLegalDocuments,
            CanConvertKeDaoBudgetToKeContract
        };
    },

    [types.TOGGLE_EDO_DATA]: state => ({
        ...state,
        showEdoData: !state.showEdoData
    }),

    [types.TOGGLE_DOCUMENT_HISTORY]: (state, action) => ({
        ...state,
        documentHistoryDisplayMap: showDocumentHistoryMapReducer(state.documentHistoryDisplayMap, action)
    }),

    [types.UPLOAD_DOCUMENT_SUCCEED]: (state) => ({
        ...state,
        fetchRequired: true
    }),

    [types.DELETE_DOCUMENTS_PACKAGE_SUCCESS]: (state) => ({
        ...state,
        fetchRequired: true
    }),

    [types.SWITCH_CONTRACT_BEGIN]: (state) => ({
        ...state,
        isLoading: true
    }),
    [types.SWITCH_CONTRACT_SUCCESS]: (state) => ({
        ...state,
        isLoading: false
    }),
    [types.SWITCH_CONTRACT_ERROR]: (state) => ({
        ...state,
        isLoading: false
    }),

    [LOCATION_CHANGE]: (state, { payload }) => {
        const { pathname } = payload;

        if (state.searchedDocumentId && isDocumentsRoute.test(pathname)) {
            return {
                ...state,
                searchedDocumentId: null
            };
        } else if (!state.searchedBillId && isDocumentsWithDocumentIdRoute.test(pathname)) {
            const [ searchedDocumentId ] = pathname.split("/").reverse();

            return {
                ...state,
                searchedDocumentId
            };
        }

        return state;
    }
}, initialState);
