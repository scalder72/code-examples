import { expect } from "chai";
import freeze from "deep-freeze";

import reducer from "../reducer";
import * as actions from "../actions";

describe("bill info documents reducer ", () => {
    const initialState = freeze({
        fetchRequired: true,
        isLoading: false,
        wasLoaded: false,
        showEdoData: false,
        searchedDocumentId: null,
        documentHistoryDisplayMap: {}
    });

    it("should have initial state", () => {
        const actual = reducer(undefined, { type: "actionType" });
        expect(actual).to.deep.equal(initialState);
    });

    it("should toggle showEdoData state", () => {
        const state = freeze({ showEdoData: false });
        const expected = freeze({ showEdoData: true });

        const actual = reducer(state, actions.toggleEdoData());
        expect(actual).to.deep.equal(expected);
    });

    describe("should handle toggleDocumentHistory action", () => {
        it("and set documentId display state", () => {
            const initState = freeze({});
            const documentId = "documentId";
            const expected = { documentHistoryDisplayMap: { [documentId]: true } };

            const actual = reducer(initState, actions.toggleDocumentHistory(documentId));
            expect(actual).to.deep.equal(expected);
        });

        it("and toggle documentId display state", () => {
            const documentId = "documentId";
            const initState = freeze({ documentHistoryDisplayMap: { [documentId]: true } });
            const expected = { documentHistoryDisplayMap: { [documentId]: false } };

            const actual = reducer(initState, actions.toggleDocumentHistory(documentId));
            expect(actual).to.deep.equal(expected);
        });

        it("do nothing when documentId wasn't pass", () => {
            const documentHistoryDisplayMap = freeze({ documentId: true });
            const initState = freeze({ documentHistoryDisplayMap });
            const expected = { documentHistoryDisplayMap: { documentId: true } };

            const actual = reducer(initState, actions.toggleDocumentHistory());
            expect(actual).to.deep.equal(expected);
            expect(actual.documentHistoryDisplayMap).to.equal(documentHistoryDisplayMap);
        });
    });

    describe("should handle fetch data ", () => {
        const responseData = freeze({
            ElectronicDocflowInfoModel: { ElectronicDocumentsGroupModels: [1] },
            PaperDocumentsInfoModel: { Documents: [1, 2] }
        });

        it("begin action", () => {
            const expected = { ...initialState, fetchRequired: false, isLoading: true };
            const actual = reducer(initialState, actions.fetchDocumentsBegin());
            expect(actual).to.deep.equal(expected);
        });

        it("complete action", () => {
            const expected = { ...initialState, isLoading: false };
            const actual = reducer(initialState, actions.fetchDocumentsComplete());
            expect(actual).to.deep.equal(expected);
        });

        it("success action", () => {
            const expected = {
                ...initialState,
                wasLoaded: true,
                ElectronicDocflowInfoModel: { ElectronicDocumentsGroupModels: [1] },
                PaperDocumentsInfoModel: { Documents: [1, 2] },
                CanRemoveLegalDocuments: undefined,
                CanConvertKeDaoBudgetToKeContract: undefined
            };

            const actual = reducer(initialState, actions.fetchDocumentsSuccess(responseData));
            expect(actual).to.deep.equal(expected);
        });

        it("success action with no documents", () => {
            const innerResponseData = freeze({
                ElectronicDocflowInfoModel: { ElectronicDocumentsGroupModels: [] },
                PaperDocumentsInfoModel: { Documents: [], LegalDocuments: [] }
            });

            const expected = {
                ...initialState,
                wasLoaded: true,
                ElectronicDocflowInfoModel: null,
                PaperDocumentsInfoModel: null,
                CanRemoveLegalDocuments: undefined,
                CanConvertKeDaoBudgetToKeContract: undefined
            };

            const actual = reducer(initialState, actions.fetchDocumentsSuccess(innerResponseData));
            expect(actual).to.deep.equal(expected);
        });

        it("actions as a sequence", () => {
            const expected = {
                ...initialState,
                fetchRequired: false,
                isLoading: false,
                wasLoaded: true,
                ElectronicDocflowInfoModel: { ElectronicDocumentsGroupModels: [1] },
                PaperDocumentsInfoModel: { Documents: [1, 2] },
                CanRemoveLegalDocuments: undefined,
                CanConvertKeDaoBudgetToKeContract: undefined
            };

            const actual1 = reducer(initialState, actions.fetchDocumentsBegin());
            const actual2 = reducer(actual1, actions.fetchDocumentsComplete());
            const actual3 = reducer(actual2, actions.fetchDocumentsSuccess(responseData));

            expect(expected).to.deep.equal(actual3);
        });
    });

    describe("handle switch contract ", () => {
        it("should handle begin", () => {
            const initState = freeze({ isLoading: false });
            const expectedState = { isLoading: true };
            const actual = reducer(initState, actions.switchContractBegin());

            expect(actual).to.deep.equal(expectedState);
        });

        it("should handle success", () => {
            const initState = freeze({ isLoading: true });
            const expectedState = { isLoading: false };
            const actual = reducer(initState, actions.switchContractSuccess());

            expect(actual).to.deep.equal(expectedState);
        });

        it("should handle error", () => {
            const initState = freeze({ redirectToPrintRequired: false, isLoading: true });
            const expectedState = { redirectToPrintRequired: false, isLoading: false };
            const actual = reducer(initState, actions.switchContractError());

            expect(actual).to.deep.equal(expectedState);
        });
    });
});
