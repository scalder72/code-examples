﻿import { handleActions } from "redux-actions";
import * as types from "./actionTypes";

const initialState = {
    docflowRejectReason: ""
};

export const layoutReducer = handleActions({
    [types.ASIDE_FETCH_SUCCESS]: (state, { payload: { IsDocflowRejected, DocflowRejectionReasons } }) => {
        const hasDocflowRejectionReasons = IsDocflowRejected && DocflowRejectionReasons.length > 0;

        if (!hasDocflowRejectionReasons) {
            return state;
        }

        return ({
            ...state,
            docflowRejectReason: DocflowRejectionReasons[0]
        });
    }
}, initialState);

export default layoutReducer;

