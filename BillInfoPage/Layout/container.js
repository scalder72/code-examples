﻿import { connect } from "react-redux";
import BillInfoPage from "./components/BillInfoPage.jsx";

const mapStateToProps = ({ billInfo: { mainBillInfo, availableTabs, urls }}) => ({ mainBillInfo, availableTabs, urls });
export default connect(mapStateToProps)(BillInfoPage);
