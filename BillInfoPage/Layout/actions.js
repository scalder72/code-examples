﻿import { createAction } from "redux-actions";
import axios from "billing-ui/libs/axios";
import * as types from "./actionTypes";

export const asideFetchSuccess = createAction(types.ASIDE_FETCH_SUCCESS);

export const fetchAsideData = () => (dispatch, getStore) => {
    const { billInfo } = getStore();
    const { urls: { CheckDocflowStatusUrl } } = billInfo;

    axios
        .get(CheckDocflowStatusUrl)
        .then(({ data }) => dispatch(asideFetchSuccess(data)));
};

