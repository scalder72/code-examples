﻿import { getDescriptionCreator } from "../../../Shared/utils";

const billInfoTab = {
    Purchase: "Purchase",
    Certificates: "Certificates",
    Tokens: "Tokens",
    Subscriptions: "Subscriptions",
    Documents: "Documents"
};

billInfoTab.getDescription = getDescriptionCreator({
    [billInfoTab.Purchase]: "Покупка",
    [billInfoTab.Certificates]: "Сертификаты",
    [billInfoTab.Tokens]: "Токены",
    [billInfoTab.Subscriptions]: "Получатели",
    [billInfoTab.Documents]: "Документы"
});

billInfoTab.getRouteTo = getDescriptionCreator({
    [billInfoTab.Purchase]: "",
    [billInfoTab.Certificates]: "/certificates",
    [billInfoTab.Tokens]: "/tokens",
    [billInfoTab.Subscriptions]: "/subscriptions",
    [billInfoTab.Documents]: "/documents"
});

export default billInfoTab;
