﻿import { getDescriptionCreator } from "./../../../Shared/utils";

const type = {
    None: "None",
    ClientTestOrPilot: "ClientTestOrPilot",
    ClientRejected: "ClientRejected",
    ClientHaveSeveralHeadOrganization: "ClientHaveSeveralHeadOrganization",
    IncorrectLegalAddress: "IncorrectLegalAddress",
    EdoStatusInvalid: "EdoStatusInvalid",
    UserScanExists: "UserScanExists",
    ClientNotFound: "ClientNotFound",
    NoRegionCode: "NoRegionCode",
    ExternalDocflowExists: "ExternalDocflowExists",
    IncorrectLegalAddressAndNoRegionCode: "IncorrectLegalAddressAndNoRegionCode",
    UserScanCompleted: "UserScanCompleted",
    BillWaitingForPayment: "BillWaitingForPayment",
    BillAlreadyPaid: "BillAlreadyPaid",
    ExpiredAfterBillPaid: "ExpiredAfterBillPaid",
    ExpiredAfterBillConditionallyPaid: "ExpiredAfterBillConditionallyPaid",
    BillAlreadyConditionallyPaid: "BillAlreadyConditionallyPaid",
    DocumentFieldsNotFilled: "DocumentFieldsNotFilled",
    DocflowAlreadyExists: "DocflowAlreadyExists",
    BillNotFound: "BillNotFound",
    BillTypeIsImport: "BillTypeIsImport",
    DocumentSendInProgress: "DocumentSendInProgress"
};

type.getDescription = getDescriptionCreator({
    [type.None]: "Всё хорошо",
    [type.ClientTestOrPilot]: "У клиента зарегистрирован только тестовый ящик в Диадоке",
    [type.ClientRejected]: "Клиент отказался от электронного документооборота с Контуром",
    [type.ClientHaveSeveralHeadOrganization]: "У клиента имеется несколько головных организаций",
    [type.IncorrectLegalAddress]: "Адрес заполнен не по КЛАДР",
    [type.EdoStatusInvalid]: "Документ в диадоке не анулирован",
    // eslint-disable-next-line max-len
    [type.UserScanExists]: "Идет документооборот по бумажному акту, поэтому электронный отправить нельзя. Статус бумажного документооборота на вкладке «документы».",
    [type.ClientNotFound]: "Клиента нет в Диадоке, либо он не добавил контрагентов",
    [type.NoRegionCode]: "Регион не заполнен. Заполните, чтобы отправить документы",
    [type.ExternalDocflowExists]: "Запущен ЭДО вне системы",
    [type.IncorrectLegalAddressAndNoRegionCode]: "Адрес заполнен не по КЛАДР. Заполните хотя бы регион, чтобы отправить документы",
    [type.UserScanCompleted]: "Успешно заверешен документооборот по бумажному акту, поэтому электронный отправить нельзя.",
    [type.BillWaitingForPayment]: "Счет не оплачен, поэтому документ нельзя отправить.",
    [type.BillAlreadyPaid]: "Счет оплачен, поэтому документ нельзя отправить.",
    [type.ExpiredAfterBillPaid]: "Нельзя отправить документ через 12 месяцев после оплаты счета",
    [type.ExpiredAfterBillConditionallyPaid]: "Нельзя отправить документ через 12 месяцев после принятия условной оплаты счета",
    [type.BillAlreadyConditionallyPaid]: "Нельзя отправить документ после принятия условной оплаты счета",
    [type.DocumentFieldsNotFilled]: "Документ не заполнен и не сохранен.\r\nЗаполните на странице печати и напечатайте.",
    [type.DocflowAlreadyExists]: "ЭДО по документу уже запущен.",
    [type.BillNotFound]: "Счет не найден.",
    [type.BillTypeIsImport]: "ЭДО нельзя запустить по импортированному счету",
    [type.DocumentSendInProgress]: "Документ отправляется."
});

export default type;
