﻿import { Component, PropTypes } from "react";

import Header from "./Header.jsx";
import AsideWrapper from "./AsideWrapper.jsx";
import CommentsBox from "./../../components/common/Comments/CommentsBox.jsx";

import BillInfoTabs from "./BillInfoTabs.jsx";
import styles from "./../styles/BillInfoPage.scss";

class BillInfoPage extends Component {
    render() {
        const { mainBillInfo, availableTabs, urls, children } = this.props;
        const { GoToPayerPageUrl, CommentariesUrl } = urls;
        const { BillId } = mainBillInfo;

        return (
            <div className={styles.wrapper}>
                <div className={styles.main}>
                    <Header {...mainBillInfo} goToBackUrl={GoToPayerPageUrl} />
                    <CommentsBox url={CommentariesUrl} billId={BillId} />
                    { availableTabs && availableTabs.length > 1 && <BillInfoTabs billInfoTabs={availableTabs} billId={BillId} /> }
                    { children }
                </div>
                <AsideWrapper />
            </div>
        );
    }
}

BillInfoPage.propTypes = {
    children: PropTypes.node,
    urls: PropTypes.object.isRequired,
    mainBillInfo: PropTypes.object.isRequired,
    availableTabs: PropTypes.arrayOf(PropTypes.string)
};

export default BillInfoPage;
