﻿import { Component, PropTypes } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import pureRender from "react-pure-render/function";

import Aside from "./Aside.jsx";

import { fetchAsideData } from "./../actions";

class AsideWrapper extends Component {
    shouldComponentUpdate: pureRender;

    _resolveFetch() {
        const { fetchRequired, fetchAsideData } = this.props;

        if (fetchRequired) {
            fetchAsideData();
        }
    }

    componentDidMount() {
        const { fetchAsideData } = this.props;
        fetchAsideData();
    }

    componentDidUpdate() {
        this._resolveFetch();
    }

    render() {
        const { docflowRejectReason, mainBillInfo } = this.props;

        return (
            <Aside { ...mainBillInfo } docflowRejectReason={docflowRejectReason} />
        );
    }
}

AsideWrapper.propTypes = {
    mainBillInfo: PropTypes.object,
    fetchRequired: PropTypes.bool,
    fetchAsideData: PropTypes.func.isRequired,
    docflowRejectReason: PropTypes.string.isRequired
};

const mapStateToProps = ({ billInfo: { layoutState, mainBillInfo }} = {}) => ({ ...layoutState, mainBillInfo });
const mapDispatchToProps = dispatch => bindActionCreators({ fetchAsideData }, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(AsideWrapper);
