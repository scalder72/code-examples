﻿import { PropTypes } from "react";
import Link from "react-router/lib/Link";

import GoogleAnalytics from "billing-ui/helpers/GoogleAnalytics";
import { createServiceCenterInfoUrl } from "../../Shared/Helpers/UrlHelper";

import ActionButtonsList from "../../ActionButtonsList";
import BillTrackItem from "./../../components/common/BillTrackItem.jsx";
import DocflowErrorBlock from "./DocflowErrorBlock.jsx";

import styles from "./../styles/Aside.scss";

const onClickPartner = label => {
    GoogleAnalytics.triggerEventAsync("bill-general", "open", label);
};

const createManagerName = (manager) => {
    if (manager) {
        return ` / ${manager.Code} - ${manager.Name}`;
    }

    return null;
};

const Aside = ({ BillCreatorInfo, BillServicePartner, BillDeliveryPartner, BillId, ServicePackageUrl, docflowRejectReason }) => (
    <div className={styles.block}>
        {ServicePackageUrl && (
            <div className={styles["cs-link-wrapper"]}>
                <a className={styles["cs-link"]} href={ServicePackageUrl} target="_blank">
                    Пакет услуг в КС
                </a>
            </div>
        )}

        <div className={styles["action-buttons"]}>
            <ActionButtonsList />
        </div>

        {docflowRejectReason && <DocflowErrorBlock rejectReason={docflowRejectReason} />}

        <div className={styles["bill-track"]}>
            {BillCreatorInfo && (
                <BillTrackItem title="Выставил:">
                    {BillCreatorInfo}
                </BillTrackItem>
            )}

            {BillServicePartner && (
                <BillTrackItem title="Обслуживает:">
                    <Link className={styles["bill-track-link"]}
                          to={createServiceCenterInfoUrl(BillId, BillServicePartner.Id)}
                          onClick={() => { onClickPartner("service-center") }}>
                        {BillServicePartner.Code}
                    </Link>
                    {createManagerName(BillServicePartner.Manager)}
                </BillTrackItem>
            )}

            {BillDeliveryPartner && (
                <BillTrackItem title="Доставляет:">
                    <Link className={styles["bill-track-link"]}
                          to={createServiceCenterInfoUrl(BillId, BillDeliveryPartner.Id)}
                          onClick={() => { onClickPartner("deliver-center") }}>
                        {BillDeliveryPartner.Code}
                    </Link>
                    {createManagerName(BillDeliveryPartner.Manager)}
                </BillTrackItem>
            )}
        </div>
    </div>
);

Aside.propTypes = {
    docflowRejectReason: PropTypes.string,

    BillCreatorInfo: PropTypes.string,
    BillServicePartner: PropTypes.shape({
        Id: PropTypes.string.isRequired,
        Code: PropTypes.string.isRequired
    }),
    BillDeliveryPartner: PropTypes.shape({
        Id: PropTypes.string.isRequired,
        Code: PropTypes.string.isRequired
    }),
    DocflowRejectReason: PropTypes.string,
    BillId: PropTypes.string.isRequired,
    ServicePackageUrl: PropTypes.string
};

export default Aside;
