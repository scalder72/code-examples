﻿import { PropTypes } from "react";
import cx from "classnames";
import css from "./../styles/DocflowErrorBlock.scss";
import docflowRejectReason from "../utils/DocflowRejectReason";

const DocflowErrorBlock = ({ rejectReason }) => (
    <div className={cx(css.block, css.error)}>
        <div>
            <span className={cx(css.diadocIcon, css.strikeout)} />
        </div>
        {docflowRejectReason.getDescription(rejectReason)}
    </div>
);

DocflowErrorBlock.propTypes = {
    rejectReason: PropTypes.string.isRequired
};

export default DocflowErrorBlock;
