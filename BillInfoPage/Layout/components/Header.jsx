﻿import { Component, PropTypes } from "react";
import cx from "classnames";

import DateWithTime from "../../Shared/components/DateWithTime.jsx";
import HeaderBackLink from "./../../Shared/components/HeaderBackLink.jsx";

import styles from "./../styles/Header.scss";

class Header extends Component {
    _toShortProductName = string => {
        return string.replace("Контур.", "").replace("Контур-", "");
    };

    render() {
        const {BillNumber, BillCreateDate, ProductName, ProductId, goToBackUrl} = this.props;
        const iconClasses = cx(styles.product_icon, styles[ProductId.toLowerCase()]);

        const shortProductName = this._toShortProductName(ProductName);

        return (
            <div className={styles.wrapper}>
                <HeaderBackLink to={goToBackUrl} withHoverEffect={true}>карточка клиента</HeaderBackLink>

                <div className={styles.bill}>
                    <div>
                        Счет №<span className={styles.billNumber}>{BillNumber || "_________"}</span>
                    </div>
                    <div className={styles.date}>
                        <DateWithTime date={BillCreateDate} ignoreZeroTime={true} />
                    </div>
                </div>

                <div className={styles.product}>
                    <span className={iconClasses} />
                    {shortProductName}
                </div>
            </div>
        );
    }
}

Header.propTypes = {
    goToBackUrl: PropTypes.string.isRequired,
    BillNumber: PropTypes.string,
    BillCreateDate: PropTypes.string.isRequired,
    ProductName: PropTypes.string.isRequired,
    ProductId: PropTypes.string.isRequired
};

export default Header;
