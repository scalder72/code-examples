﻿import { PropTypes } from "react";
import Link from "react-router/lib/Link";
import IndexLink from "react-router/lib/IndexLink";

import billInfoTab from "./../utils/billInfoTab";
import styles from "./../styles/BillInfoTabs.scss";

const BillInfoTab = ({ tab, billId }) => {
    const TabLink = tab === billInfoTab.Purchase ? IndexLink : Link;
    return (
        <TabLink to={`/billinfo/${billId}${billInfoTab.getRouteTo(tab)}`}
                 data-ft-id={tab}
                 className={styles.tab}
                 activeClassName={styles.active}>
            {billInfoTab.getDescription(tab)}
        </TabLink>
    );
};

BillInfoTab.propTypes = {
    billId: PropTypes.string.isRequired,
    tab: PropTypes.string
};

export default BillInfoTab;
