﻿import { PropTypes } from "react";
import cx from "classnames";

import BillInfoTab from "./BillInfoTab.jsx";
import styles from "./../styles/BillInfoTabs.scss";

const BillInfoTabs = ({ billInfoTabs, billId }) => (
    <div className={cx(styles.block, (billInfoTabs.length > 3 ? (styles.small) : ""))}>
        {billInfoTabs.map(tab => <BillInfoTab key={tab} tab={tab} billId={billId} />)}
    </div>
);

BillInfoTabs.propTypes = {
    billInfoTabs: PropTypes.arrayOf(PropTypes.string),
    billId: PropTypes.string.isRequired,
    tab: PropTypes.string
};

export default BillInfoTabs;
