﻿import { Component, PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import cx from "classnames";

import headerStyles from "../Layout/styles/Header.scss";
import styles from "./BillInfoDumpedPage.scss";
import buttonStyles from "../Shared/styles/ActionButton.scss";
import imgSrc from "./img/dumped.png";

import Button from "billing-ui/components/Button";
import HeaderBackLink from "../Shared/components/HeaderBackLink.jsx";
import { restoreBill } from "../actions/billDeleteActions";

class DumpedPage extends Component {
    render() {
        const { PayerId, BillNumber, BillId, restoreBill } = this.props;

        return (
            <div className={styles.wrapper}>
                <div className={headerStyles.wrapper}>
                    <HeaderBackLink to={`/client/${PayerId}`}>Вернуться на карточку клиента</HeaderBackLink>
                </div>

                <div className={styles.content}>
                    <img src={imgSrc} alt="" />

                    <p>
                        <span className={styles["dumped-number"]}>Счет №{BillNumber}</span>
                    </p>
                    <p className={styles.status}>удален</p>

                    <div className={styles.restore}>
                        <Button className={cx(buttonStyles.button, buttonStyles.item)}
                                onClick={() => { restoreBill(this.context.router, BillId); }}>
                            Восстановить
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

DumpedPage.propTypes = {
    PayerId: PropTypes.string.isRequired,
    BillNumber: PropTypes.string.isRequired,
    BillId: PropTypes.string.isRequired,
    restoreBill: PropTypes.func.isRequired
};

DumpedPage.contextTypes = {
    router: PropTypes.object
};

const mapStateToProps = ({ billInfo: { mainBillInfo }}) => mainBillInfo;
const mapDispatchToProps = dispatch => bindActionCreators({restoreBill}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(DumpedPage);
