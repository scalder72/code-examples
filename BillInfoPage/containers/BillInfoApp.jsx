﻿import { PropTypes } from "react";
import { connect } from "react-redux";

import Loader from "react-ui/Loader";
import styles from "./BillInfoApp.scss";

const BillInfoApp = ({ appIsLoading, children }) => (
    <div className={styles.container}>
        <Loader active={appIsLoading} type="big" caption="">
            {children}
        </Loader>
    </div>
);

BillInfoApp.propTypes = {
    children: PropTypes.node,
    appIsLoading: PropTypes.bool
};

const mapStateToProps = ({ billInfo: { appState = {} }}) => appState;
export default connect(mapStateToProps)(BillInfoApp);
